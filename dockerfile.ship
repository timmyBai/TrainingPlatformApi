FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app

COPY /TrainingPlatformApi/*.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:6.0

WORKDIR /TrainingPlatformApi

RUN apt-get update -y \ 
    && apt-get install -y libgdiplus \
    && apt-get clean \
    && ln -s /usr/lib/libgdiplus.so /usr/lib/gdiplus.dll

COPY --from=build-env /app/out .
COPY --from=build-env /app/TrainingPlatformApi/Fonts ./Fonts
COPY --from=build-env /app/TrainingPlatformApi/Assets ./Assets
COPY --from=build-env /app/package.json .

ENTRYPOINT ["dotnet", "TrainingPlatformApi.dll"]