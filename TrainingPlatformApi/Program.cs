using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using TrainingPlatformApi.Models;
using TrainingPlatformApi.Services;
using TrainingPlatformApi.Utils;

var TrainingBackstage = "trainingBackstage";
string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

var builder = WebApplication.CreateBuilder(args);

// 加入控制器
builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddHttpContextAccessor();

// 加入跨域
builder.Services.AddCors(policy =>
{
    policy.AddPolicy(name: TrainingBackstage, builder =>
    {
        builder.WithOrigins("*").WithMethods("GET", "POST", "PUT", "PATCH", "DELETE")
            .WithHeaders("Content-Type")
            .WithHeaders("Authorization")
            .WithHeaders("X-Requested-With")
            .WithHeaders("Accept")
            .WithHeaders("Origin");
    });
});

// 加入 swgger
builder.Services.AddSwaggerGen(config =>
{
    string projectName = $"Training Platform Api {environment}";
    string packageJson = $@"{AppDomain.CurrentDomain.BaseDirectory}package.json";
    StreamReader sr = new StreamReader(packageJson);
    string fileText = sr.ReadToEnd();
    var packageJsonKey = JsonConvert.DeserializeObject<PackageJsonModels>(fileText);
    string major = packageJsonKey.version.Split(".")[0];

    config.SwaggerDoc($"v{major}", new OpenApiInfo()
    {
        Version = packageJsonKey.version,
        Title = projectName,
        Description = packageJsonKey.description,
        TermsOfService = new Uri(new HostService().Domain),
        Contact = new OpenApiContact()
        {
            Name = "Supper",
            Email = "supper@emgtech.com.tw"
        },
        License = new OpenApiLicense()
        {
            Name = packageJsonKey.license,
            Url = new Uri(new HostService().Domain)
        }
    });

    config.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = "JWT Authorization"
    });

    config.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            new string[] {}
        }
    });


});

builder.Services.AddTransient<HostService>();

// 加入 jwt 驗證：
builder.Services
    .AddAuthentication(s =>
    {
        s.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        s.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        s.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateLifetime = true, //是否驗證失效時間
            ClockSkew = TimeSpan.Zero,  // 允許誤差時間
            ValidateAudience = false, // 是否驗證 Audience（驗證之前的 token 是否失效)
            ValidateIssuer = true, //是否驗證頒發者
            ValidAudience = JwtUtils.Domain, // 有效接收者 
            ValidIssuer = JwtUtils.Domain, // 有效頒發者
            ValidateIssuerSigningKey = true, // 是否驗證安全密鑰
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtUtils.SecurityKey)) //拿到祕鑰 SecurityKey
        };

        options.Events = new JwtBearerEvents()
        {
            OnAuthenticationFailed = context =>
            {
                //Token expired
                if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                {
                    context.Response.StatusCode = 401;
                    context.Response.Headers.Add("Token-Expired", "true");
                }

                return Task.CompletedTask;
            }
        };
    });

var app = builder.Build();

if (app.Environment.IsDevelopment() || app.Environment.IsStaging())
{
    app.UseSwagger(options =>
    {
        options.RouteTemplate = "TrainingPlatformApi/swagger/{documentName}/swagger.json";
    });
    app.UseSwaggerUI(options =>
    {
        options.RoutePrefix = "TrainingPlatformApi/swagger";
        options.SwaggerEndpoint("/TrainingPlatformApi/swagger/v1/swagger.json", "v1");
    });
}

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

app.UseCors(TrainingBackstage);

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
