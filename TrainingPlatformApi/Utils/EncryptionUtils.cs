using System.Security.Cryptography;
using System.Text;

namespace TrainingPlatformApi.Utils
{
    public class EncryptionUtils
    {
        /// <summary>
        /// sha256 加密
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public string SHA256Encryption(string text)
        {
            // 新增 sha256 哈希值
            var sha256 = SHA256.Create();

            // 產生 hash
            var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(text));

            return BitConverter.ToString(hash, 0).Replace("-", "");
        }
    }
}
