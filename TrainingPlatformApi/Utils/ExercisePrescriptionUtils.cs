/*
 * 運動處方共用方法
 */
namespace TrainingPlatformApi.Utils
{
    public class ExercisePrescriptionUtils
    {
        public static string ageSql = "SET @age = (SELECT YEAR(DATE_SUB(NOW(), INTERVAL 0 YEAR)) - SubString(user_authorized.birthday, 1, 4) AS age FROM user_authorized WHERE userAuthorizedId = @userAuthorizedId);";
        public static string lowheartRateSql = "SET @lowheartRate = round(206.9 - (0.67 * @age) * 0.55);";
        public static string middleMinHeartRateSql = "SET @middleMinHeartRate = round(206.9 - (0.67 * @age) * 0.55);";
        public static string middleMaxHeartRateSql = "SET @middleMaxHeartRate = round(206.9 - (0.67 * @age) * 0.65);";
        public static string highMinHeartRateSql = "SET @highMinHeartRate   = round(206.9 - (0.67 * @age) * 0.65);";
        public static string highMaxHeartRateSql = "SET @highMaxHeartRate   = round(206.9 - (0.67 * @age) * 0.9);";

        /// <summary>
        /// 檢查運動處方-心肺適能，是否有非法運動項目
        /// </summary>
        /// <param name="sports">運動項目</param>
        /// <returns></returns>
        public bool InspactExercisePrescriptionCardiorespiratoryFitnessSports(List<string> sports)
        {
            bool result = true;
            List<string> sportNameList = new List<string>() { "有氧操", "自行車", "慢跑", "游泳", "跳繩", "飛輪", "原地登階", "健走" };

            if (sports.Count() == 0)
            {
                result = true;
            }
            else
            {
                for (int i = 0; i < sports.Count(); i++)
                {
                    if (sportNameList.IndexOf(sports[i]) == -1)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 檢查運動處方-肌力與肌耐力，是否有非法訓練部位項目
        /// </summary>
        /// <param name="trainingArea">訓練部位項目</param>
        /// <returns></returns>
        public bool InspactExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea(List<string> trainingArea)
        {
            bool result = true;
            List<string> trainingAreaList = new List<string>() { "上肢", "核心", "下肢" };

            if (trainingArea.Count() == 0)
            {
                result = true;
            }
            else
            {
                for (int i = 0; i < trainingArea.Count(); i++)
                {
                    if (trainingAreaList.IndexOf(trainingArea[i]) == -1)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 檢查運動處方-柔軟度，是否有非法訓練部位項目
        /// </summary>
        /// <param name="sports">運動項目</param>
        /// <returns></returns>
        public bool InspactExercisePrescriptionSoftnessSports(List<string> sports)
        {
            bool result = true;
            List<string> sportNameList = new List<string>() { "瑜珈", "伸展操" };

            if (sports.Count() == 0)
            {
                result = true;
            }
            else
            {
                for(int i = 0; i < sports.Count(); i++)
                {
                    if (sportNameList.IndexOf(sports[i]) == -1)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 檢查運動處方-平衡，是否有非法訓練部位項目
        /// </summary>
        /// <param name="sports">運動項目</param>
        /// <returns></returns>
        public bool InspactExercisePrescriptionBalanceSports(List<string> sports)
        {
            bool result = true;
            List<string> sportNameList = new List<string>() { "靜態平衡訓練", "動態平衡訓練", "太極拳" };

            if (sports.Count() > 0)
            {
                result = true;
            }
            else
            {
                for (int i = 0; i < sports.Count(); i++)
                {
                    if (sportNameList.IndexOf(sports[i]) == -1)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }
    }
}
