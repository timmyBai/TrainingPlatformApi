using System.Globalization;

namespace TrainingPlatformApi.Utils
{
    public class DayUtils
    {
        private TimeZoneInfo? timeZone = null;
        private DateTime nowUtc;

        public DayUtils()
        {
            timeZone = TimeZoneInfo.FindSystemTimeZoneById("Asia/Taipei");
            nowUtc = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);
        }

        /// <summary>
        /// 取得完整日期
        /// </summary>
        /// <returns></returns>
        public string GetFullDate()
        {
            return nowUtc.ToString("yyyyMMdd", CultureInfo.GetCultureInfo("zh-TW"));
        }

        /// <summary>
        /// 取得完整時間
        /// </summary>
        /// <returns></returns>
        public string GetFullTime()
        {
            return nowUtc.ToString("HHmmss", CultureInfo.GetCultureInfo("zh-TW"));
        }

        /// <summary>
        /// 取得這週剩餘天數
        /// </summary>
        /// <returns></returns>
        public List<string> GetThisWeekRemainingDayRange()
        {
            List<string> result = new List<string>();

            DateTime nowDate = DateTime.Now; // 取得今天日期
            DayOfWeek week = nowDate.DayOfWeek; // 取得今天星期

            for (int i = Convert.ToInt32(week); i < 7; i++)
            {
                var startNextWeekDate = nowDate.AddDays(i - Convert.ToInt32(week));

                result.Add(startNextWeekDate.ToString("yyyy-MM-dd"));
            }

            return result;
        }

        /// <summary>
        /// 取得這週日期範圍
        /// </summary>
        /// <returns></returns>
        public List<string> GetThisWeekDateRange()
        {
            List<string> result = new List<string>();

            DateTime nowDate = DateTime.Now; // 取得今天日期
            DayOfWeek week = nowDate.DayOfWeek; // 取得今天星期

            for (int i = 0; i < 7; i++)
            {
                var startNextWeekDate = nowDate.AddDays(i - Convert.ToInt32(week));

                result.Add(startNextWeekDate.ToString("yyyy-MM-dd"));
            }

            return result;
        }

        /// <summary>
        /// 取得上週日期範圍
        /// </summary>
        /// <returns></returns>
        public List<string> GetPreviousWeekDateRange(int range = 7)
        {
            List<string> result = new List<string>();

            DateTime nowDate = DateTime.Now; // 取得今天日期
            DayOfWeek week = nowDate.DayOfWeek; // 取得今天星期

            for (int i = 0; i < range; i++)
            {
                var startNextWeekDate = nowDate.AddDays(i - Convert.ToInt32(week) - range);

                result.Add(startNextWeekDate.ToString("yyyy-MM-dd"));
            }

            return result;
        }

        /// <summary>
        /// 取得下週日期範圍
        /// </summary>
        /// <returns></returns>
        public List<string> GetNextWeekDateRange(int range = 7)
        {
            List<string> result = new List<string>();

            DateTime nowDate = DateTime.Now; // 取得今天日期
            DayOfWeek week = nowDate.DayOfWeek; // 取得今天星期

            for (int i = 0; i < range; i++)
            {
                var startNextWeekDate = nowDate.AddDays(i - Convert.ToInt32(week) + 7);

                result.Add(startNextWeekDate.ToString("yyyy-MM-dd"));
            }

            return result;
        }

        /// <summary>
        /// 取得這週往後計算日期範圍
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public List<string> GetThisWeekAfterCalcRange(int range = 7)
        {
            List<string> result = new List<string>();

            DateTime nowDate = DateTime.Now; // 取得今天日期
            DayOfWeek week = nowDate.DayOfWeek; // 取得今天星期

            for (int i = 0; i < range; i++)
            {
                var startNextWeekDate = nowDate.AddDays(i - (Convert.ToInt32(week)) + 5);

                result.Add(startNextWeekDate.ToString("yyyy-MM-dd"));
            }

            return result;
        }
    }
}
