using QRCoder;
using System.Drawing.Imaging;
using System.Drawing;

namespace TrainingPlatformApi.Utils
{
    public class QRCodeUtils
    {
        /// <summary>
        /// qrcode 產生器
        /// </summary>
        QRCodeGenerator? grcodeGenerator = null;

        /// <summary>
        /// qrcode 資料
        /// </summary>
        QRCodeData? qrCodeData = null;

        /// <summary>
        /// qrcode
        /// </summary>
        QRCode? qrcode = null;

        /// <summary>
        /// 完整 qrcode 圖片
        /// </summary>
        Bitmap? currentQRCode = null;

        public void GenerateQRCodePicture(string data)
        {
            grcodeGenerator = new QRCodeGenerator();
            qrCodeData = grcodeGenerator.CreateQrCode(data, QRCodeGenerator.ECCLevel.H);
            qrcode = new QRCode(qrCodeData);

            Bitmap companyIcon = new Bitmap($"{AppDomain.CurrentDomain.BaseDirectory}/Assets/emg_logo.png");
            Bitmap qrcodeGraphic = qrcode.GetGraphic(50, Color.Black, Color.White, companyIcon, 15, 30, true);

            currentQRCode = new Bitmap(qrcodeGraphic.Width, qrcodeGraphic.Height);
            currentQRCode.MakeTransparent();

            Graphics currentQRCodeGraphic = Graphics.FromImage(currentQRCode);
            currentQRCodeGraphic.Clear(Color.Transparent);
            currentQRCodeGraphic.DrawImage(qrcodeGraphic, 0, 0);

            FontFamily fontFamily = new FontFamily("Times New Roman");
            Font font = new Font(fontFamily, 100f, FontStyle.Bold, GraphicsUnit.Pixel);

            int strWidth = (int)currentQRCodeGraphic.MeasureString(data, font).Width;

            int wordStartX = (qrcodeGraphic.Width - strWidth) / 2;
            int wordStartY = qrcodeGraphic.Height - 150;

            currentQRCodeGraphic.DrawString(data, font, Brushes.Black, wordStartX, wordStartY);
        }

        public Bitmap Save(string savePath, string saveFileName, ImageFormat format)
        {
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }

            currentQRCode.Save(savePath + saveFileName, format);

            return currentQRCode;
        }

        public string GenerateBase64Image(string data, ImageFormat format)
        {
            GenerateQRCodePicture(data);

            MemoryStream ms = new MemoryStream();

            currentQRCode.Save(ms, ImageFormat.Png);
            ms.Seek(0, SeekOrigin.Begin);

            byte[] arr = ms.ToArray();

            return Convert.ToBase64String(arr);
        }
    }
}
