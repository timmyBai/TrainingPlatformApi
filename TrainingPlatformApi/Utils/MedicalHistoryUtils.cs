﻿using System.Reflection;

namespace TrainingPlatformApi.Utils
{
    public class MedicalHistoryUtils
    {
        /// <summary>
        /// 檢查有哪些疾病史
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public List<string> InspactMedicalHistory<T>(List<T> ahaAndacsmQuestionnaireList)
        {
            List<string> result = new List<string>();

            ahaAndacsmQuestionnaireList.ToList().ForEach((item) =>
            {
                object obj = (object)item;
                PropertyInfo partPropertyInfo = obj.GetType().GetProperty("part");
                PropertyInfo subjectProperInfo = obj.GetType().GetProperty("subject");

                int partValue = Convert.ToInt32(partPropertyInfo.GetValue(obj, null));
                int subjectValue = Convert.ToInt32(partPropertyInfo.GetValue(obj, null));

                if (partValue == 1)
                {
                    if (result.IndexOf("心臟病") == -1)
                    {
                        result.Add("心臟病");
                    }
                }

                if (partValue == 3 && subjectValue == 1)
                {
                    result.Add("糖尿病");
                }

                if (partValue == 3 && subjectValue == 2)
                {
                    result.Add("肺部疾病");
                }

                if (partValue == 3 && subjectValue == 4)
                {
                    result.Add("懷孕");
                }
            });


            return result;
        }
    }
}
