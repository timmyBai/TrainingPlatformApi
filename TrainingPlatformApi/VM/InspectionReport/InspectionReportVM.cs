﻿namespace TrainingPlatformApi.VM.InspectionReport
{
    /// <summary>
    /// 取得正式訓練學員訓練各台儀器總功率清單 回傳模型
    /// </summary>
    public class GetStudentEachMachinaryTotalPowerListVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;
        
        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryName { get; set; } = "";
        
        /// <summary>
        /// 總功率
        /// </summary>
        public int power { get; set; } = 0;
    }

    /// <summary>
    /// 取得學員機器訓練總功率
    /// </summary>
    public class GetStudentMachinaryTotalPowerVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 功率
        /// </summary>
        public float power { get; set; } = 0;
    }

    /// <summary>
    /// 最新取得學員機器訓練總功率
    /// </summary>
    public class LatestStudentMachinaryTotalPowerVM
    {
        /// <summary>
        /// 總功率
        /// </summary>
        public int totalPower { get; set; } = 0;
    }

    /// <summary>
    /// 取得過去正式訓練 20 次訓練狀況
    /// </summary>
    public class GetFormalTrainingStatusReportVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 訓練次數
        /// </summary>
        public int finsh { get; set; } = 0;

        /// <summary>
        /// 失誤次數
        /// </summary>
        public int mistake { get; set; } = 0;

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 訓練日期格式化文字
        /// </summary>
        public string trainingDateText { get; set; } = "";
    }

    /// <summary>
    /// 取得過去 20 次訓練狀況功率
    /// </summary>
    public class GetFormalTrainingPowerReportVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 當天總功率
        /// </summary>
        public int dayTotalPower { get; set; } = 0;

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 訓練日期格式化文字
        /// </summary>
        public string trainingDateText { get; set; } = "";
    }

    /// <summary>
    /// 取得機器全部訓練日期
    /// </summary>
    public class GetTrainingAllDayVM
    {
        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 轉換單日報表模型
    /// </summary>
    public class ConvertGetSingleDayReportVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 訓練次數
        /// </summary>
        public int finish { get; set; } = 0;

        /// <summary>
        /// 失誤次數
        /// </summary>
        public int mistake { get; set; } = 0;

        /// <summary>
        /// 功率
        /// </summary>
        public int power { get; set; } = 0;

        /// <summary>
        /// 訓練速度百分比
        /// </summary>
        public List<int> speed { get; set; } = new List<int>();

        /// <summary>
        /// 最大速度
        /// </summary>
        public int maxSpeed { get; set; } = 0;

        /// <summary>
        /// 速度百分比
        /// </summary>
        public List<int> speedPercentage { get; set; } = new List<int>();

        /// <summary>
        /// 自覺努力
        /// </summary>
        public int consciousEffort { get; set; } = 0;

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 最終報表
    /// </summary>
    public class FinallyGetSingleDayReportVM
    {
        /// <summary>
        /// 報表清單
        /// </summary>
        public List<ConvertGetSingleDayReportVM> singleDayReport { get; set; } = new List<ConvertGetSingleDayReportVM>();

        /// <summary>
        /// 上一天日期
        /// </summary>
        public string prevDay { get; set; } = "";

        /// <summary>
        /// 下一天日期
        /// </summary>
        public string nextDay { get; set; } = "";
    }

    /// <summary>
    /// 取得學員機器訓練總表
    /// </summary>
    public class StudentTrainingMachinarySummaryVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;
        
        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;
        
        /// <summary>
        /// 訓練次數
        /// </summary>
        public int finsh { get; set; } = 0;
        
        /// <summary>
        /// 失誤次數
        /// </summary>
        public int mistake { get; set; } = 0;
        
        /// <summary>
        /// 功率
        /// </summary>
        public int power { get; set; } = 0;

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";
    }

    /// <summary>
    /// 檢查正式訓練當天資料
    /// </summary>
    public class InspectFormalTrainingSingleDayVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;
        
        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;
        
        /// <summary>
        /// 訓練次數
        /// </summary>
        public int finish { get; set; } = 0;

        /// <summary>
        /// 失誤次數
        /// </summary>
        public int mistake { get; set; } = 0;

        /// <summary>
        /// 功率
        /// </summary>
        public int power { get; set; } = 0;
        
        /// <summary>
        /// 速度陣列
        /// </summary>
        public string speed { get; set; } = "";

        /// <summary>
        /// 最大速度
        /// </summary>
        public int maxSpeed { get; set; } = 0;
        
        /// <summary>
        /// 自覺努力量表
        /// </summary>
        public int consciousEffort { get; set; } = 0;
        
        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
        
        /// <summary>
        /// 訓練時間
        /// </summary>
        public string trainingTime { get; set; } = "";
    }
    
    public class InspectFormalTrainingSingleDayMarkVM
    {
        public int userAuthorizedId { get; set; } = 0;
        public string trainingDate { get; set; } = "";
        public string type { get; set; } = "";
        public int machinaryCode { get; set; } = 0;
        public string mark { get; set; } = "";
        public int newRecordId { get; set; } = 0;
        public string newRecordDate { get; set; } = "";
        public string newRecordTime { get; set; } = "";
        public int updateRecordId { get; set; } = 0;
        public string updateRecordDate { get; set; } = "";
        public string updateRecordTime { get; set; } = "";
    }
}
