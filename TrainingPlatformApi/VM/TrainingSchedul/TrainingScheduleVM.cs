﻿namespace TrainingPlatformApi.VM.TrainingSchedul
{
    /// <summary>
    /// 檢查訓練排程剩餘次數
    /// </summary>
    public class InspactTrainingScheduleRemainingFrequencyNotifyVM
    {
        /// <summary>
        /// 訓練排程-心肺適能剩餘次數
        /// </summary>
        public int cardiorespiratoryFitnessRemainingFrequency { get; set; } = 0;
        
        /// <summary>
        /// 訓練排程-肌力與肌耐力剩餘次數
        /// </summary>
        public int muscleStrengthAndMuscleEnduranceRemainingFrequency { get; set; } = 0;
        
        /// <summary>
        /// 訓練排程-柔軟度剩餘次數
        /// </summary>
        public int softnessRemainingFrequency { get; set; } = 0;
        
        /// <summary>
        /// 訓練排程-平衡剩餘次數
        /// </summary>
        public int balanceRemainingFrequency { get; set; } = 0;
    }

    /// <summary>
    /// 訓練排程通知模型
    /// </summary>
    public class TrainingScheduleNotifyVM
    {
        /// <summary>
        /// 是否通知
        /// </summary>
        public bool isNotify { get; set; } = false;
    }

    /// <summary>
    /// 最新運動處方-心肺適能下週剩餘排程天數
    /// </summary>
    public class LatestExercisePrescriptionCardiorespiratoryFitnessVM
    {
        public int dayCount { get; set; } = 0;

        /// <summary>
        /// 這週剩餘排程天數
        /// </summary>
        public int remainingFrequency { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/週)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練頻率文字(單位: 天/週)
        /// </summary>
        public string exerciseFrequencyText { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 訓練排程-心肺適能下週已排程日期
    /// </summary>
    public class ThisWeekDateTrainingScheduleCardiorespiratoryFitnessVM
    {
        /// <summary>
        /// 排程日期
        /// </summary>
        public string scheduleDate { get; set; } = "";
    }

    /// <summary>
    /// 訓練排程-心肺適能上週已排程日期
    /// </summary>
    public class PrevWeekDateTrainingScheduleCardiorespiratoryFitnessVM
    {
        public string scheduleDate { get; set; } = "";
    }

    /// <summary>
    /// 轉換最新運動處方-心肺適能下週剩餘排程天數
    /// </summary>
    public class ConvertThisWeekDateTrainingScheduleCardiorespiratoryFitnessVM
    {
        /// <summary>
        /// 這週剩餘排程天數
        /// </summary>
        public int remainingFrequency { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/週)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練頻率文字(單位: 天/週)
        /// </summary>
        public string exerciseFrequencyText { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 排程日期
        /// </summary>
        public List<string> scheduleDate { get; set; } = new List<string>();

        /// <summary>
        /// 這週日期範圍
        /// </summary>
        public List<string> thisWeekDateRemainingDayRange { get; set; } = new List<string>();

        /// <summary>
        /// 預設訓練排程
        /// </summary>
        public List<string> defaultTrainingScheduleDate { get; set; } = new List<string>();
    }

    /// <summary>
    /// 最新運動處方-肌力與肌耐力下週剩餘排程天數
    /// </summary>
    public class LatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceVM
    {
        /// <summary>
        /// 這週剩餘排程天數
        /// </summary>
        public int remainingFrequency { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/週)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練頻率文字(單位: 天/週)
        /// </summary>
        public string exerciseFrequencyText { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 組/天)
        /// </summary>
        public string trainingUnitGroup { get; set; } = "";

        /// <summary>
        /// 訓練單位(單位: 次/組)
        /// </summary>
        public string trainingUnitNumber { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 訓練排程-肌力與肌耐力下週排程日期
    /// </summary>
    public class ThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEnduranceVM
    {
        /// <summary>
        /// 排程日期
        /// </summary>
        public string scheduleDate { get; set; } = "";
    }

    /// <summary>
    /// 訓練排程-肌力與肌耐力上週排程日期
    /// </summary>
    public class PrevWeekDateTrainingScheduleMuscleStrengthAndMuscleEnduranceVM
    {
        /// <summary>
        /// 排程日期
        /// </summary>
        public string scheduleDate { get; set; } = "";
    }

    /// <summary>
    /// 轉換最新運動處方-肌力與肌耐力下週剩餘排程天數
    /// </summary>
    public class ConvertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEnduranceVM
    {
        /// <summary>
        /// 這週剩餘排程天數
        /// </summary>
        public int remainingFrequency { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/週)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練頻率文字(單位: 天/週)
        /// </summary>
        public string exerciseFrequencyText { get; set; } = "";

        /// <summary>
        /// 訓練單位(單位: 組/天)
        /// </summary>
        public string trainingUnitGroup { get; set; } = "";

        /// <summary>
        /// 訓練單位(單位: 次/組)
        /// </summary>
        public string trainingUnitNumber { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 排程日期
        /// </summary>
        public List<string> scheduleDate { get; set; } = new List<string>();

        /// <summary>
        /// 下周日期範圍
        /// </summary>
        public List<string> thisWeekDateRemainingDayRange { get; set; } = new List<string>();

        /// <summary>
        /// 預設訓練排程
        /// </summary>
        public List<string> defaultTrainingScheduleDate { get; set; } = new List<string>();
    }

    /// <summary>
    /// 最新運動處方-柔軟度下週剩餘排程天數
    /// </summary>
    public class LatestExercisePrescriptionSoftnessVM
    {
        /// <summary>
        /// 這週剩餘排程天數
        /// </summary>
        public int remainingFrequency { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/週)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練頻率文字(單位: 天/週)
        /// </summary>
        public string exerciseFrequencyText { get; set; } = "";

        /// <summary>
        /// 訓練單位(次/組)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 訓練排程-柔軟度下週排程日期
    /// </summary>
    public class ThisWeekDateTrainingScheduleSoftnessVM
    {
        /// <summary>
        /// 排程日期
        /// </summary>
        public string scheduleDate { get; set; } = "";
    }

    /// <summary>
    /// 訓練排程-柔軟度上週排程日期
    /// </summary>
    public class PrevWeekDateTrainingScheduleSoftnessVM
    {
        /// <summary>
        /// 排程日期
        /// </summary>
        public string scheduleDate { get; set; } = "";
    }

    /// <summary>
    /// 轉換訓練排程-柔軟度下週排程日期
    /// </summary>
    public class ConvertThisWeekDateTrainingScheduleSoftnessVM
    {
        /// <summary>
        /// 這週剩餘排程天數
        /// </summary>
        public int remainingFrequency { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/週)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練頻率文字(單位: 天/週)
        /// </summary>
        public string exerciseFrequencyText { get; set; } = "";

        /// <summary>
        /// 訓練單位(次/組)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 排程日期
        /// </summary>
        public List<string> scheduleDate { get; set; } = new List<string>();

        /// <summary>
        /// 下周日期範圍
        /// </summary>
        public List<string> thisWeekDateRemainingDayRange { get; set; } = new List<string>();

        /// <summary>
        /// 預設訓練排程
        /// </summary>
        public List<string> defaultTrainingScheduleDate { get; set; } = new List<string>();
    }

    /// <summary>
    /// 最新運動處方-平衡下週剩餘排程天數
    /// </summary>
    public class LatestExercisePrescriptionBalanceVM
    {
        /// <summary>
        /// 這週剩餘排程天數
        /// </summary>
        public int remainingFrequency { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/週)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練頻率文字(單位: 天/週)
        /// </summary>
        public string exerciseFrequencyText { get; set; } = "";

        /// <summary>
        /// 訓練單位(次/組)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 訓練排程-平衡這週排程日期
    /// </summary>
    public class ThisWeekDateTrainingScheduleBalanceVM
    {
        /// <summary>
        /// 排程日期
        /// </summary>
        public string scheduleDate { get; set; } = "";
    }

    /// <summary>
    /// 訓練排程-平衡上週排程日期
    /// </summary>
    public class PrevWeekDateTrainingScheduleBalanceVM
    {
        /// <summary>
        /// 排程日期
        /// </summary>
        public string scheduleDate { get; set; } = "";
    }

    /// <summary>
    /// 轉換最新運動處方-平衡下週剩餘排程天數
    /// </summary>
    public class ConvertThisWeekDateTrainingScheduleBalanceVM
    {
        /// <summary>
        /// 這週剩餘排程天數
        /// </summary>
        public int remainingFrequency { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/週)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練頻率文字(單位: 天/週)
        /// </summary>
        public string exerciseFrequencyText { get; set; } = "";

        /// <summary>
        /// 訓練單位(次/組)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 排程日期
        /// </summary>
        public List<string> scheduleDate { get; set; } = new List<string>();

        /// <summary>
        /// 下周日期範圍
        /// </summary>
        public List<string> thisWeekDateRemainingDayRange { get; set; } = new List<string>();

        /// <summary>
        /// 預設訓練排程
        /// </summary>
        public List<string> defaultTrainingScheduleDate { get; set; } = new List<string>();
    }

    /// <summary>
    /// 當日訓練排程-心肺適能實際完成
    /// </summary>
    public class DayTrainingScheduleCardiorespiratoryFitnessActualCompletionVM
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";
        
        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 本日目標(天/週)
        /// </summary>
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 本日目標(分鐘/次)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";
        
        /// <summary>
        /// 運動項目
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 實際完成(單位: 分鐘)
        /// </summary>
        public int actualCompletion { get; set; } = 0;
    }

    /// <summary>
    /// 轉換當日訓練排程實際完成
    /// </summary>
    public class ConvertDayTrainingScheduleCardiorespiratoryFitnessActualCompletionVM
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 本日目標(天/週)
        /// </summary>
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 本日目標(分鐘/次)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 實際完成(單位: 分鐘)
        /// </summary>
        public int actualCompletion { get; set; }
    }

    /// <summary>
    /// 當日訓練排程-肌力與肌耐力實際完成
    /// </summary>
    public class DayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletionVM
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 本日目標(天/週)
        /// </summary>
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 本日目標(組/天)
        /// </summary>
        public string trainingUnitGroup { get; set; } = "";

        /// <summary>
        /// 本日目標(次/組)
        /// </summary>
        public string trainingUnitNumber { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 共完成次
        /// </summary>
        public string finish { get; set; } = "";
    }

    /// <summary>
    /// 轉換當日訓練排程-肌力與肌耐力實際完成
    /// </summary>
    public class ConvertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletionVM
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 本日目標(天/週)
        /// </summary>
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 本日目標(組/天)
        /// </summary>
        public string trainingUnitGroup { get; set; } = "";

        /// <summary>
        /// 本日目標(次/組)
        /// </summary>
        public string trainingUnitNumber { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 共完成次
        /// </summary>
        public string finish { get; set; } = "";
    }

    /// <summary>
    /// 當日訓練排程-柔軟度實際完成
    /// </summary>
    public class DayTrainingScheduleSoftnessActualCompletionVM
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 本日目標(天/週)
        /// </summary>
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 本日目標(分鐘/次)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 實際完成(單位: 分鐘)
        /// </summary>
        public int actualCompletion { get; set; } = 0;
    }

    /// <summary>
    /// 轉換當日訓練排程-柔軟度實際完成
    /// </summary>
    public class ConvertDayTrainingScheduleSoftnessActualCompletionVM
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 本日目標(天/週)
        /// </summary>
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 本日目標(分鐘/次)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 實際完成(單位: 分鐘)
        /// </summary>
        public int actualCompletion { get; set; } = 0;
    }

    /// <summary>
    /// 當日訓練排程-平衡實際完成
    /// </summary>
    public class DayTrainingScheduleBalanceActualCompletionVM
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 本日目標(天/週)
        /// </summary>
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 本日目標(分鐘/次)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 實際完成(單位: 分鐘)
        /// </summary>
        public int actualCompletion { get; set; } = 0;
    }

    /// <summary>
    /// 轉換當日訓練排程-平衡實際完成
    /// </summary>
    public class ConvertDayTrainingScheduleBalanceActualCompletionVM
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 本日目標(天/週)
        /// </summary>
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 本日目標(分鐘/次)
        /// </summary>
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 實際完成(單位: 分鐘)
        /// </summary>
        public int actualCompletion { get; set; } = 0;
    }

    /// <summary>
    /// 檢查排程資料是否存在 回傳模型
    /// </summary>
    public class InspactTrainingScheduleDataVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動類型
        /// </summary>
        public int trainingType { get; set; } = 0;

        /// <summary>
        /// 排程日期
        /// </summary>
        public string scheduleDate { get; set; } = "";

        /// <summary>
        /// 實際完成次數
        /// </summary>
        public int actualCompletion { get; set; } = 0;

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 取得訓練排程報表 回傳模型
    /// </summary>
    public class GetBeforeTrainingScheduleReportVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 排程時間
        /// </summary>
        public string scheduleDate { get; set; } = "";

        /// <summary>
        /// 目標時間
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 實際時間
        /// </summary>
        public int actualCompletion { get; set; } = 0;
    }

    /// <summary>
    /// 檢查訓練排程天數和運動處方天數是否一致 回傳模型
    /// </summary>
    public class InspaceTrainingScheduleAndExercisePrescriptionDayContainsVM
    {
        public int exerciseFrequency { get; set; } = 0;
    }
}
