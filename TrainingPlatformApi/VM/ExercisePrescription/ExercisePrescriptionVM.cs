﻿namespace TrainingPlatformApi.VM.ExercisePrescription
{
    /// <summary>
    /// 檢查今日運動處方-訓練階段
    /// </summary>
    public class InspactTodayExercisePrescriptionTrainingStage
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 開始階段(單位: 周)
        /// </summary>
        public int firstStage { get; set; } = 0;

        /// <summary>
        /// 改善階段(單位: 周)
        /// </summary>
        public int middleStage { get; set; } = 0;

        /// <summary>
        /// 設定起始日期
        /// </summary>
        public string startDate { get; set; } = "";

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 檢查今日運動處方-心肺適能 回傳模型
    /// </summary>
    public class InspactTodayExercisePrescriptionCardiorespiratoryFitnessVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動項目(字串陣列)
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 運動強度
        /// </summary>
        public int exerciseIntensity { get; set; } = 0;

        /// <summary>
        /// 訓練頻率(單位: 天/周)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 紀錄修改人 id
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 檢查今日運動處方-肌力與肌耐力 回傳模型
    /// </summary>
    public class InspactTodayExercisePrescriptionMuscleStrengthAndMuscleEnduranceVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public int exerciseIntensity { get; set; } = 0;

        /// <summary>
        /// 訓練頻率(單位: 天/周)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練單位(組/台)
        /// </summary>
        public int trainingUnitGroup { get; set; } = 0;

        /// <summary>
        /// 訓練單位(次/組)
        /// </summary>
        public int trainingUnitNumber { get; set; } = 0;

        /// <summary>
        /// 加強訓練部位
        /// </summary>
        public string trainingArea { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 紀錄修改人 id
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 檢查今日運動處方-柔軟度 回傳模型
    /// </summary>
    public class InspactTodayExercisePrescriptionSoftnessVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動訓練項目(字串陣列)
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 運動強度
        /// </summary>
        public int exerciseIntensity { get; set; } = 0;

        /// <summary>
        /// 運練頻率(天/周)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    public class InspactTodayExercisePrescriptionBalanceVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動訓練項目(字串陣列)
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 運動強度
        /// </summary>
        public int exerciseIntensity { get; set; } = 0;

        /// <summary>
        /// 運練頻率(天/周)
        /// </summary>
        public int exerciseFrequency { get; set; } = 0;

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }
}
