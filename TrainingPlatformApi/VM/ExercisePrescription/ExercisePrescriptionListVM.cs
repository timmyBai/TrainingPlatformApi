﻿namespace TrainingPlatformApi.VM.ExercisePrescription
{
    /// <summary>
    /// 運動處方-心肺適能歷史資料
    /// </summary>
    public class ExercisePrescriptionCardiorespiratoryFitnessHistoryVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 訊練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 運動處方-心肺適能群組
    /// </summary>
    public class ExercisePrescriptionCardiorespiratoryFitnessGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 訓練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 運動處方-心肺適能子群組
        /// </summary>
        public List<ExercisePrescriptionCardiorespiratoryFitnessGroupChildren> children { get; set; } = new List<ExercisePrescriptionCardiorespiratoryFitnessGroupChildren>();
    }

    /// <summary>
    /// 運動處方-心肺適能子群組
    /// </summary>
    public class ExercisePrescriptionCardiorespiratoryFitnessGroupChildren
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 訊練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 運動處方-肌力與肌耐力歷史資料
    /// </summary>
    public class ExercisePrescriptionMuscleStrengthAndMuscleEnduranceHistoryVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訊練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練部位
        /// </summary>
        public string trainingArea { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 運動處方-肌力與肌耐力群組
    /// </summary>
    public class ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訊練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練部位
        /// </summary>
        public List<string> trainingArea { get; set; } = new List<string>();

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 運動處方-肌力與肌耐力子群組
        /// </summary>
        public List<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupChildrenVM> children { get; set; } = new List<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupChildrenVM>();
    }

    /// <summary>
    /// 運動處方-肌力與肌耐力子群組
    /// </summary>
    public class ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訊練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練部位
        /// </summary>
        public List<string> trainingArea { get; set; } = new List<string>();

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 運動處方-柔軟度歷史紀錄
    /// </summary>
    public class ExercisePrescriptionSoftnessHistoryVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 訓練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";
        
        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 運動處方-柔軟度群組
    /// </summary>
    public class ExercisePrescriptionSoftnessGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 訓練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 運動處方-柔軟度子群組
        /// </summary>
        public List<ExercisePrescriptionSoftnessGroupChildrenVM> children { get; set; } = new List<ExercisePrescriptionSoftnessGroupChildrenVM>();
    }

    /// <summary>
    /// 運動處方-柔軟度子群組
    /// </summary>
    public class ExercisePrescriptionSoftnessGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 訓練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 運動處方-平衡歷史資料
    /// </summary>
    public class ExercisePrescriptionBalanceHistoryVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public string sports { get; set; } = "";

        /// <summary>
        /// 運動頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 運動處方-平衡群組
    /// </summary>
    public class ExercisePrescriptionBalanceGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 運動頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 運動處方-平衡子群組
        /// </summary>
        public List<ExercisePrescriptionBalanceGroupChildrenVM> children { get; set; } = new List<ExercisePrescriptionBalanceGroupChildrenVM>();
    }

    public class ExercisePrescriptionBalanceGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 運動頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        public int trainingUnitMinute { get; set; } = 0;

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 運動處方清單
    /// </summary>
    public class ExercisePrescriptionListVM
    {
        /// <summary>
        /// 運動處方-心肺適能群組
        /// </summary>
        public ExercisePrescriptionCardiorespiratoryFitnessGroupVM exercisePrescriptionCardiorespiratoryFitnessGroup { get; set; } = new ExercisePrescriptionCardiorespiratoryFitnessGroupVM();

        /// <summary>
        /// 運動處方-肌力與肌耐力群組
        /// </summary>
        public ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupVM exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup { get; set; } = new ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupVM();

        /// <summary>
        /// 運動處方-柔軟度群組
        /// </summary>
        public ExercisePrescriptionSoftnessGroupVM exercisePrescriptionSoftnessGroup { get; set; } = new ExercisePrescriptionSoftnessGroupVM();

        /// <summary>
        /// 運動處方-平衡
        /// </summary>
        public ExercisePrescriptionBalanceGroupVM exercisePrescriptionBalanceGroup { get; set; } = new ExercisePrescriptionBalanceGroupVM();

        public ExercisePrescriptionListVM()
        {
            exercisePrescriptionCardiorespiratoryFitnessGroup = new ExercisePrescriptionCardiorespiratoryFitnessGroupVM();
            exercisePrescriptionCardiorespiratoryFitnessGroup.children = new List<ExercisePrescriptionCardiorespiratoryFitnessGroupChildren>();

            exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup = new ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupVM();
            exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.children = new List<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupChildrenVM>();

            exercisePrescriptionSoftnessGroup = new ExercisePrescriptionSoftnessGroupVM();
            exercisePrescriptionSoftnessGroup.children = new List<ExercisePrescriptionSoftnessGroupChildrenVM>();

            exercisePrescriptionBalanceGroup = new ExercisePrescriptionBalanceGroupVM();
            exercisePrescriptionBalanceGroup.children = new List<ExercisePrescriptionBalanceGroupChildrenVM>();
        }
    }

    public class LatestExercisePrescriptionListVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 開始階段週
        /// </summary>
        public int firstStage { get; set; } = 0;

        /// <summary>
        /// 改善階段週
        /// </summary>
        public int middleStage { get; set; } = 0;

        /// <summary>
        /// 起始日期
        /// </summary>
        public string startDate { get; set; } = "";

        /// <summary>
        /// 目前階段週
        /// </summary>
        public int week { get; set; } = 0;

        /// <summary>
        /// 目前階段週文字
        /// </summary>
        public string weekText { get; set; } = "";
    }
}
