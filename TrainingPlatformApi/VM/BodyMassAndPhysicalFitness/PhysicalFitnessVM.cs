namespace TrainingPlatformApi.VM.BodyMassAndPhysicalFitness
{
    /// <summary>
    /// 最新一筆 體適能-心肺適能 回傳模型
    /// </summary>
    public class InspactTodayPhysicalFitnessCardiorespiratoryFitnessVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; }

        /// <summary>
        /// 登階測驗 測量第一次脈搏
        /// </summary>
        public int pulseRateOne { get; set; } = 0;

        /// <summary>
        /// 登階測驗 測量第二次脈搏
        /// </summary>
        public int pulseRateTwo { get; set; } = 0;

        /// <summary>
        /// 登階測驗 測量第三次脈搏
        /// </summary>
        public int pulseRateThree { get; set; } = 0;

        /// <summary>
        /// 原地站立抬膝
        /// </summary>
        public int kneeLiftFrequency { get; set; } = 0;

        /// <summary>
        /// 紀錄修改人 id
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新一筆 體適能-肌力與肌耐力 回傳模型
    /// </summary>
    public class InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEnduranceVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 屈膝仰臥起坐次數
        /// </summary>
        public int kneeCrunchesFrequency { get; set; } = 0;

        /// <summary>
        /// 手臂彎舉 次數
        /// </summary>
        public int armCurlUpperBodyFrequency { get; set; } = 0;

        /// <summary>
        /// 起立坐下 次數
        /// </summary>
        public int armCurlLowerBodyFrequency { get; set; } = 0;

        /// <summary>
        /// 紀錄修改人 id
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新一筆 體適能-柔軟度 回傳模型
    /// </summary>
    public class InspactTodayPhysicalFitnessSoftnessVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 坐姿體前彎第一次距離
        /// </summary>
        public double seatedForwardBendOne { get; set; } = 0.0;

        /// <summary>
        /// 坐姿體前彎第二次距離
        /// </summary>
        public double seatedForwardBendTwo { get; set; } = 0.0;

        /// <summary>
        /// 坐姿體前彎第三次距離
        /// </summary>
        public double seatedForwardBendThree { get; set; } = 0.0;

        /// <summary>
        /// 椅子坐姿體前彎第一次距離
        /// </summary>
        public double chairSeatedForwardBendOne { get; set; } = 0.0;

        /// <summary>
        /// 椅子坐姿體前彎第二次距離
        /// </summary>
        public double chairSeatedForwardBendTwo { get; set; } = 0.0;

        /// <summary>
        /// 椅子坐姿體前彎第三次距離
        /// </summary>
        public double chairSeatedForwardBendThree { get; set; } = 0.0;

        /// <summary>
        /// 拉背測驗第一次距離
        /// </summary>
        public double pullBackTestOne { get; set; } = 0.0;

        /// <summary>
        /// 拉背測驗第二次距離
        /// </summary>
        public double pullBackTestTwo { get; set; } = 0.0;

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新一筆 體適能-平衡 回傳模型
    /// </summary>
    public class InspactTodayPhysicalFitnessBalanceVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 壯年開眼足立
        /// </summary>
        public double adultEyeOpeningMonopodSecond { get; set; } = 0.0;

        /// <summary>
        /// 老年開眼足立
        /// </summary>
        public double elderlyEyeOpeningMonopodSecond { get; set; } = 0.0;

        /// <summary>
        /// 坐立繞物第一次秒數
        /// </summary>
        public double sittingAroundOneSecond { get; set; } = 0.0;

        /// <summary>
        /// 坐立繞物第二次秒數
        /// </summary>
        public double sittingAroundTwoSecond { get; set; } = 0.0;

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }
}
