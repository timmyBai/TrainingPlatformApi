namespace TrainingPlatformApi.VM.BodyMassAndPhysicalFitness
{
    /// <summary>
    /// 身體組成 回傳模型
    /// </summary>
    public class BodyMassBmiVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// bmi 指數
        /// </summary>
        public double bmi { get; set; } = 0.0;

        /// <summary>
        /// bmi 評價等級
        /// </summary>
        public string bmiCommentLevel { get; set; } = "";

        /// <summary>
        /// 體脂肪 指數
        /// </summary>
        public double bodyFat { get; set; } = 0.0;

        /// <summary>
        /// 體脂肪 評價等級
        /// </summary>
        public string bodyFatCommentLevel { get; set; } = "";

        /// <summary>
        /// 內臟脂肪 指數
        /// </summary>
        public double visceralFat { get; set; } = 0.0;

        /// <summary>
        /// 內臟脂肪 評價等級
        /// </summary>
        public string visceralFatCommentLevel { get; set; } = "";

        /// <summary>
        /// 骨骼心肌率
        /// </summary>
        public double skeletalMuscleRate { get; set; } = 0.0;

        /// <summary>
        /// 骨骼心肌率 評價等級
        /// </summary>
        public string skeletalMuscleRateCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 身體組成 回傳模型
    /// </summary>
    public class BodyMassBodyFatVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 體脂肪 指數
        /// </summary>
        public double bodyFat { get; set; } = 0.0;

        /// <summary>
        /// 體脂肪 評價等級
        /// </summary>
        public string bodyFatCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 內臟脂肪 回傳模型
    /// </summary>
    public class BodyMassVisceralFatVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 內臟脂肪 指數
        /// </summary>
        public double visceralFat { get; set; } = 0.0;

        /// <summary>
        /// 內臟脂肪 評價等級
        /// </summary>
        public string visceralFatCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    //  骨骼肌率 回傳模型
    /// </summary>
    public class BodyMassSkeletalMuscleRateVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 骨骼心肌率
        /// </summary>
        public double skeletalMuscleRate { get; set; } = 0.0;

        /// <summary>
        /// 骨骼心肌率 評價等級
        /// </summary>
        public string skeletalMuscleRateCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 身體組成 bmi 群組 回傳模型
    /// </summary>
    public class BmiGroupVM
    {
        /// <summary>
        /// bmi 指數
        /// </summary>
        public double bmi { get; set; } = 0.0;
        
        /// <summary>
        /// bmi 指數評語
        /// </summary>
        public string bmiCommentLevel { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// bmi 子群組清單
        /// </summary>
        public List<BmiGroupChildrenVM> children { get; set; } = new List<BmiGroupChildrenVM>();
    }

    /// <summary>
    /// 身體組成 bmi 子群組 回傳模型
    /// </summary>
    public class BmiGroupChildrenVM
    {
        /// <summary>
        /// bmi 指數
        /// </summary>
        public double bmi { get; set; } = 0.0;

        /// <summary>
        /// bmi 指數評語
        /// </summary>
        public string bmiCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 體脂肪 群組 回傳模型
    /// </summary>
    public class BodyFatGroupVM
    {
        /// <summary>
        /// 體脂肪 指數
        /// </summary>
        public double bodyFat { get; set; } = 0.0;

        /// <summary>
        /// 體脂肪 評語
        /// </summary>
        public string bodyFatCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 體脂肪 子群組清單
        /// </summary>
        public List<BodyFatGroupChildrenVM> children { get; set; } = new List<BodyFatGroupChildrenVM>();
    }

    /// <summary>
    /// 體脂肪 子群組 回傳模型
    /// </summary>
    public class BodyFatGroupChildrenVM
    {
        /// <summary>
        /// 體脂肪 評語
        /// </summary>
        public double bodyFat { get; set; } = 0.0;

        /// <summary>
        /// 體脂肪 子群組清單
        /// </summary>
        public string bodyFatCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 內臟脂肪 群組 回傳模型
    /// </summary>
    public class VisceralFatGroupVM
    {
        /// <summary>
        /// 內臟脂肪 指數
        /// </summary>
        public double visceralFat { get; set; } = 0.0;

        /// <summary>
        /// 內臟脂肪 評語
        /// </summary>
        public string visceralFatCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 內臟脂肪 子群組清單
        /// </summary>
        public List<VisceralFatGroupChildrenVM> children { get; set; } = new List<VisceralFatGroupChildrenVM>();
    }

    /// <summary>
    /// 內臟脂肪 子群組 回傳模型
    /// </summary>
    public class VisceralFatGroupChildrenVM
    {
        /// <summary>
        /// 內臟脂肪 指數
        /// </summary>
        public double visceralFat { get; set; } = 0.0;

        /// <summary>
        /// 內臟脂肪 評語
        /// </summary>
        public string visceralFatCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 骨骼心肌率 群組 回傳模型
    /// </summary>
    public class SkeletalMuscleRateGroupVM
    {
        /// <summary>
        /// 骨骼心肌率 指數
        /// </summary>
        public double skeletalMuscleRate { get; set; } = 0.0;

        /// <summary>
        /// 骨骼心肌率 評語
        /// </summary>
        public string skeletalMuscleRateCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 骨骼心肌率 子群組
        /// </summary>
        public List<SkeletalMuscleRateGroupChildrenVM> children { get; set; } = new List<SkeletalMuscleRateGroupChildrenVM>();
    }

    /// <summary>
    /// 骨骼心肌率 子群組 回傳模型
    /// </summary>
    public class SkeletalMuscleRateGroupChildrenVM
    {
        /// <summary>
        /// 骨骼心肌率 指數
        /// </summary>
        public double skeletalMuscleRate { get; set; } = 0.0;

        /// <summary>
        /// 骨骼心肌率 評語
        /// </summary>
        public string skeletalMuscleRateCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 學員體適能訓練-心肺適能歷史資料 回傳模型
    /// </summary>
    public class PhysicalFitnessCardiorespiratoryFitnessHistoryVM
    {
        /// <summary>
        /// 心肺適能-登階測驗體力指數結果
        /// </summary>
        public double setUpTest { get; set; } = 0.0;

        /// <summary>
        /// 心肺適能-原地抬膝
        /// </summary>
        public int kneeLiftFrequency { get; set; } = 0;

        /// <summary>
        /// 體適能-心肺適能分數
        /// </summary>
        public int cardiorespiratoryFitness { get; set; } = 0;

        /// <summary>
        /// 體適能-心肺適能評價
        /// </summary>
        public string cardiorespiratoryFitnessCommentLevel { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 學員體適能訓練-心肺適能 群組 回傳模型
    /// </summary>
    public class PhysicalFitnessCardiorespiratoryFitnessGroupVM
    {
        /// <summary>
        /// 體適能-心肺適能分數
        /// </summary>
        public int cardiorespiratoryFitnessFraction { get; set; } = 0;

        /// <summary>
        /// 體適能-心肺適能評語
        /// </summary>
        public string cardiorespiratoryFitnessComment { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
        
        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 體適能-心肺適能 子群組
        /// </summary>
        public List<PhysicalFitnessCardiorespiratoryFitnessGroupChildrenVM> children { get; set; } = new List<PhysicalFitnessCardiorespiratoryFitnessGroupChildrenVM>();
    }

    /// <summary>
    /// 學員體適能訓練-心肺適能 子群組 回傳模型
    /// </summary>
    public class PhysicalFitnessCardiorespiratoryFitnessGroupChildrenVM
    {
        /// <summary>
        /// 體適能-心肺適能分數
        /// </summary>
        public int cardiorespiratoryFitnessFraction { get; set; } = 0;

        /// <summary>
        /// 體適能-心肺適能評語
        /// </summary>
        public string cardiorespiratoryFitnessComment { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";
    }

    /// <summary>
    /// 學員體適能訓練-肌力與肌耐力歷史資料 回傳模型
    /// </summary>
    public class PhysicalFitnessMuscleStrengthAndMuscleEnduranceHistoryVM
    {
        /// <summary>
        /// 屈膝仰臥起坐
        /// </summary>
        public int kneeCrunchesFrequency { get; set; } = 0;

        /// <summary>
        /// 三十秒手臂彎舉
        /// </summary>
        public int armCurlUpperBodyFrequency { get; set; } = 0;

        /// <summary>
        /// 三十秒起立坐下
        /// </summary>
        public int armCurlLowerBodyFrequency { get; set; } = 0;

        /// <summary>
        /// 體適能-肌力與肌耐力分數
        /// </summary>
        public int muscleStrengthAndMuscleEnduranceFitness { get; set; } = 0;

        /// <summary>
        /// 體適能-肌立與肌耐力評價
        /// </summary>
        public string muscleStrengthAndMuscleEnduranceCommentLevel { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";
    }

    /// <summary>
    /// 學員體適能訓練-肌力與肌耐力群組 回傳模型
    /// </summary>
    public class PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupVM
    {
        /// <summary>
        /// 體適能-肌力與肌耐力分數
        /// </summary>
        public int muscleStrengthAndMuscleEnduranceFitness { get; set; } = 0;

        /// <summary>
        /// 體適能-肌力與肌耐力評語
        /// </summary>
        public string muscleStrengthAndMuscleEnduranceComment { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 體適能-肌力與肌耐力 子群組
        /// </summary>
        public List<PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupChildrenVM> children { get; set; } = new List<PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupChildrenVM>();
    }

    /// <summary>
    /// 學員體適能訓練-肌力與肌耐力 子群組 回傳模型
    /// </summary>
    public class PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupChildrenVM
    {
        /// <summary>
        /// 體適能-肌力與肌耐力分數
        /// </summary>
        public int muscleStrengthAndMuscleEnduranceFitness { get; set; } = 0;

        /// <summary>
        /// 體適能-肌力與肌耐力評語
        /// </summary>
        public string muscleStrengthAndMuscleEnduranceComment { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";
    }

    /// <summary>
    /// 學員體適能訓練-柔軟度歷史資料 回傳模型
    /// </summary>
    public class PhysicalFitnessSoftnessHistoryVM
    {
        /// <summary>
        /// 坐姿體前彎第一次距離
        /// </summary>
        public double seatedForwardBendOne { get; set; } = 0.0;

        /// <summary>
        /// 坐姿體前彎第二次距離
        /// </summary>
        public double seatedForwardBendTwo { get; set; } = 0.0;

        /// <summary>
        /// 坐姿體前彎第三次距離
        /// </summary>
        public double seatedForwardBendThree { get; set; } = 0.0;

        /// <summary>
        /// 椅子坐姿體前彎第一次距離
        /// </summary>
        public double chairSeatedForwardBendOne { get; set; } = 0.0;

        /// <summary>
        /// 椅子坐姿體前彎第二次距離
        /// </summary>
        public double chairSeatedForwardBendTwo { get; set; } = 0.0;

        /// <summary>
        /// 椅子坐姿體前彎第三次距離
        /// </summary>
        public double chairSeatedForwardBendThree { get; set; } = 0.0;

        /// <summary>
        /// 拉背測驗第一次距離
        /// </summary>
        public double pullBackTestOne { get; set; } = 0.0;

        /// <summary>
        /// 拉背測驗第二次距離
        /// </summary>
        public double pullBackTestTwo { get; set; } = 0.0;

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 體適能-柔軟度分數
        /// </summary>
        public int softnessFitness { get; set; } = 0;

        /// <summary>
        /// 體適能-柔軟度評語
        /// </summary>
        public string softnessComment { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";
    }

    /// <summary>
    /// 學員體適能訓練-柔軟度群組 回傳模型
    /// </summary>
    public class PhysicalFitnessSoftnessGroupVM
    {
        /// <summary>
        /// 體適能-柔軟度分數
        /// </summary>
        public int softnessFitness { get; set; } = 0;

        /// <summary>
        /// 體適能-柔軟度評語
        /// </summary>
        public string softnessComment { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 體適能-柔軟度 子群組
        /// </summary>
        public List<PhysicalFitnessSoftnessGroupChildrenVM> children { get; set; } = new List<PhysicalFitnessSoftnessGroupChildrenVM>();
    }

    /// <summary>
    /// 學員體適能訓練-柔軟度 子群組 回傳模型
    /// </summary>
    public class PhysicalFitnessSoftnessGroupChildrenVM
    {
        /// <summary>
        /// 體適能-柔軟度分數
        /// </summary>
        public int softnessFitness { get; set; } = 0;

        /// <summary>
        /// 體適能-柔軟度評語
        /// </summary>
        public string softnessComment { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";
    }

    /// <summary>
    /// 學員體適能訓練-平衡歷史資料 回傳模型
    /// </summary>
    public class PhysicalFitnessBalanceHistoryVM
    {
        /// <summary>
        /// 壯年開眼足立
        /// </summary>
        public double adultEyeOpeningMonopodSecond { get; set; } = 0.0;

        /// <summary>
        /// 老年開眼足立
        /// </summary>
        public double elderlyEyeOpeningMonopodSecond { get; set; } = 0.0;

        /// <summary>
        /// 坐立繞物第一次秒數
        /// </summary>
        public double sittingAroundOneSecond { get; set; } = 0.0;

        /// <summary>
        /// 坐立繞物第二次秒數
        /// </summary>
        public double sittingAroundTwoSecond { get; set; } = 0.0;

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 體適能-平衡分數
        /// </summary>
        public int balanceFitness { get; set; } = 0;

        /// <summary>
        /// 體適能-平衡評語
        /// </summary>
        public string balanceComment { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";
    }

    /// <summary>
    /// 學員體適能訓練-平衡 群組 回傳模型
    /// </summary>
    public class PhysicalFitnessBalanceGroupVM
    {
        /// <summary>
        /// 體適能-平衡分數
        /// </summary>
        public double balanceFitness { get; set; } = 0;

        /// <summary>
        /// 體適能-平衡評語
        /// </summary>
        public string balanceComment { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 體適能-平衡 子群組
        /// </summary>
        public List<PhysicalFitnessBalanceGroupChildrenVM> children { get; set; } = new List<PhysicalFitnessBalanceGroupChildrenVM>();
    }

    /// <summary>
    /// 學員體適能訓練-平衡 子群組 回傳模型
    /// </summary>
    public class PhysicalFitnessBalanceGroupChildrenVM
    {
        /// <summary>
        /// 體適能-平衡分數
        /// </summary>
        public double balanceFitness { get; set; } = 0.0;

        /// <summary>
        /// 體適能-平衡評語
        /// </summary>
        public string balanceComment { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 身體組成與體適能評價
    /// </summary>
    public class BodyCompositionAndPhysicalFitnessVM
    {
        /// <summary>
        /// bmi 群組
        /// </summary>
        public BmiGroupVM bmiGroup { get; set; } = new BmiGroupVM();

        /// <summary>
        /// 體脂肪群組
        /// </summary>
        public BodyFatGroupVM bodyFatGroup { get; set; } = new BodyFatGroupVM();

        /// <summary>
        /// 內臟肌率群組
        /// </summary>
        public VisceralFatGroupVM visceralFatGroup { get; set; } = new VisceralFatGroupVM();

        /// <summary>
        /// 骨骼心肌率群組
        /// </summary>
        public SkeletalMuscleRateGroupVM skeletalMuscleRateGroup { get; set; } = new SkeletalMuscleRateGroupVM();

        /// <summary>
        /// 體適能心肺適能群組
        /// </summary>
        public PhysicalFitnessCardiorespiratoryFitnessGroupVM physicalFitnessCardiorespiratoryFitnessGroup { get; set; } = new PhysicalFitnessCardiorespiratoryFitnessGroupVM();

        /// <summary>
        /// 體適能肌力與肌耐力群組
        /// </summary>
        public PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupVM physicalFitnessMuscleStrengthAndMuscleEnduranceGroup { get; set; } = new PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupVM();

        /// <summary>
        /// 體適能柔軟度群組
        /// </summary>
        public PhysicalFitnessSoftnessGroupVM physicalFitnessSoftnessGroup { get; set; } = new PhysicalFitnessSoftnessGroupVM();

        /// <summary>
        /// 體適能平衡群組
        /// </summary>
        public PhysicalFitnessBalanceGroupVM physicalFitnessBalanceGroup { get; set; } = new PhysicalFitnessBalanceGroupVM();

        public BodyCompositionAndPhysicalFitnessVM()
        {
            bmiGroup = new BmiGroupVM();
            bmiGroup.children = new List<BmiGroupChildrenVM>();

            bodyFatGroup = new BodyFatGroupVM();
            bodyFatGroup.children = new List<BodyFatGroupChildrenVM>();

            visceralFatGroup = new VisceralFatGroupVM();
            visceralFatGroup.children = new List<VisceralFatGroupChildrenVM>();

            skeletalMuscleRateGroup = new SkeletalMuscleRateGroupVM();
            skeletalMuscleRateGroup.children = new List<SkeletalMuscleRateGroupChildrenVM>();

            physicalFitnessCardiorespiratoryFitnessGroup = new PhysicalFitnessCardiorespiratoryFitnessGroupVM();
            physicalFitnessCardiorespiratoryFitnessGroup.children = new List<PhysicalFitnessCardiorespiratoryFitnessGroupChildrenVM>();

            physicalFitnessMuscleStrengthAndMuscleEnduranceGroup = new PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupVM();
            physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.children = new List<PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupChildrenVM>();

            physicalFitnessSoftnessGroup = new PhysicalFitnessSoftnessGroupVM();
            physicalFitnessSoftnessGroup.children = new List<PhysicalFitnessSoftnessGroupChildrenVM>();

            physicalFitnessBalanceGroup = new PhysicalFitnessBalanceGroupVM();
            physicalFitnessBalanceGroup.children = new List<PhysicalFitnessBalanceGroupChildrenVM>();
        }
    }
}
