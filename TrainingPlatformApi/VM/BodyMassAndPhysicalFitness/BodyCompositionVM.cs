namespace TrainingPlatformApi.VM.BodyMassAndPhysicalFitness
{
    /// <summary>
    /// 最新一筆身體組成 bmi 回傳模型
    /// </summary>
    public class TodayBodyCompositionBmiVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// bmi 指數
        /// </summary>
        public double bmi { get; set; } = 0.0;

        /// <summary>
        /// 身體組成備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新一筆身體組成 體脂肪 回傳模型
    /// </summary>
    public class TodayBodyCompositionBodyFatVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 體脂肪指數
        /// </summary>
        public double bodyFat { get; set; } = 0.0;

        /// <summary>
        /// 身體組成備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新一筆身體組成 內臟脂肪 回傳模型
    /// </summary>
    public class TodayBodyCompositionVisceralFatVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 內臟脂肪
        /// </summary>
        public double visceralFat { get; set; } = 0.0;

        /// <summary>
        /// 身體組成備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新一筆身體組成 骨骼心機率 回傳模型
    /// </summary>
    public class TodayBodyCompositionSkeletalMuscleRateVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 骨骼心肌率
        /// </summary>
        public double skeletalMuscleRate { get; set; } = 0.0;

        /// <summary>
        /// 身體組成備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }
}
