﻿namespace TrainingPlatformApi.VM.InstrumentSettings
{
    /// <summary>
    /// 儀器設定運動表現清單 回傳模型
    /// </summary>
    public class InstrumentSettingSportsPerformanceListVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";
        
        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }
    
    /// <summary>
    /// 機器編號一群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeOneGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號一子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeOneGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeOneGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號一子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeOneGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 機器編號二群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeTwoGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號二子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeTwoGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeTwoGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號二子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeTwoGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 機器編號三群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeThreeGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號三子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeThreeGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeThreeGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號三子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeThreeGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 機器編號四群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeFourGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號四子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeFourGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeFourGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號四子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeFourGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 機器編號五群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeFiveGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號五子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeFiveGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeFiveGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號五子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeFiveGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 機器編號六群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeSixGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號六子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeSixGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeSixGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號六子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeSixGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 機器編號七群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeSevenGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號七子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeSevenGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeSevenGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號七子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeSevenGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 機器編號八群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeEightGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號八子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeEightGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeEightGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號八子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeEightGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 機器編號九群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeNineGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號九子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeNineGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeNineGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號九子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeNineGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 機器編號十群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeTenGroupVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";

        /// <summary>
        /// 機器編號十子群組
        /// </summary>
        public List<InstrumentSettingMachinaryCodeTenGroupChildrenVM> children { get; set; } = new List<InstrumentSettingMachinaryCodeTenGroupChildrenVM>();
    }

    /// <summary>
    /// 機器編號十子群組
    /// </summary>
    public class InstrumentSettingMachinaryCodeTenGroupChildrenVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 最大活動範圍
        /// </summary>
        public string maxRange { get; set; } = "";

        /// <summary>
        /// 功率
        /// </summary>
        public string power { get; set; } = "";

        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 儀器設定
    /// </summary>
    public class InstrumentSettingListVM
    {
        public InstrumentSettingMachinaryCodeOneGroupVM machinaryCodeOneGroup { get; set; } = new InstrumentSettingMachinaryCodeOneGroupVM();

        public InstrumentSettingMachinaryCodeTwoGroupVM machinaryCodeTwoGroup { get; set; } = new InstrumentSettingMachinaryCodeTwoGroupVM();

        public InstrumentSettingMachinaryCodeThreeGroupVM machinaryCodeThreeGroup { get; set; } = new InstrumentSettingMachinaryCodeThreeGroupVM();

        public InstrumentSettingMachinaryCodeFourGroupVM machinaryCodeFourGroup { get; set; } = new InstrumentSettingMachinaryCodeFourGroupVM();

        public InstrumentSettingMachinaryCodeFiveGroupVM machinaryCodeFiveGroup { get; set; } = new InstrumentSettingMachinaryCodeFiveGroupVM();

        public InstrumentSettingMachinaryCodeSixGroupVM machinaryCodeSixGroup { get; set; } = new InstrumentSettingMachinaryCodeSixGroupVM();

        public InstrumentSettingMachinaryCodeSevenGroupVM machinaryCodeSevenGroup { get; set; } = new InstrumentSettingMachinaryCodeSevenGroupVM();

        public InstrumentSettingMachinaryCodeEightGroupVM machinaryCodeEightGroup { get; set; } = new InstrumentSettingMachinaryCodeEightGroupVM();

        public InstrumentSettingMachinaryCodeNineGroupVM machinaryCodeNineGroup { get; set; } = new InstrumentSettingMachinaryCodeNineGroupVM();

        public InstrumentSettingMachinaryCodeTenGroupVM machinaryCodeTenGroup { get; set; } = new InstrumentSettingMachinaryCodeTenGroupVM();

        public InstrumentSettingListVM()
        {
            machinaryCodeOneGroup = new InstrumentSettingMachinaryCodeOneGroupVM();
            machinaryCodeOneGroup.children = new List<InstrumentSettingMachinaryCodeOneGroupChildrenVM>();

            machinaryCodeTwoGroup = new InstrumentSettingMachinaryCodeTwoGroupVM();
            machinaryCodeTwoGroup.children = new List<InstrumentSettingMachinaryCodeTwoGroupChildrenVM>();

            machinaryCodeThreeGroup = new InstrumentSettingMachinaryCodeThreeGroupVM();
            machinaryCodeThreeGroup.children = new List<InstrumentSettingMachinaryCodeThreeGroupChildrenVM>();

            machinaryCodeFourGroup = new InstrumentSettingMachinaryCodeFourGroupVM();
            machinaryCodeFourGroup.children = new List<InstrumentSettingMachinaryCodeFourGroupChildrenVM>();

            machinaryCodeFiveGroup = new InstrumentSettingMachinaryCodeFiveGroupVM();
            machinaryCodeFiveGroup.children = new List<InstrumentSettingMachinaryCodeFiveGroupChildrenVM>();

            machinaryCodeSixGroup = new InstrumentSettingMachinaryCodeSixGroupVM();
            machinaryCodeSixGroup.children = new List<InstrumentSettingMachinaryCodeSixGroupChildrenVM>();

            machinaryCodeSevenGroup = new InstrumentSettingMachinaryCodeSevenGroupVM();
            machinaryCodeSevenGroup.children = new List<InstrumentSettingMachinaryCodeSevenGroupChildrenVM>();

            machinaryCodeEightGroup = new InstrumentSettingMachinaryCodeEightGroupVM();
            machinaryCodeEightGroup.children = new List<InstrumentSettingMachinaryCodeEightGroupChildrenVM>();

            machinaryCodeNineGroup = new InstrumentSettingMachinaryCodeNineGroupVM();
            machinaryCodeNineGroup.children = new List<InstrumentSettingMachinaryCodeNineGroupChildrenVM>();

            machinaryCodeTenGroup = new InstrumentSettingMachinaryCodeTenGroupVM();
            machinaryCodeTenGroup.children = new List<InstrumentSettingMachinaryCodeTenGroupChildrenVM>();
        }
    }
}
