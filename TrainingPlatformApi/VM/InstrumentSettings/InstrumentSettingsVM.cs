﻿namespace TrainingPlatformApi.VM.InstrumentSettings
{
    /// <summary>
    /// 單日報表模型
    /// </summary>
    public class GetSingleDayReportVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 機器名稱
        /// </summary>
        public string machinaryCodeName { get; set; } = "";

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;
        
        /// <summary>
        /// 訓練次數
        /// </summary>
        public int finish { get; set; } = 0;
        
        /// <summary>
        /// 失誤次數
        /// </summary>
        public int mistake { get; set; } = 0;
        
        /// <summary>
        /// 功率
        /// </summary>
        public int power { get; set; } = 0;

        /// <summary>
        /// 訓練速度百分比
        /// </summary>
        public string speed { get; set; } = "";

        /// <summary>
        /// 最大速度
        /// </summary>
        public int maxSpeed { get; set; } = 0;

        /// <summary>
        /// 自覺努力
        /// </summary>
        public int consciousEffort { get; set; } = 0;

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
    }

    /// <summary>
    /// 儀器設定最大運動表現
    /// </summary>
    public class InspactInstrumentSettingsSportsPerformanceVM
    {
        /// <summary>
        /// 主鍵
        /// </summary>
        public int instrumentSettingsId { get; set; } = 0;
        
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;
        
        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;
        
        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;
        
        /// <summary>
        /// 完成次數
        /// </summary>
        public int finish { get; set; } = 0;
        
        /// <summary>
        /// 失誤次數
        /// </summary>
        public int mistake { get; set; } = 0;
        
        /// <summary>
        /// 活動範圍
        /// </summary>
        public int maxRange { get; set; } = 0;
        
        /// <summary>
        /// 功率
        /// </summary>
        public int power { get; set; } = 0;

        /// <summary>
        /// 速度陣列
        /// </summary>
        public string speed { get; set; } = "";

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
        
        /// <summary>
        /// 訓練時間
        /// </summary>
        public string trainingTime { get; set; } = "";
    }

    /// <summary>
    /// 儀器設定最大運動表現備註
    /// </summary>
    public class InspactInstrumentSettingsSportsPerformanceMarkVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 訓練日期
        /// </summary>
        public string trainingDate { get; set; } = "";
        
        /// <summary>
        /// 備註
        /// </summary>
        public string mark { get; set; } = "";

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改人
        /// </summary>
        public int updateRecordId { get; set; } = 0;

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }
}
