namespace TrainingPlatformApi.VM.Student
{
    /// <summary>
    /// 學員清單
    /// </summary>
    public class StudentListVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 學員名稱
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 學員性別
        /// </summary>
        public string gender { get; set; } = "";

        /// <summary>
        /// 學員性別序號男為 1、女為 0
        /// </summary>
        public int genderMequence { get; set; } = 0;

        /// <summary>
        /// 身高與體重(體態)
        /// </summary>
        public string posture { get; set; } = "";

        /// <summary>
        /// 年齡
        /// </summary>
        public int age { get; set; } = 0;

        /// <summary>
        /// 手機
        /// </summary>
        public string phone { get; set; } = "";

        /// <summary>
        /// 地址
        /// </summary>
        public string address { get; set; } = "";

        /// <summary>
        /// 信箱
        /// </summary>
        public string email { get; set; } = "";

        /// <summary>
        /// 圖片網址
        /// </summary>
        public string imageUrl { get; set; } = "";
    }

    /// <summary>
    /// 最新學員清單彙總
    /// </summary>
    public class LatestStudentListVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;
        
        /// <summary>
        /// 學員名稱
        /// </summary>
        public string name { get; set; } = "";
        
        /// <summary>
        /// 學員性別
        /// </summary>
        public string gender { get; set; } = "";

        /// <summary>
        /// 學員性別序號男為 1、女為 0
        /// </summary>
        public int genderMequence { get; set; } = 0;

        /// <summary>
        /// 身高與體重(體態)
        /// </summary>
        public string posture { get; set; } = "";
        
        /// <summary>
        /// 年齡
        /// </summary>
        public int age { get; set; } = 0;

        /// <summary>
        /// 手機
        /// </summary>
        public string phone { get; set; } = "";

        /// <summary>
        /// 地址
        /// </summary>
        public string address { get; set; } = "";

        /// <summary>
        /// 危險等級
        /// </summary>
        public string dangerGrading { get; set; } = "";

        /// <summary>
        /// 危險等級序號(低危險群為 1、中危險群為 2、高危險群為 3)
        /// </summary>
        public int dangerGradingMequence { get; set; } = 0;

        /// <summary>
        /// 疾病史
        /// </summary>
        public string medicalHistory { get; set; } = "";
        
        /// <summary>
        /// 疾病史序號(有疾病史為 1，沒有則為 0)
        /// </summary>
        public int medicalHistoryMequence { get; set; } = 0;

        /// <summary>
        /// 信箱
        /// </summary>
        public string email { get; set; } = "";

        /// <summary>
        /// 圖片網址
        /// </summary>
        public string imageUrl { get; set; } = "";
    }

    /// <summary>
    /// aha 和 ascsm 問卷
    /// </summary>
    public class AhaAndAcsmQuestionnaireVM
    {
        /// <summary>
        /// 填寫學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;
        
        /// <summary>
        /// 問卷 id
        /// </summary>
        public int questionnaireId { get; set; } = 0;
        
        /// <summary>
        /// 問卷部分
        /// </summary>
        public int part { get; set; } = 0;
        
        /// <summary>
        /// 子題序號
        /// </summary>
        public int subject { get; set; } = 0;
    }

    /// <summary>
    /// 檢查學員 回傳模型
    /// </summary>
    public class InspactStudentVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 學員性別
        /// </summary>
        public string gender { get; set; } = "";

        /// <summary>
        /// 學員年齡
        /// </summary>
        public int age { get; set; } = 0;

        /// <summary>
        /// 學員手機
        /// </summary>
        public string phone { get; set; } = "";
    }

    /// <summary>
    /// 學員資訊
    /// </summary>
    public class PhoneRepeatVM
    {
        /// <summary>
        /// 手機
        /// </summary>
        public string phone { get; set; } = "";
    }

    /// <summary>
    /// 今日體態資料 回傳模型
    /// </summary>
    public class TodayFirstBodyMassRecordVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 身高
        /// </summary>
        public double height { get; set; } = 0.0;
        
        /// <summary>
        /// 體重
        /// </summary>
        public double weight { get; set; } = 0.0;
        
        /// <summary>
        /// 建立紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";
        
        /// <summary>
        /// 建立紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 取得最新一筆學員資訊
    /// </summary>
    public class GetOnlyStudentInfoVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;
        
        /// <summary>
        /// 電話號碼
        /// </summary>
        public string phone { get; set; } = "";
        
        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; } = "";
        
        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; } = "";
        
        /// <summary>
        /// 年齡
        /// </summary>
        public int age { get; set; } = 0;
        
        /// <summary>
        /// 信箱
        /// </summary>
        public string email { get; set; } = "";
        
        /// <summary>
        /// 地址
        /// </summary>
        public string address { get; set; } = "";
        
        /// <summary>
        /// 圖片位置
        /// </summary>
        public string imageUrl { get; set; } = "";

        /// <summary>
        /// 身體體態(身高/體重)
        /// </summary>
        public string posture { get; set; } = "";
    }

    /// <summary>
    /// 取得最新一筆 aha 和 ascm 問建
    /// </summary>
    public class GetOnlyAhaAndAcsmQuestionnaireVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;
        
        /// <summary>
        /// 問卷 id
        /// </summary>
        public int questionnaireId { get; set; } = 0;
        
        /// <summary>
        /// 問卷部分序號
        /// </summary>
        public int part { get; set; } = 0;
        
        /// <summary>
        /// 問卷子題題號
        /// </summary>
        public int subject { get; set; } = 0;

        /// <summary>
        /// 更新紀錄修改日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄修改時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 取得最新學員資訊
    /// </summary>
    public class LatestStudentInfoVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 電話號碼
        /// </summary>
        public string phone { get; set; } = "";

        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; } = "";

        /// <summary>
        /// 年齡
        /// </summary>
        public int age { get; set; } = 0;

        /// <summary>
        /// 信箱
        /// </summary>
        public string email { get; set; } = "";

        /// <summary>
        /// 地址
        /// </summary>
        public string address { get; set; } = "";

        /// <summary>
        /// 圖片位置
        /// </summary>
        public string imageUrl { get; set; } = "";
        
        /// <summary>
        /// 身體體態(身高/體重)
        /// </summary>
        public string posture { get; set; } = "";

        /// <summary>
        /// 危險等級
        /// </summary>
        public string dangerGrading { get; set; } = "";

        /// <summary>
        /// 疾病史
        /// </summary>
        public string medicalHistory { get; set; } = "";
    }
}
