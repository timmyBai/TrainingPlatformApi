using TrainingPlatformApi.Utils;

namespace TrainingPlatformApi.Services
{
    public class HostService
    {
        private string Host { get; set; } = "";

        private int Port { get; set; } = 80;

        public string Domain { get; set; } = "";

        public string ImagePath { get; set; } = "";

        public HostService()
        {
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                Host = "192.168.18.71";
                Port = 8080;
                Domain = $"http://{Host}:{Port}";
                JwtUtils.Domain = Domain;
                ImagePath = "/DEV/TrainingPlatform/Image";
            }
            else if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Staging")
            {
                Host = "192.168.18.71";
                Port = 8081;
                Domain = $"http://{Host}:{Port}";
                JwtUtils.Domain = Domain;
                ImagePath = "/Stag/TrainingPlatform/Image";
            }
            else if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Production")
            {
                Host = "192.168.18.71";
                Port = 80;
                Domain = $"http://{Host}";
                JwtUtils.Domain = Domain;
                ImagePath = "/TrainingPlatform/Image";
            }
            else
            {
                Host = "192.168.18.3";
                Port = 80;
                Domain = $"http://{Host}";
                JwtUtils.Domain = Domain;
                ImagePath = "/TrainingPlatform/Image";
            }
        }
    }
}
