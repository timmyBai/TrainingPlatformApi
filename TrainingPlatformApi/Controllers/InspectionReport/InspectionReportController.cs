using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;
using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.VM.Student;
using TrainingPlatformApi.Models.InspectionReport;
using TrainingPlatformApi.VM.InspectionReport;
using TrainingPlatformApi.Models.TrainingSchedule;
using TrainingPlatformApi.VM.TrainingSchedul;
using TrainingPlatformApi.VM.InstrumentSettings;
using TrainingPlatformApi.Utils;
using Newtonsoft.Json;
using UnityEngine;

namespace TrainingPlatformApi.Controllers.InspectionReport
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class InspectionReportController : ControllerBase
    {
        private MySqlConnection? conn;
        private ILogger<InspectionReportController> logger;

        public InspectionReportController(ILogger<InspectionReportController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得報表體適能(心肺適能、柔軟度、平衡)
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/inspectionReport/{studentId:required:int}/physicalFitness/{trainingType:required:int:regex((1|3|4))}/report")]
        public IActionResult GetInspectionReportPhysicalFitness(int studentId, int trainingType)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得過去 60 次訓練排程資料(心肺適能、柔軟度、平衡)
                    var inspectionReportPhysicalFitness = new TrainingScheduleModels(conn).GetBeforeTrainingScheduleReport<GetBeforeTrainingScheduleReportVM>(studentId, trainingType);

                    result.isSuccess = true;
                    result.data = inspectionReportPhysicalFitness;
                    logger.LogInformation("GetInspectionReportPhysicalFitnessSuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得正式訓練學員訓練各台儀器總功率清單
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/{studentId:required:int}/eachMachinary/totalPower/list")]
        public IActionResult GetStudentEachMachinaryTotalPowerList(int studentId)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    var machinaryTotalPowerList = new InspectionReportModels(conn).GetStudentEachMachinaryTotalPowerList<GetStudentEachMachinaryTotalPowerListVM>(studentId);

                    result.isSuccess = false;
                    result.data = machinaryTotalPowerList;
                    logger.LogInformation("GetMachinaryTotalPowerList");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得學員使用機器總功率
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/{studentId:required:int}/machinary/totalPower/{machinaryCode:int:regex(^(1|2|3|4|5|6|7|8|9|10)$)}/fetch")]
        public IActionResult GetStudentMachinaryTotalPower(int studentId, int machinaryCode)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    var studnetTotalPower = new InspectionReportModels(conn).GetStudentTotalPower<GetStudentMachinaryTotalPowerVM>(studentId, machinaryCode);

                    LatestStudentMachinaryTotalPowerVM latestStudentMachinaryTotalPower = new LatestStudentMachinaryTotalPowerVM();

                    if (studnetTotalPower != null)
                    {
                        // 計算總功率百分比
                        float totalPowerPercentage = MathF.Round(studnetTotalPower.power / 20000 * 100);
                        latestStudentMachinaryTotalPower.totalPower = (int)totalPowerPercentage;
                    }

                    result.isSuccess = true;
                    result.data = latestStudentMachinaryTotalPower;
                    logger.LogInformation("GetLatestStudentMachinaryTotalPowerSuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得過去 20 次正式訓練狀況報表
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpGet]
        [Authorize]
        [Route("api/{studentId:int}/formatTraining/status/{machinaryCode:int:regex(^(1|2|3|4|5|6|7|8|9|10)$)}/report")]
        public IActionResult GetStudentFormalTrainingStatusReport(int studentId, int machinaryCode)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    var formalTrainingReportList = new InspectionReportModels(conn).GetFormalTrainingStatusReport<GetFormalTrainingStatusReportVM>(studentId, machinaryCode, today);

                    result.isSuccess = true;
                    result.data = formalTrainingReportList;
                    logger.LogInformation("GetTrainingReportSuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得過去 20 次正式訓練狀況-功率
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpGet]
        [Authorize]
        [Route("api/{studentId:int}/formalTraining/power/{machinaryCode:int:regex(^(1|2|3|4|5|6|7|8|9|10)$)}/report")]
        public IActionResult GetStudentTrainingPowerReport(int studentId, int machinaryCode)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    var trainingFormalPowerList = new InspectionReportModels(conn).GetFormalTrainingPowerReport<GetFormalTrainingPowerReportVM>(studentId, machinaryCode, today);

                    result.isSuccess = true;
                    result.data = trainingFormalPowerList;
                    logger.LogInformation("getTrainingPowerSuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得單日報表
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpPost]
        [Authorize]
        [Route("api/{studentId:int}/singleDay/report")]
        public IActionResult GetStudentSingleDayReport(int studentId, RequestStudentSingleDayReportModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得訓練肌力日期清單
                    var trainingDateList = new InspectionReportModels(conn).GetTrainingAllDay<GetTrainingAllDayVM>(studentId);

                    if (trainingDateList.Count() == 0)
                    {
                        result.isSuccess = false;
                        result.message = "NotAnyDayTraining";
                        logger.LogError("NotAnyDayTraining");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 彙整訓練日期清單
                    List<string> trainingDate = new List<string>();

                    trainingDateList.ToList().ForEach((item) =>
                    {
                        trainingDate.Add(item.trainingDate);
                    });

                    // 轉換單日報表模型
                    FinallyGetSingleDayReportVM finallyGetSingleDayReport = new FinallyGetSingleDayReportVM();

                    // 檢查接收日期是否存在於日期清單
                    int trainingDateIndex = trainingDate.ToList().IndexOf(body.trainingDate.ToString("yyyy-MM-dd"));

                    string targetDate = "";

                    if (trainingDateIndex != -1)
                    {
                        targetDate = trainingDate[trainingDateIndex];
                    }
                    else
                    {
                        targetDate = trainingDate[trainingDate.Count() - 1];
                    }

                    // 取得單日報表
                    var singleDayReportList = new InspectionReportModels(conn).GetSingleDayReport<GetSingleDayReportVM>(studentId, targetDate);

                    // 轉換單日報表模型
                    singleDayReportList.ToList().ForEach((item) =>
                    {
                        finallyGetSingleDayReport.singleDayReport.Add(new ConvertGetSingleDayReportVM()
                        {
                            userAuthorizedId = item.userAuthorizedId,
                            machinaryCode = item.machinaryCode,
                            machinaryCodeName = item.machinaryCodeName,
                            level = item.level,
                            finish = item.finish,
                            mistake = item.mistake,
                            power = item.power,
                            speed = JsonConvert.DeserializeObject<List<int>>(item.speed),
                            maxSpeed = item.maxSpeed,
                            consciousEffort = item.consciousEffort,
                            trainingDate = item.trainingDate
                        });
                    });

                    // 計算速度百分比
                    for (int i = 0; i < finallyGetSingleDayReport.singleDayReport.Count(); i++)
                    {
                        int maxSpeed = finallyGetSingleDayReport.singleDayReport[i].maxSpeed;

                        for (int j = 0; j < finallyGetSingleDayReport.singleDayReport[i].speed.Count(); j++)
                        {
                            int speed = 0;

                            if (finallyGetSingleDayReport.singleDayReport[i].speed[j] > 0)
                            {
                                if (j == finallyGetSingleDayReport.singleDayReport[i].speed.Count() - 1)
                                {
                                    speed = Mathf.RoundToInt((float)finallyGetSingleDayReport.singleDayReport[i].speed[j] * 100.0f / (float)maxSpeed);
                                }
                                else
                                {
                                    speed = Mathf.RoundToInt((float)(finallyGetSingleDayReport.singleDayReport[i].speed[j] - finallyGetSingleDayReport.singleDayReport[i].speed[j + 1]) * 100.0f / (2.0f * maxSpeed));
                                }

                                finallyGetSingleDayReport.singleDayReport[i].speedPercentage.Add(speed);
                            }
                            else if (finallyGetSingleDayReport.singleDayReport[i].speed[j] == 0)
                            {
                                finallyGetSingleDayReport.singleDayReport[i].speedPercentage.Add(speed);
                            }
                        }
                    }

                    // 取得目標日期在第幾索引
                    trainingDateIndex = trainingDate.ToList().IndexOf(targetDate);

                    // 如果不是最後一個索引，也不是第一索引
                    if (trainingDateIndex != trainingDate.Count() - 1 && trainingDateIndex != 0)
                    {
                        finallyGetSingleDayReport.prevDay = trainingDate[trainingDateIndex - 1].ToString();
                        finallyGetSingleDayReport.nextDay = trainingDate[trainingDateIndex + 1].ToString();
                    }

                    // 如果是第一索引
                    if (trainingDateIndex == 0)
                    {
                        if (trainingDateIndex + 1 > trainingDate.Count() - 1)
                        {
                            finallyGetSingleDayReport.prevDay = "";
                            finallyGetSingleDayReport.nextDay = "";
                        }
                        else
                        {
                            finallyGetSingleDayReport.prevDay = "";
                            finallyGetSingleDayReport.nextDay = trainingDate[trainingDateIndex + 1].ToString();
                        }
                    }

                    // 如果是最後索引
                    if (trainingDateIndex == trainingDate.Count() - 1)
                    {
                        if (trainingDateIndex - 1 == -1)
                        {
                            finallyGetSingleDayReport.prevDay = "";
                            finallyGetSingleDayReport.nextDay = "";
                        }
                        else
                        {
                            finallyGetSingleDayReport.prevDay = trainingDate[trainingDateIndex - 1].ToString();
                            finallyGetSingleDayReport.nextDay = "";
                        }
                    }

                    result.isSuccess = true;
                    result.data = finallyGetSingleDayReport;
                    logger.LogInformation("GetSingleDaySuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得學員機器訓練總表
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpGet]
        [Authorize]
        [Route("api/{studentId:int}/trainingMachinary/{machinaryCode:int:regex(^(1|2|3|4|5|6|7|8|9|10)$)}/summary/list")]
        public IActionResult GetStudentTrainingMachinarySummaryList(int studentId, int machinaryCode)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    var studentTrainingSummary = new InspectionReportModels(conn).GetStudentTrainingMachinarySummary<StudentTrainingMachinarySummaryVM>(studentId, machinaryCode);

                    result.isSuccess = true;
                    result.data = studentTrainingSummary;
                    logger.LogInformation("GetStudentTrainingMachinarySummaryListSuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員正式機器訓練當天備註
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/{studentId:required:int}/formalTraining/{machinaryCode:int:required:regex(^(1|2|3|4|5|6|7|8|9|10)$)}/mark/replace")]
        public IActionResult ReplaceStudentFormalTrainingMark(int studentId, int machinaryCode, RequestStudentTrainingMachinarySummaryListModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    var singleFormalTrainingExist = new InspectionReportModels(conn).InspectFormalTrainingSingleDay<InspectFormalTrainingSingleDayVM>(studentId, machinaryCode, body);

                    // 如果沒有正式訓練資料
                    if (singleFormalTrainingExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "NotAnyFormalTrainingInfo";
                        logger.LogError("NotAnyFormalTrainingInfo");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    var singleFormalTrainingMarkExist = new InspectionReportModels(conn).InspectFormalTrainingSingleDayMark<InspectFormalTrainingSingleDayMarkVM>(studentId, machinaryCode, body);

                    if (singleFormalTrainingMarkExist == null)
                    {
                        new InspectionReportModels(conn).InsertFormalTrainingSingleDayMark(recordId, studentId, machinaryCode, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertFormalTrainingSingleDayMarkSuccess";
                        logger.LogInformation("InsertFormalTrainingSingleDayMarkSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new InspectionReportModels(conn).UpdateFormalTrainingSingleDayMark(recordId, studentId, machinaryCode, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateFormalTrainingSingleDayMarkSuccess";
                        logger.LogInformation("UpdateFormalTrainingSingleDayMarkSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 刪除學員正式機器訓練當天備註
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpDelete]
        [Route("api/{studentId:int}/formalTraining/{machinaryCode:int:required:regex(^(1|2|3|4|5|6|7|8|9|10)$)}/mark/delete")]
        public IActionResult DeleteStudentFormalTrainingMark(int studentId, int machinaryCode, RequestDeleteStudentTrainingMachinarySummaryListModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    var deleteFormalTrainingMark = new InspectionReportModels(conn).DeleteFormalTrainingSingleDayMark(studentId, machinaryCode, body);

                    transaction.Commit();
                    result.isSuccess = true;
                    result.message = "DeleteFormalTrainingMarkSuccess";
                    logger.LogInformation("DeleteFormalTrainingMarkSuccess");
                    return StatusCode((int)HttpStatusCode.NoContent, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
