using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.InstrumentSettings;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.VM.InstrumentSettings;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.VM.Student;

namespace TrainingPlatformApi.Controllers.InstrumentSettings
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class InstrumentSettingsController : ControllerBase
    {
        private ILogger<InstrumentSettingsController> logger;
        private MySqlConnection? conn;

        public InstrumentSettingsController(ILogger<InstrumentSettingsController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 新增或更新儀器設定運動表現備註
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpPut]
        [Authorize]
        [Route("api/{studentId:int}/sportsPerformance/{machinaryCode:int}/mark/replace")]
        public IActionResult ReplaceInstrumentSettingsSportsPerformance(int studentId, int machinaryCode, RequestSportsPerformanceModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 檢查儀器設定最大運動表現資料
                    var instrumentSettingsSportsPerformance = new InstrumentSettingsModels(conn).InspactInstrumentSettingsSportsPerformance<InspactInstrumentSettingsSportsPerformanceVM>(studentId, machinaryCode, body);
                    
                    // 如果沒有此筆資料
                    if (instrumentSettingsSportsPerformance == null)
                    {
                        result.isSuccess = false;
                        result.message = "DataDoesNotExist";
                        logger.LogError("DataDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 檢查儀器設定最大運動表現備註資料
                    var instrumentSettingsSportsPerformanceMark = new InstrumentSettingsModels(conn).InspactInstrumentSettingsSportsPerformanceMark<InspactInstrumentSettingsSportsPerformanceMarkVM>(studentId, machinaryCode, body);

                    if(instrumentSettingsSportsPerformanceMark == null)
                    {
                        new InstrumentSettingsModels(conn).InsertInstrumentSettingsSportsPerformanceMark(studentId, recordId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertSportsPerformanceSuccess";
                        logger.LogInformation("InsertSportsPerformanceSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new InstrumentSettingsModels(conn).UpdateInstrumentSettingsSportsPerformanceMark(studentId, recordId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateSportsPerformanceSuccess";
                        logger.LogInformation("UpdateSportsPerformanceSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
