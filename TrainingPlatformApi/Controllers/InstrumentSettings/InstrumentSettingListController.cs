using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;

using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.InstrumentSettings;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.VM.InstrumentSettings;
using TrainingPlatformApi.VM.Student;

namespace TrainingPlatformApi.Controllers.InstrumentSettings
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class InstrumentSettingListController : ControllerBase
    {
        private ILogger<InstrumentSettingListController> logger;
        private MySqlConnection? conn;

        public InstrumentSettingListController(ILogger<InstrumentSettingListController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得儀器設定最大運動表現清單
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/instrumentSettings/{studentId:int}/sportsPerformance/list")]
        public IActionResult GetInstrumentSettingsSportsPerformanceList(int studentId)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得儀器設定運動表現清單
                    var instrumentSettingSportList = new InstrumentSettingListModels(conn).GetInstrumentSettingSportsPerformanceList<InstrumentSettingSportsPerformanceListVM>(studentId);

                    InstrumentSettingListVM instrumentSettingList = new InstrumentSettingListVM();

                    // 抓取機器編號一
                    var machinaryCodeOne = instrumentSettingSportList.Where(w => w.machinaryCode == 1);

                    if (machinaryCodeOne.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeOneGroup.userAuthorizedId = machinaryCodeOne.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeOneGroup.machinaryCode = machinaryCodeOne.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeOneGroup.machinaryCodeName = machinaryCodeOne.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeOneGroup.level = machinaryCodeOne.ToList()[0].level;
                        instrumentSettingList.machinaryCodeOneGroup.maxRange = machinaryCodeOne.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeOneGroup.power = machinaryCodeOne.ToList()[0].power;
                        instrumentSettingList.machinaryCodeOneGroup.mark = machinaryCodeOne.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeOneGroup.trainingDate = machinaryCodeOne.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeOne.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeOneGroup.children.Add(new InstrumentSettingMachinaryCodeOneGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeOne.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeOne.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeOne.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeOne.ToList()[i].level,
                                    maxRange = machinaryCodeOne.ToList()[i].maxRange,
                                    power = machinaryCodeOne.ToList()[i].power,
                                    mark = machinaryCodeOne.ToList()[i].mark,
                                    trainingDate = machinaryCodeOne.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeOneGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeOneGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeOneGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeOneGroup.level = 0;
                        instrumentSettingList.machinaryCodeOneGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeOneGroup.power = "-";
                        instrumentSettingList.machinaryCodeOneGroup.mark = "-";
                        instrumentSettingList.machinaryCodeOneGroup.trainingDate = "-";
                    }

                    // 抓取機器編號二
                    var machinaryCodeTwo = instrumentSettingSportList.Where(w => w.machinaryCode == 2);

                    if (machinaryCodeTwo.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeTwoGroup.userAuthorizedId = machinaryCodeTwo.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeTwoGroup.machinaryCode = machinaryCodeTwo.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeTwoGroup.machinaryCodeName = machinaryCodeTwo.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeTwoGroup.level = machinaryCodeTwo.ToList()[0].level;
                        instrumentSettingList.machinaryCodeTwoGroup.maxRange = machinaryCodeTwo.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeTwoGroup.power = machinaryCodeTwo.ToList()[0].power;
                        instrumentSettingList.machinaryCodeTwoGroup.mark = machinaryCodeTwo.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeTwoGroup.trainingDate = machinaryCodeTwo.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeTwo.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeTwoGroup.children.Add(new InstrumentSettingMachinaryCodeTwoGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeTwo.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeTwo.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeTwo.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeTwo.ToList()[i].level,
                                    maxRange = machinaryCodeTwo.ToList()[i].maxRange,
                                    power = machinaryCodeTwo.ToList()[i].power,
                                    mark = machinaryCodeTwo.ToList()[i].mark,
                                    trainingDate = machinaryCodeTwo.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeTwoGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeTwoGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeTwoGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeTwoGroup.level = 0;
                        instrumentSettingList.machinaryCodeTwoGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeTwoGroup.power = "-";
                        instrumentSettingList.machinaryCodeTwoGroup.mark = "-";
                        instrumentSettingList.machinaryCodeTwoGroup.trainingDate = "-";
                    }

                    // 抓取機器編號三
                    var machinaryCodeThree = instrumentSettingSportList.Where(w => w.machinaryCode == 3);

                    if (machinaryCodeThree.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeThreeGroup.userAuthorizedId = machinaryCodeThree.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeThreeGroup.machinaryCode = machinaryCodeThree.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeThreeGroup.machinaryCodeName = machinaryCodeThree.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeThreeGroup.level = machinaryCodeThree.ToList()[0].level;
                        instrumentSettingList.machinaryCodeThreeGroup.maxRange = machinaryCodeThree.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeThreeGroup.power = machinaryCodeThree.ToList()[0].power;
                        instrumentSettingList.machinaryCodeThreeGroup.mark = machinaryCodeThree.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeThreeGroup.trainingDate = machinaryCodeThree.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeThree.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeThreeGroup.children.Add(new InstrumentSettingMachinaryCodeThreeGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeThree.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeThree.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeThree.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeThree.ToList()[i].level,
                                    maxRange = machinaryCodeThree.ToList()[i].maxRange,
                                    power = machinaryCodeThree.ToList()[i].power,
                                    mark = machinaryCodeThree.ToList()[i].mark,
                                    trainingDate = machinaryCodeThree.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeThreeGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeThreeGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeThreeGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeThreeGroup.level = 0;
                        instrumentSettingList.machinaryCodeThreeGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeThreeGroup.power = "-";
                        instrumentSettingList.machinaryCodeThreeGroup.mark = "-";
                        instrumentSettingList.machinaryCodeThreeGroup.trainingDate = "-";
                    }

                    // 抓取機器編號四
                    var machinaryCodeFour = instrumentSettingSportList.Where(w => w.machinaryCode == 4);

                    if (machinaryCodeFour.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeFourGroup.userAuthorizedId = machinaryCodeFour.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeFourGroup.machinaryCode = machinaryCodeFour.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeFourGroup.machinaryCodeName = machinaryCodeFour.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeFourGroup.level = machinaryCodeFour.ToList()[0].level;
                        instrumentSettingList.machinaryCodeFourGroup.maxRange = machinaryCodeFour.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeFourGroup.power = machinaryCodeFour.ToList()[0].power;
                        instrumentSettingList.machinaryCodeFourGroup.mark = machinaryCodeFour.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeFourGroup.trainingDate = machinaryCodeFour.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeFour.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeFourGroup.children.Add(new InstrumentSettingMachinaryCodeFourGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeFour.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeFour.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeFour.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeFour.ToList()[i].level,
                                    maxRange = machinaryCodeFour.ToList()[i].maxRange,
                                    power = machinaryCodeFour.ToList()[i].power,
                                    mark = machinaryCodeFour.ToList()[i].mark,
                                    trainingDate = machinaryCodeFour.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeFourGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeFourGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeFourGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeFourGroup.level = 0;
                        instrumentSettingList.machinaryCodeFourGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeFourGroup.power = "-";
                        instrumentSettingList.machinaryCodeFourGroup.mark = "-";
                        instrumentSettingList.machinaryCodeFourGroup.trainingDate = "-";
                    }

                    // 抓取機器編號五
                    var machinaryCodeFive = instrumentSettingSportList.Where(w => w.machinaryCode == 5);

                    if (machinaryCodeFive.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeFiveGroup.userAuthorizedId = machinaryCodeFive.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeFiveGroup.machinaryCode = machinaryCodeFive.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeFiveGroup.machinaryCodeName = machinaryCodeFive.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeFiveGroup.level = machinaryCodeFive.ToList()[0].level;
                        instrumentSettingList.machinaryCodeFiveGroup.maxRange = machinaryCodeFive.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeFiveGroup.power = machinaryCodeFive.ToList()[0].power;
                        instrumentSettingList.machinaryCodeFiveGroup.mark = machinaryCodeFive.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeFiveGroup.trainingDate = machinaryCodeFive.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeFive.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeFiveGroup.children.Add(new InstrumentSettingMachinaryCodeFiveGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeFive.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeFive.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeFive.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeFive.ToList()[i].level,
                                    maxRange = machinaryCodeFive.ToList()[i].maxRange,
                                    power = machinaryCodeFive.ToList()[i].power,
                                    mark = machinaryCodeFive.ToList()[i].mark,
                                    trainingDate = machinaryCodeFive.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeFiveGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeFiveGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeFiveGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeFiveGroup.level = 0;
                        instrumentSettingList.machinaryCodeFiveGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeFiveGroup.power = "-";
                        instrumentSettingList.machinaryCodeFiveGroup.mark = "-";
                        instrumentSettingList.machinaryCodeFiveGroup.trainingDate = "-";
                    }

                    // 抓取機器編號六
                    var machinaryCodeSix = instrumentSettingSportList.Where(w => w.machinaryCode == 6);

                    if (machinaryCodeSix.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeSixGroup.userAuthorizedId = machinaryCodeSix.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeSixGroup.machinaryCode = machinaryCodeSix.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeSixGroup.machinaryCodeName = machinaryCodeSix.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeSixGroup.level = machinaryCodeSix.ToList()[0].level;
                        instrumentSettingList.machinaryCodeSixGroup.maxRange = machinaryCodeSix.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeSixGroup.power = machinaryCodeSix.ToList()[0].power;
                        instrumentSettingList.machinaryCodeSixGroup.mark = machinaryCodeSix.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeSixGroup.trainingDate = machinaryCodeSix.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeSix.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeSixGroup.children.Add(new InstrumentSettingMachinaryCodeSixGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeSix.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeSix.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeSix.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeSix.ToList()[i].level,
                                    maxRange = machinaryCodeSix.ToList()[i].maxRange,
                                    power = machinaryCodeSix.ToList()[i].power,
                                    mark = machinaryCodeSix.ToList()[i].mark,
                                    trainingDate = machinaryCodeSix.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeSixGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeSixGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeSixGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeSixGroup.level = 0;
                        instrumentSettingList.machinaryCodeSixGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeSixGroup.power = "-";
                        instrumentSettingList.machinaryCodeSixGroup.mark = "-";
                        instrumentSettingList.machinaryCodeSixGroup.trainingDate = "-";
                    }

                    // 抓取機器編號七
                    var machinaryCodeSeven = instrumentSettingSportList.Where(w => w.machinaryCode == 7);

                    if (machinaryCodeSeven.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeSevenGroup.userAuthorizedId = machinaryCodeSeven.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeSevenGroup.machinaryCode = machinaryCodeSeven.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeSevenGroup.machinaryCodeName = machinaryCodeSeven.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeSevenGroup.level = machinaryCodeSeven.ToList()[0].level;
                        instrumentSettingList.machinaryCodeSevenGroup.maxRange = machinaryCodeSeven.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeSevenGroup.power = machinaryCodeSeven.ToList()[0].power;
                        instrumentSettingList.machinaryCodeSevenGroup.mark = machinaryCodeSeven.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeSevenGroup.trainingDate = machinaryCodeSeven.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeSeven.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeSevenGroup.children.Add(new InstrumentSettingMachinaryCodeSevenGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeSeven.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeSeven.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeSeven.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeSeven.ToList()[i].level,
                                    maxRange = machinaryCodeSeven.ToList()[i].maxRange,
                                    mark = machinaryCodeSeven.ToList()[i].mark,
                                    power = machinaryCodeSeven.ToList()[i].power,
                                    trainingDate = machinaryCodeSeven.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeSevenGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeSevenGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeSevenGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeSevenGroup.level = 0;
                        instrumentSettingList.machinaryCodeSevenGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeSevenGroup.power = "-";
                        instrumentSettingList.machinaryCodeSevenGroup.mark = "-";
                        instrumentSettingList.machinaryCodeSevenGroup.trainingDate = "-";
                    }

                    // 抓取機器編號八
                    var machinaryCodeEight = instrumentSettingSportList.Where(w => w.machinaryCode == 8);

                    if (machinaryCodeEight.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeEightGroup.userAuthorizedId = machinaryCodeEight.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeEightGroup.machinaryCode = machinaryCodeEight.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeEightGroup.machinaryCodeName = machinaryCodeEight.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeEightGroup.level = machinaryCodeEight.ToList()[0].level;
                        instrumentSettingList.machinaryCodeEightGroup.maxRange = machinaryCodeEight.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeEightGroup.power = machinaryCodeEight.ToList()[0].power;
                        instrumentSettingList.machinaryCodeEightGroup.mark = machinaryCodeEight.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeEightGroup.trainingDate = machinaryCodeEight.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeEight.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeEightGroup.children.Add(new InstrumentSettingMachinaryCodeEightGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeEight.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeEight.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeEight.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeEight.ToList()[i].level,
                                    maxRange = machinaryCodeEight.ToList()[i].maxRange,
                                    power = machinaryCodeEight.ToList()[i].power,
                                    mark = machinaryCodeEight.ToList()[i].mark,
                                    trainingDate = machinaryCodeEight.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeEightGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeEightGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeEightGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeEightGroup.level = 0;
                        instrumentSettingList.machinaryCodeEightGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeEightGroup.power = "-";
                        instrumentSettingList.machinaryCodeEightGroup.mark = "-";
                        instrumentSettingList.machinaryCodeEightGroup.trainingDate = "-";
                    }

                    // 抓取機器編號九
                    var machinaryCodeNine = instrumentSettingSportList.Where(w => w.machinaryCode == 9);

                    if (machinaryCodeNine.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeNineGroup.userAuthorizedId = machinaryCodeNine.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeNineGroup.machinaryCode = machinaryCodeNine.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeNineGroup.machinaryCodeName = machinaryCodeNine.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeNineGroup.level = machinaryCodeNine.ToList()[0].level;
                        instrumentSettingList.machinaryCodeNineGroup.maxRange = machinaryCodeNine.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeNineGroup.power = machinaryCodeNine.ToList()[0].power;
                        instrumentSettingList.machinaryCodeNineGroup.mark = machinaryCodeNine.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeNineGroup.trainingDate = machinaryCodeNine.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeNine.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeNineGroup.children.Add(new InstrumentSettingMachinaryCodeNineGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeNine.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeNine.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeNine.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeNine.ToList()[i].level,
                                    maxRange = machinaryCodeNine.ToList()[i].maxRange,
                                    power = machinaryCodeNine.ToList()[i].power,
                                    mark = machinaryCodeNine.ToList()[i].mark,
                                    trainingDate = machinaryCodeNine.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeNineGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeNineGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeNineGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeNineGroup.level = 0;
                        instrumentSettingList.machinaryCodeNineGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeNineGroup.power = "-";
                        instrumentSettingList.machinaryCodeNineGroup.mark = "-";
                        instrumentSettingList.machinaryCodeNineGroup.trainingDate = "-";
                    }

                    // 抓取機器編號十
                    var machinaryCodeTen = instrumentSettingSportList.Where(w => w.machinaryCode == 10);

                    if (machinaryCodeTen.Count() > 0)
                    {
                        instrumentSettingList.machinaryCodeTenGroup.userAuthorizedId = machinaryCodeTen.ToList()[0].userAuthorizedId;
                        instrumentSettingList.machinaryCodeTenGroup.machinaryCode = machinaryCodeTen.ToList()[0].machinaryCode;
                        instrumentSettingList.machinaryCodeTenGroup.machinaryCodeName = machinaryCodeTen.ToList()[0].machinaryCodeName;
                        instrumentSettingList.machinaryCodeTenGroup.level = machinaryCodeTen.ToList()[0].level;
                        instrumentSettingList.machinaryCodeTenGroup.maxRange = machinaryCodeTen.ToList()[0].maxRange;
                        instrumentSettingList.machinaryCodeTenGroup.power = machinaryCodeTen.ToList()[0].power;
                        instrumentSettingList.machinaryCodeTenGroup.mark = machinaryCodeTen.ToList()[0].mark;
                        instrumentSettingList.machinaryCodeTenGroup.trainingDate = machinaryCodeTen.ToList()[0].trainingDate;

                        for (int i = 0; i < machinaryCodeTen.Count(); i++)
                        {
                            if (i != 0)
                            {
                                instrumentSettingList.machinaryCodeTenGroup.children.Add(new InstrumentSettingMachinaryCodeTenGroupChildrenVM()
                                {
                                    userAuthorizedId = machinaryCodeTen.ToList()[i].userAuthorizedId,
                                    machinaryCode = machinaryCodeTen.ToList()[i].machinaryCode,
                                    machinaryCodeName = machinaryCodeTen.ToList()[i].machinaryCodeName,
                                    level = machinaryCodeTen.ToList()[i].level,
                                    maxRange = machinaryCodeTen.ToList()[i].maxRange,
                                    power = machinaryCodeTen.ToList()[i].power,
                                    mark = machinaryCodeTen.ToList()[i].mark,
                                    trainingDate = machinaryCodeTen.ToList()[i].trainingDate
                                });
                            }
                        }
                    }
                    else
                    {
                        instrumentSettingList.machinaryCodeTenGroup.userAuthorizedId = 0;
                        instrumentSettingList.machinaryCodeTenGroup.machinaryCode = 0;
                        instrumentSettingList.machinaryCodeTenGroup.machinaryCodeName = "-";
                        instrumentSettingList.machinaryCodeTenGroup.level = 0;
                        instrumentSettingList.machinaryCodeTenGroup.maxRange = "-";
                        instrumentSettingList.machinaryCodeTenGroup.power = "-";
                        instrumentSettingList.machinaryCodeTenGroup.mark = "-";
                        instrumentSettingList.machinaryCodeTenGroup.trainingDate = "-";
                    }

                    result.isSuccess = true;
                    result.data = instrumentSettingList;
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
