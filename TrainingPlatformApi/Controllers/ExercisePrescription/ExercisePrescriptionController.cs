using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;

using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.ExercisePrescription;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.VM.ExercisePrescription;
using TrainingPlatformApi.VM.Student;

/*
 * 運動處方-新增與更新相關 api
 */
namespace TrainingPlatformApi.Controllers.ExercisePrescription
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class ExercisePrescriptionController : ControllerBase
    {
        private readonly ILogger<ExercisePrescriptionController> logger;
        private MySqlConnection? conn;
        
        public ExercisePrescriptionController(ILogger<ExercisePrescriptionController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 新增或更新 訓練階段
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/trainingStage/{studentId:int}/replace")]
        public IActionResult ReplaceExercisePrescriptionTrainingStage(int studentId, RequestExercisePrescriptionTrainingStage body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            string today = new DayUtils().GetFullDate();
            int startDate = Convert.ToInt32(body.startDate.ToString("yyyyMMdd"));

            // 如果設定日期大於今天日期
            if (startDate > Convert.ToInt32(today))
            {
                result.isSuccess = false;
                result.message = "LargerToday";
                logger.LogError("LargerToday");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 檢查今日是否有運動處方-訓練階段資料
                    var todayExercisePrescriptionTrainingStage = new ExercisePrescriptionModels(conn).InspactTodayExercisePrescriptionTrainingStage<InspactTodayExercisePrescriptionTrainingStage>(studentId, today);
                    
                    // 如果今日沒有運動處方-訓練階段資料
                    if (todayExercisePrescriptionTrainingStage == null)
                    {
                        new ExercisePrescriptionModels(conn).InsertExercisePrescriptionTrainingStage(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertExercisePrescriptionTrainingStageSuccess";
                        logger.LogInformation("InsertExercisePrescriptionTrainingStageSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new ExercisePrescriptionModels(conn).UpdateExercisePrescriptionTrainingStage(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateExercisePrescriptionTrainingStageSuccess";
                        logger.LogInformation("UpdateExercisePrescriptionTrainingStageSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新運動處方 心肺適能
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/exercisePrescriptionCardiorespiratoryFitness/{studentId:int}/mark/replace")]
        public IActionResult ReplaceExercisePrescriptionCardiorespiratoryFitness(int studentId, RequestExercisePrescriptionCardiorespiratoryFitness body)
        {
            ResultModels result = new ResultModels();
            
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            // 檢查運動處方-心肺適能，是否有非法運動項目
            var isNotSport = new ExercisePrescriptionUtils().InspactExercisePrescriptionCardiorespiratoryFitnessSports(body.sports);

            if (!isNotSport)
            {
                result.isSuccess = false;
                result.message = "IllegalSports";
                logger.LogError("IllegalSports");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();
                    
                    // 檢查今日是否有運動處方-心肺適能資料
                    var todayExercisePrescriptionCardiorespiratoryFitness = new ExercisePrescriptionModels(conn).InspactTodayExercisePrescriptionCardiorespiratoryFitness<InspactTodayExercisePrescriptionCardiorespiratoryFitnessVM>(studentId, today);

                    // 如果今日沒有運動處方-心肺適能資料
                    if (todayExercisePrescriptionCardiorespiratoryFitness == null)
                    {
                        new ExercisePrescriptionModels(conn).InsertExercisePrescriptionCardiorespiratoryFitness(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertExercisePrescriptionCardiorespiratoryFitnessSuccess";
                        logger.LogInformation("InsertExercisePrescriptionCardiorespiratoryFitnessSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new ExercisePrescriptionModels(conn).UpdateExercisePrescriptionCardiorespiratoryFitness(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateExercisePrescriptionCardiorespiratoryFitnessSuccess";
                        logger.LogInformation("UpdateExercisePrescriptionCardiorespiratoryFitnessSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新運動處方 肌力與肌耐力
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpPut]
        [Authorize]
        [Route("api/exercisePrescriptionMuscleStrengthAndMuscleEndurance/{studentId:int}/mark/replace")]
        public IActionResult ReplaceExercisePrescriptionMuscleStrengthAndMuscleEndurance(int studentId, RequestExercisePrescriptionMuscleStrengthAndMuscleEndurance body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            // 檢查運動處方-肌力與肌耐力，檢查是否有非法訓練部位
            var isNotTrainingArea = new ExercisePrescriptionUtils().InspactExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea(body.trainingArea);

            if (!isNotTrainingArea)
            {
                result.isSuccess = false;
                result.message = "IllegalTrainingArea";
                logger.LogError("IllegalTrainingArea");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 檢查今日是否有運動處方-肌力與肌耐力資料
                    var todayExercisePrescriptionMuscleStrengthAndMuscleEndurance = new ExercisePrescriptionModels(conn).InspactTodayExercisePrescriptionMuscleStrengthAndMuscleEndurance<InspactTodayExercisePrescriptionMuscleStrengthAndMuscleEnduranceVM>(studentId, today);

                    // 如果今日沒有運動處方-肌力與肌耐力資料
                    if (todayExercisePrescriptionMuscleStrengthAndMuscleEndurance == null)
                    {
                        new ExercisePrescriptionModels(conn).InsertExercisePrescriptionMuscleStrengthAndMuscleEndurance(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertExercisePrescriptionMuscleStrengthAndMuscleEnduranceSuccess";
                        logger.LogInformation("InsertExercisePrescriptionMuscleStrengthAndMuscleEnduranceSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new ExercisePrescriptionModels(conn).UpdateExercisePrescriptionMuscleStrengthAndMuscleEndurance(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateExercisePrescriptionMuscleStrengthAndMuscleEnduranceSuccess";
                        logger.LogInformation("UpdateExercisePrescriptionMuscleStrengthAndMuscleEnduranceSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新運動處方 柔軟度
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpPut]
        [Authorize]
        [Route("api/exercisePrescriptionSoftness/{studentId:int}/mark/replace")]
        public IActionResult ReplaceExercisePrescriptionSoftness(int studentId, RequestExercisePrescriptionSoftness body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            // 檢查運動處方-柔軟度，檢查是否有非法運動項目
            var isNotSports = new ExercisePrescriptionUtils().InspactExercisePrescriptionSoftnessSports(body.sports);

            if (!isNotSports)
            {
                result.isSuccess = false;
                result.message = "IllegalSports";
                logger.LogError("IllegalSports");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 檢查今日是否有運動處方-柔軟度資料
                    var todayExercisePrescriptionSoftness = new ExercisePrescriptionModels(conn).InspactTodayExercisePrescriptionSoftness<InspactTodayExercisePrescriptionSoftnessVM>(studentId, today);

                    // 如果今日沒有運動處方-柔軟度資料
                    if (todayExercisePrescriptionSoftness == null)
                    {
                        new ExercisePrescriptionModels(conn).InsertExercisePrescriptionSoftness(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertExercisePrescriptionSoftnessSuccess";
                        logger.LogInformation("InsertExercisePrescriptionSoftnessSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new ExercisePrescriptionModels(conn).UpdateExercisePrescriptionSoftness(recordId, studentId, today, body);
                    
                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateExercisePrescriptionSoftnessSuccess";
                        logger.LogInformation("UpdateExercisePrescriptionSoftnessSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新運動處方 平衡
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpPut]
        [Authorize]
        [Route("api/exercisePrescriptionBalance/{studentId:int}/mark/replace")]
        public IActionResult ReplaceExercisePrescriptionBalance(int studentId, RequestExercisePrescriptionBalance body)
        {
            ResultModels result = new ResultModels();
            
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            var isNotSport = new ExercisePrescriptionUtils().InspactExercisePrescriptionBalanceSports(body.sports);

            if (!isNotSport)
            {
                result.isSuccess = false;
                result.message = "IllegalSports";
                logger.LogError("IllegalSports");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    var todayExercisePrescriptioBalance = new ExercisePrescriptionModels(conn).InspactTodayExercisePrescriptioBalance<InspactTodayExercisePrescriptionBalanceVM>(studentId, today);
                    
                    if (todayExercisePrescriptioBalance == null)
                    {
                        new ExercisePrescriptionModels(conn).InsertExercisePrescriptioBalance(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertExercisePrescriptioBalanceSuccess";
                        logger.LogInformation("InsertExercisePrescriptioBalanceSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new ExercisePrescriptionModels(conn).UpdateExercisePrescriptioBalance(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateExercisePrescriptioBalanceSuccess";
                        logger.LogInformation("UpdateExercisePrescriptioBalanceSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
