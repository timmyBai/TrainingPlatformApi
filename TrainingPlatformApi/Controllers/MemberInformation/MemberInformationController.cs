using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Net;

using TrainingPlatformApi.Models;
using TrainingPlatformApi.Utils;

/*
 * 會員資訊 api
 */
namespace TrainingPlatformApi.Controllers.MemberInformation
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class MemberInformationController : ControllerBase
    {
        private readonly ILogger<MemberInformationController> logger;

        public MemberInformationController(ILogger<MemberInformationController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得會員資料
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/member/information")]
        public IActionResult MemberInformation()
        {
            ResultModels result = new ResultModels();

            try
            {
                // 連線 redis 暫存使用者 token
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var memberInfo = new JwtUtils().DecryptJwtKey(accessToken);

                if (memberInfo.Count == 0)
                {
                    result.isSuccess = false;
                    result.message = "TokenHasExpired";
                    logger.LogError("TokenHasExpired");
                    return StatusCode((int)HttpStatusCode.Unauthorized, result);
                }

                result.isSuccess = true;
                result.data = memberInfo;

                logger.LogInformation("DecryptJwtKeySuccess");
                return StatusCode((int)HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
        }
    }
}
