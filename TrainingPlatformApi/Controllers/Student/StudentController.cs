using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;
using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.Services;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.VM.Student;

/* 學員 api */
namespace TrainingPlatformApi.Controllers.Student
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private MySqlConnection? conn;
        private ILogger<StudentController> logger;
        private HostService hostService;

        public StudentController(HostService _hostService, ILogger<StudentController> _logger)
        {
            hostService = _hostService;
            logger = _logger;
        }

        /// <summary>
        /// 取得學員清單
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/student/list")]
        public IActionResult GetStudentList()
        {
            ResultModels result = new ResultModels();
            
            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    var studentList = new StudentModels(conn).GetStudentList<StudentListVM>(hostService.Domain, hostService.ImagePath);

                    List<LatestStudentListVM> latestStudentList = new List<LatestStudentListVM>();

                    // 如果學員清單不是空值在塞入最新學員名單
                    if (studentList != null)
                    {
                        studentList.ToList().ForEach((obj) =>
                        {
                            if (latestStudentList.Count() == 0)
                            {
                                latestStudentList.Add(new LatestStudentListVM()
                                {
                                    userAuthorizedId = obj.userAuthorizedId,
                                    name = obj.name,
                                    gender = obj.gender,
                                    genderMequence = obj.genderMequence,
                                    posture = obj.posture,
                                    age = obj.age,
                                    phone = obj.phone,
                                    address = obj.address,
                                    imageUrl = obj.imageUrl
                                });
                            }
                            else
                            {
                                if (latestStudentList[latestStudentList.Count - 1]?.userAuthorizedId != obj.userAuthorizedId)
                                {
                                    latestStudentList.Add(new LatestStudentListVM()
                                    {
                                        userAuthorizedId = obj.userAuthorizedId,
                                        name = obj.name,
                                        gender = obj.gender,
                                        genderMequence = obj.genderMequence,
                                        posture = obj.posture,
                                        age = obj.age,
                                        phone = obj.phone,
                                        address = obj.address,
                                        imageUrl = obj.imageUrl
                                    });
                                }
                            }
                        });
                    }

                    // 抓取 aha 和 acsm 問卷
                    var ahaAndAcsmQuestionnaire = new StudentModels(conn).AhaAndAcsmQuestionnaire<AhaAndAcsmQuestionnaireVM>();

                    // 將最新的學員清單在進去去判定危險族群
                    latestStudentList.ToList().ForEach(studentItem =>
                    {
                        // 檢查學員存在運動風險
                        var isStudentDangerGroup = ahaAndAcsmQuestionnaire.Where(w => w.userAuthorizedId == studentItem.userAuthorizedId && w.part != 0);

                        if (isStudentDangerGroup.Count() == 0)
                        {
                            studentItem.dangerGrading = "低危險群";
                            studentItem.dangerGradingMequence = 1;
                            studentItem.medicalHistory = "-";
                        }
                        else
                        {
                            // 高危險群
                            var highDangerGrading = isStudentDangerGroup.Where(w => (w.part == 1 || w.part == 2 || w.part == 3)).ToList();
                            studentItem.dangerGrading = "高危險群";
                            studentItem.medicalHistoryMequence = 1;
                            studentItem.dangerGradingMequence = 3;

                            // 如果學員有疾病史
                            if (highDangerGrading.Count() > 0)
                            {
                                // 檢查此學員那些疾病史
                                List<string> medicalHistoryArr = new MedicalHistoryUtils().InspactMedicalHistory(highDangerGrading);

                                // 如果多種疾病史成立
                                if (medicalHistoryArr.Count() > 0)
                                {
                                    medicalHistoryArr.ForEach((item) =>
                                    {
                                        studentItem.medicalHistory += item + "、";
                                    });

                                    studentItem.medicalHistory = studentItem.medicalHistory.Substring(0, studentItem.medicalHistory.Length - 1);
                                }
                                else
                                {
                                    studentItem.medicalHistory = "-";
                                    studentItem.medicalHistoryMequence = 0;
                                }
                            }
                            else
                            {
                                // 中低危險群
                                var lowRiskDangerGrading = isStudentDangerGroup.Where(w => w.part == 4);
                                studentItem.medicalHistoryMequence = 0;

                                if (lowRiskDangerGrading.Count() >= 2)
                                {
                                    studentItem.dangerGrading = "中危險群";
                                    studentItem.medicalHistory = "-";
                                    studentItem.dangerGradingMequence = 2;
                                }
                                else
                                {
                                    studentItem.dangerGrading = "低危險群";
                                    studentItem.medicalHistory = "-";
                                    studentItem.dangerGradingMequence = 1;
                                }
                            }
                        }
                    });

                    result.isSuccess = true;
                    result.data = latestStudentList;
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 停用學員
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPatch]
        [Route("api/student/disabled/{studentId:int}/replace")]
        public IActionResult DisabledStudent(int studentId)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    var studentExist = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (studentExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "NoSuchStudentFound";
                        logger.LogError("NoSuchStudentFound");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 更新學員停用
                    var deleteStudentRow = new StudentModels(conn).UpdateDisabledStudent(studentId);

                    // 如果學員未更新成功
                    if (deleteStudentRow != 1)
                    {
                        transaction.Rollback();
                        result.isSuccess = false;
                        result.message = "DeleteStudentFail";
                        logger.LogError("DeleteStudentFail");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                    else
                    {
                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "DeleteStudentSuccess";
                        logger.LogInformation("DeleteStudentSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 更新學員基本資訊
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/base/{studentId:int}/info/replace")]
        public IActionResult ReplactStudentBaseInfo(int studentId, RequestStudentInfoModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }
            
            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    var isRepeatPhone = new StudentModels(conn).InspactRepeatPhone<PhoneRepeatVM>(body.phone);

                    // 沒有重複手機資料
                    if (isRepeatPhone == null)
                    {
                        // 更新學員資料
                        new StudentModels(conn).UpdateStudentInfo(recordId, studentId, student.phone, body);
                    }
                    else
                    {
                        if (isRepeatPhone.phone == student.phone)
                        {
                            new StudentModels(conn).UpdateStudentInfo(recordId, studentId, student.phone, body);
                        }
                        else
                        {
                            result.isSuccess = false;
                            result.message = "UserPhoneAlreadyRegistered";
                            logger.LogError("UserPhoneAlreadyRegistered");
                            return StatusCode((int)HttpStatusCode.Forbidden, result);
                        }
                    }

                    // 今日日期
                    string today = new DayUtils().GetFullDate();

                    // 檢查今日是否有體態資料
                    var todayBodyMassRecord = new StudentModels(conn).InspactTodayBodyMassRecord<TodayFirstBodyMassRecordVM>(studentId, today);

                    // 如果今日沒有體態資料
                    if (todayBodyMassRecord == null)
                    {
                        new StudentModels(conn).InsertBodyMassRecord(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertStudentInfoSuccess";
                        logger.LogInformation("InsertStudentInfoSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new StudentModels(conn).UpdateBodyMassRecord(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateStudentInfoSuccess";
                        logger.LogInformation("UpdateStudentInfoSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得某學員最新基本資料
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/base/{studentId:int}/info")]
        public IActionResult GetStudentBaseInfo(int studentId)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 最新學員資訊
                    LatestStudentInfoVM latestStudentInfo = new LatestStudentInfoVM();

                    var onlyStudnetInfo = new StudentModels(conn).GetOnlyStudentInfo<GetOnlyStudentInfoVM>(studentId, hostService.Domain, hostService.ImagePath);

                    // 組合最新學員資訊
                    latestStudentInfo.userAuthorizedId = onlyStudnetInfo.userAuthorizedId;
                    latestStudentInfo.phone = onlyStudnetInfo.phone;
                    latestStudentInfo.name = onlyStudnetInfo.name;
                    latestStudentInfo.gender = onlyStudnetInfo.gender;
                    latestStudentInfo.age = onlyStudnetInfo.age;
                    latestStudentInfo.email = onlyStudnetInfo.email;
                    latestStudentInfo.address = onlyStudnetInfo.address;
                    latestStudentInfo.imageUrl = onlyStudnetInfo.imageUrl;
                    latestStudentInfo.posture = onlyStudnetInfo.posture;
                    
                    // 撈取最新 aha 和 acsm 問卷
                    var onlyStudentAhaAndAscmQuestionnaireInfo = new StudentModels(conn).GetOnlyAhaAndAcsmQuestionnaire<GetOnlyAhaAndAcsmQuestionnaireVM>(studentId);

                    if (onlyStudentAhaAndAscmQuestionnaireInfo.Count() == 0)
                    {
                        latestStudentInfo.dangerGrading = "低危險群";
                        latestStudentInfo.medicalHistory = "-";
                    }
                    else
                    {
                        var highDangerGrading = onlyStudentAhaAndAscmQuestionnaireInfo.Where(w => (w.part == 1 || w.part == 2 || w.part == 3)).ToList();

                        if (highDangerGrading.Count() > 0)
                        {
                            List<string> medicalHistoryArr = new MedicalHistoryUtils().InspactMedicalHistory(highDangerGrading);

                            latestStudentInfo.dangerGrading = "高危險群";
                            
                            if (medicalHistoryArr.Count() > 0)
                            {
                                medicalHistoryArr.ForEach((item) =>
                                {
                                    latestStudentInfo.medicalHistory += item + "、";
                                });

                                latestStudentInfo.medicalHistory = latestStudentInfo.medicalHistory.Substring(0, latestStudentInfo.medicalHistory.Length - 1);
                            }
                            else
                            {
                                latestStudentInfo.medicalHistory = "-";
                            }
                        }
                        else
                        {
                            // 中低危險群
                            var lowRiskDangerGrading = onlyStudentAhaAndAscmQuestionnaireInfo.Where(w => w.part == 4);

                            if (lowRiskDangerGrading.Count() >= 2)
                            {
                                latestStudentInfo.dangerGrading = "中危險群";
                                latestStudentInfo.medicalHistory = "-";
                            }
                            else
                            {
                                latestStudentInfo.dangerGrading = "低危險群";
                                latestStudentInfo.medicalHistory = "-";
                            }
                        }
                    }

                    result.isSuccess = true;
                    result.data = latestStudentInfo;
                    logger.LogInformation("GetLatestStudentSuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
