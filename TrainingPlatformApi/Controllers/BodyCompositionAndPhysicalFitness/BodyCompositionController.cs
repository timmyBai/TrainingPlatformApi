using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;

using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.Models;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.VM.BodyMassAndPhysicalFitness;
using TrainingPlatformApi.VM.Student;
using TrainingPlatformApi.Models.BodyMassAndPhysicalFitness;

/*
 * 身體組成與體適能 api
 */
namespace TrainingPlatformApi.Controllers.BodyMassAndPhysicalFitness
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class BodyCompositionController : ControllerBase
    {
        private ILogger<BodyCompositionController> logger;
        private MySqlConnection? conn;

        public BodyCompositionController(ILogger<BodyCompositionController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 新增或更學員 bmi 與 備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/bodyCompositionRecord/{studentId:int}/bmi/replace")]
        public IActionResult ReplaceBodyCompositionBmiRecord(int studentId, RequestBodyCompositionBmiModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = DateTime.Now.ToString("yyyyMMdd");

                    // 檢查今日是否有 bmi 指數資料
                    var todayBodyMassRecord = new BodyCompositionModels(conn).InspactTodayBodyCompositionBmiRecord<TodayBodyCompositionBmiVM>(studentId, today);

                    // 如果找不到最新資料
                    if (todayBodyMassRecord == null)
                    {
                        // 新增學員 bmi 及 備註資料
                        new BodyCompositionModels(conn).InsertBodyCompositionBmiRecord(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertBossMassRecordBmiSuccess";
                        logger.LogInformation("InsertBossMassRecordBmiSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        // 更新學員 bmi 及 備註資料
                        new BodyCompositionModels(conn).UpdateBodyCompositionBmiRecord(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateBossMassRecordBmiSuccess";
                        logger.LogInformation("UpdateBossMassRecordBmiSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員 體脂肪 與 備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/bodyCompositionRecord/{studentId:int}/bodyFat/replace")]
        public IActionResult ReplaceBodyCompositionBodyFatRecord(int studentId, RequestBodyCompositionBodyFatModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = DateTime.Now.ToString("yyyyMMdd");

                    // 檢查今日是否有 體脂肪 指數資料
                    var todayBodyMassRecord = new BodyCompositionModels(conn).InspactTodayBodyCompositionBodyFatRecord<TodayBodyCompositionBodyFatVM>(studentId, today);

                    // 如果找不到最新資料
                    if (todayBodyMassRecord == null)
                    { 
                        // 新增學員 體脂肪 及 備註資料
                        new BodyCompositionModels(conn).InsertBodyCompositionBodyFatRecord(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertBossMassRecordBodyFatSuccess";
                        logger.LogInformation("InsertBossMassRecordBodyFatSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        // 更新學員 體脂肪 及 備註資料
                        new BodyCompositionModels(conn).UpdateBodyCompositionBodyFatRecord(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateBossMassRecordBodyFatSuccess";
                        logger.LogInformation("UpdateBossMassRecordBodyFatSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員 內臟脂肪 與 備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/bodyCompositionRecord/{studentId:int}/visceralFat/replace")]
        public IActionResult ReplaceBodyCompositionVisceralFatRecord(int studentId, RequestBodyCompositionVisceralFatModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = DateTime.Now.ToString("yyyyMMdd");

                    // 查詢最新身體組成資料
                    var todayBodyMassRecord = new BodyCompositionModels(conn).InspactTodayBodyCompositionVisceralFatRecord<TodayBodyCompositionVisceralFatVM>(studentId, today);

                    // 如果找不到最新資料
                    if (todayBodyMassRecord == null)
                    {
                        // 新增學員 內臟脂肪 及 備註資料
                        new BodyCompositionModels(conn).InsertBodyCompositionVisceralFatRecord(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertBossMassRecordVisceralFatSuccess";
                        logger.LogInformation("InsertBossMassRecordVisceralFatSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        // 更新學員 內臟脂肪 及 備註資料
                        new BodyCompositionModels(conn).UpdateBodyCompositionVisceralFatRecord(recordId, studentId, today, body);
                        
                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateBossMassRecordVisceralFatSuccess";
                        logger.LogInformation("UpdateBossMassRecordVisceralFatSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員 骨骼心肌率 與 備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/bodyCompositionRecord/{studentId:int}/skeletalMuscleRate/replace")]
        public IActionResult ReplaceBodyCompositionSkeletalMuscleRateRecord(int studentId, RequestBodyCompositionSkeletalMuscleRateModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = DateTime.Now.ToString("yyyyMMdd");

                    // 查詢最新身體組成資料
                    var todayBodyMassRecord = new BodyCompositionModels(conn).InspactTodayBodyCompositionSkeletalMuscleRateRecord<TodayBodyCompositionBodyFatVM>(studentId, today);

                    // 如果找不到最新資料
                    if (todayBodyMassRecord == null)
                    {
                        // 新增學員 骨骼心肌率 及 備註資料
                        new BodyCompositionModels(conn).InsertBodyCompositionSkeletalMuscleRateRecord(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertBossMassRecordSkeletalMuscleRateSuccess";
                        logger.LogInformation("InsertBossMassRecordVisceralFatSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                    else
                    {
                        // 更新學員 骨骼心肌率 及 備註資料
                        new BodyCompositionModels(conn).UpdateBodyCompositionSkeletalMuscleRateRecord(recordId, studentId, today, body);
                        
                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateBossMassRecordSkeletalMuscleRateSuccess";
                        logger.LogInformation("UpdateBossMassRecordVisceralFatSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
