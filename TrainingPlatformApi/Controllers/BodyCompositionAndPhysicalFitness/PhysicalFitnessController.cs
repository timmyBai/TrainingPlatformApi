using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;

using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.BodyMassAndPhysicalFitness;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.VM.BodyMassAndPhysicalFitness;
using TrainingPlatformApi.VM.Student;

/*
 * 身體組成與體適能 api
 */
namespace TrainingPlatformApi.Controllers.BodyMassAndPhysicalFitness
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class PhysicalFitnessController : ControllerBase
    {
        private MySqlConnection? conn;
        private readonly ILogger logger;

        public PhysicalFitnessController(ILogger<PhysicalFitnessController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 新增或更新學員體適能-心肺適能 登階測驗與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessCardiorespiratoryFitness/{studentId:int}/stepTest/mark/replace")]
        public IActionResult ReplacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMark(int studentId, RequestPhysicalFitnessCardiorespiratoryFitnessStepTestModels body)
        {
            ResultModels result = new ResultModels();
            
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-心肺適能資料
                    var todayPhysicalFitnessCardiorespiratoryFitness = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessCardiorespiratoryFitnessRecord<InspactTodayPhysicalFitnessCardiorespiratoryFitnessVM>(studentId, today);
                    
                    if (student.age >= 21 && student.age <= 65)
                    {
                        if (todayPhysicalFitnessCardiorespiratoryFitness == null)
                        {
                            // 新增 體適能-心肺適能 登階測驗與備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessCardiorespiratoryStepTestAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertPhysicalFitnessCardiorespiratoryFitnessSetupAndMarkSuccess";
                            logger.LogInformation("InsertPhysicalFitnessCardiorespiratoryFitnessSetupAndMarkSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-心肺適能 登階測驗與備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessCardiorespiratoryStepTestAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdatePhysicalFitnessCardiorespiratoryFitnessSetupAndMarkSuccess";
                            logger.LogInformation("UpdatePhysicalFitnessCardiorespiratoryFitnessMarkSuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf21To65";
                        logger.LogError("UnderTheAgeOf21To65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-心肺適能 原地站立抬膝與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessCardiorespiratoryFitness/{studentId:int}/kneeLiftFrequency/mark/replace")]
        public IActionResult ReplacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaise(int studentId, RequestPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-肌力與肌耐力資料
                    var todayPhysicalFitnessCardiorespiratoryFitness = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessCardiorespiratoryFitnessRecord<InspactTodayPhysicalFitnessCardiorespiratoryFitnessVM>(studentId, today);
                    
                    if (student.age > 65)
                    {
                        if (todayPhysicalFitnessCardiorespiratoryFitness == null)
                        {
                            // 新增 體適能-肌力與肌耐力 屈膝仰臥起坐與備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessCardiorespiratoryKneeLiftFrequencyAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertCardiorespiratoryFitnessKneeLiftFrequencySuccess";
                            logger.LogInformation("InsertCardiorespiratoryFitnessKneeLiftFrequencySuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-肌力與肌耐力 屈膝仰臥起坐與備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessCardiorespiratoryKneeLiftFrequencyAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateCardiorespiratoryFitnessKneeLiftFrequencySuccess";
                            logger.LogInformation("UpdateCardiorespiratoryFitnessKneeLiftFrequencySuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf65";
                        logger.LogError("UnderTheAgeOf65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-肌力與肌耐力 屈膝仰臥起坐與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessMuscleStrengthAndMuscleEndurance/{studentId:int}/kneeCrunchesFrequency/mark/replace")]
        public IActionResult ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequency(int studentId, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-肌力與肌耐力資料
                    var todayPhysicalFitnessMuscleStrengthAndMuscleEndurance = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEndurance<InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEnduranceVM>(studentId, today);
                    
                    if (student.age >= 21 && student.age <= 65)
                    {
                        if (todayPhysicalFitnessMuscleStrengthAndMuscleEndurance == null)
                        {
                            // 新增 體適能-肌力與肌耐力 屈膝仰臥起坐或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencySuccess";
                            logger.LogInformation("InsertMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencySuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-肌力與肌耐力 屈膝仰臥起坐或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencySuccess";
                            logger.LogInformation("UpdateMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencySuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf21To65";
                        logger.LogError("UnderTheAgeOf21To65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-肌力與肌耐力 手臂彎舉(上肢)與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessMuscleStrengthAndMuscleEndurance/{studentId:int}/armCurlUpperBodyFrequency/replace")]
        public IActionResult ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency(int studentId, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-肌力與肌耐力資料
                    var todayPhysicalFitnessMuscleStrengthAndMuscleEndurance = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEndurance<InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEnduranceVM>(studentId, today);
                
                    if (student.age > 65)
                    {
                        if (todayPhysicalFitnessMuscleStrengthAndMuscleEndurance == null)
                        {
                            // 新增 體適能-肌力與肌耐力 手臂彎舉(上肢)或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencySuccess";
                            logger.LogInformation("InsertMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencySuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-肌力與肌耐力 手臂彎舉(上肢)或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencySuccess";
                            logger.LogInformation("UpdateMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencySuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf65";
                        logger.LogError("UnderTheAgeOf65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-肌力與肌耐力 手臂彎舉(下肢)與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessMuscleStrengthAndMuscleEndurance/{studentId:int}/armCurlLowerBodyFrequency/mark/replace")]
        public IActionResult ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency(int studentId, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-肌力與肌耐力資料
                    var todayPhysicalFitnessMuscleStrengthAndMuscleEndurance = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEndurance<InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEnduranceVM>(studentId, today);
                    
                    if (student.age > 65)
                    {
                        if (todayPhysicalFitnessMuscleStrengthAndMuscleEndurance == null)
                        {
                            // 新增 體適能-肌力與肌耐力 手臂彎舉(下肢)或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencySuccess";
                            logger.LogInformation("InsertMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencySuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-肌力與肌耐力 手臂彎舉(下肢)或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencySuccess";
                            logger.LogInformation("UpdateMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencySuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf65";
                        logger.LogError("UnderTheAgeOf65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-柔軟度 坐姿體前彎與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessSoftness/{studentId:int}/seatedForwardBend/mark/replace")]
        public IActionResult ReplacePhysicalFitnessSoftnessSeatedForwardBend(int studentId, RequestPysicalFitnessSoftnessSeatedForwardBendModels body)
        {
            ResultModels result = new ResultModels();
            
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-柔軟度資料
                    var todayPhysicalFitnessSoftness = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessSoftness<InspactTodayPhysicalFitnessSoftnessVM>(studentId, today);

                    if (student.age >= 21 && student.age <= 65)
                    {
                        if (todayPhysicalFitnessSoftness == null)
                        {
                            // 新增 體適能-柔軟度 坐姿體前彎或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessSoftnessSeatedForwardBendAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertSoftnessSeatedForwardBendSuccess";
                            logger.LogInformation("InsertSoftnessSeatedForwardBendSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-柔軟度 坐姿體前彎或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessSoftnessSeatedForwardBendAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateSoftnessSeatedForwardBendSuccess";
                            logger.LogInformation("UpdateSoftnessSeatedForwardBendSuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf21To65";
                        logger.LogError("UnderTheAgeOf21To65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-柔軟度 椅子坐姿體前彎與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessSoftness/{studentId:int}/chairSeatedForwardBend/mark/replace")]
        public IActionResult ReplacePhysicalFitnessSoftnessChairSeatedForwardBend(int studentId, RequestPysicalFitnessSoftnessChairSeatedForwardBendModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-柔軟度資料
                    var todayPhysicalFitnessSoftness = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessSoftness<InspactTodayPhysicalFitnessSoftnessVM>(studentId, today);
                    
                    if (student.age > 65)
                    {
                        if (todayPhysicalFitnessSoftness == null)
                        {
                            // 新增 體適能-柔軟度 椅子坐姿體前彎或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessSoftnessChairSeatedForwardBendAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertSoftnessChairSeatedForwardBendSuccess";
                            logger.LogInformation("InsertSoftnessChairSeatedForwardBendSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-柔軟度 椅子坐姿體前彎或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessSoftnessChairSeatedForwardBendAndMark(recordId, studentId, today, body);
                            
                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateSoftnessChairSeatedForwardBendSuccess";
                            logger.LogInformation("UpdateSoftnessChairSeatedForwardBendSuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf65";
                        logger.LogError("UnderTheAgeOf65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-柔軟度 拉背側驗與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessSoftness/{studentId:int}/pullBackTest/mark/replace")]
        public IActionResult ReplacePhysicalFitnessSoftnessPullBackTest(int studentId, RequestPysicalFitnessSoftnessPullBackModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-柔軟度資料
                    var todayPhysicalFitnessSoftness = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessSoftness<InspactTodayPhysicalFitnessSoftnessVM>(studentId, today);
                    
                    if (student.age > 65)
                    {
                        if (todayPhysicalFitnessSoftness == null)
                        {
                            // 新增 體適能-柔軟度 拉背測驗或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessSoftnessPullBackTestAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertSoftnessPullBackTestSuccess";
                            logger.LogInformation("InsertSoftnessPullBackTestSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-柔軟度 拉背測驗或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessSoftnessPullBackTestAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateSoftnessPullBackTestSuccess";
                            logger.LogInformation("UpdateSoftnessPullBackTestSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf65";
                        logger.LogError("UnderTheAgeOf65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-平衡 壯年開眼單足立與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessBalance/{studentId:int}/adultEyeOpeningMonopodSecond/mark/replace")]
        public IActionResult ReplacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecond(int studentId, RequestPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondModels body)
        {
            ResultModels result = new ResultModels();
            
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-平衡資料
                    var todayPhysicalFitnessBalance = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessBalance<InspactTodayPhysicalFitnessBalanceVM>(studentId, today);

                    if (student.age >= 21 && student.age <= 65)
                    {
                        if (todayPhysicalFitnessBalance == null)
                        {
                            // 新增 體適能-平衡 壯年開眼單足立或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertBalanceAdultEyeOpeningMonopodSecondSuccess";
                            logger.LogInformation("InsertBalanceAdultEyeOpeningMonopodSecondSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-平衡 老年開眼單足立或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateBalanceAdultEyeOpeningMonopodSecondSuccess";
                            logger.LogInformation("UpdateBalanceAdultEyeOpeningMonopodSecondSuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf21To65";
                        logger.LogError("UnderTheAgeOf21To65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-平衡 老年開眼單足立與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessBalance/{studentId:int}/elderlyEyeOpeningMonopodSecond/mark/replace")]
        public IActionResult ReplacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecond(int studentId, RequestPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondModels body)
        {
            ResultModels result = new ResultModels();
            
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-平衡資料
                    var todayPhysicalFitnessBalance = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessBalance<InspactTodayPhysicalFitnessBalanceVM>(studentId, today);
                    
                    if (student.age > 65)
                    {
                        if (todayPhysicalFitnessBalance == null)
                        {
                            // 新增 體適能-平衡 老年開眼單足立或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertBalanceElderlyEyeOpeningMonopodSecondSuccess";
                            logger.LogInformation("InsertBalanceElderlyEyeOpeningMonopodSecondSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-平衡 老年開眼單足立或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateBalanceElderlyEyeOpeningMonopodSecondSuccess";
                            logger.LogInformation("UpdateBalanceElderlyEyeOpeningMonopodSecondSuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf65";
                        logger.LogError("UnderTheAgeOf65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-平衡 坐立繞物第一次秒數與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessBalance/{studentId:int}/sittingAroundOneSecond/mark/replace")]
        public IActionResult ReplacePhysicalFitnessBalanceSittingAroundOneSecond(int studentId, RequestPhysicalFitnessBalanceSittingAroundOneSecondModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-平衡資料
                    var todayPhysicalFitnessBalance = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessBalance<InspactTodayPhysicalFitnessBalanceVM>(studentId, today);
                
                    if (student.age > 65)
                    {
                        if (todayPhysicalFitnessBalance == null)
                        {
                            // 新增 體適能-平衡 坐立繞物第一次秒數或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessBalanceSittingAroundOneSecondAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertBalanceSittingAroundOneSecondSuccess";
                            logger.LogInformation("InsertBalanceSittingAroundOneSecondSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-平衡 坐立繞物第一次秒數或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessBalanceSittingAroundOneSecondAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateBalanceSittingAroundOneSecondSuccess";
                            logger.LogInformation("UpdateBalanceSittingAroundOneSecondSuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf65";
                        logger.LogError("UnderTheAgeOf65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-平衡 坐立繞物第二次秒數與備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessBalance/{studentId:int}/sittingAroundTwoSecond/mark/replace")]
        public IActionResult ReplacePhysicalFitnessBalanceSittingAroundTwoSecond(int studentId, RequestPhysicalFitnessBalanceSittingAroundTwoSecondModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-平衡資料
                    var todayPhysicalFitnessBalance = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessBalance<InspactTodayPhysicalFitnessBalanceVM>(studentId, today);

                    if (student.age > 65)
                    {
                        if (todayPhysicalFitnessBalance == null)
                        {
                            // 新增 體適能-平衡 坐立繞物第二次秒數或備註
                            new PhysicalFitnessModels(conn).InsertPhysicalFitnessBalanceSittingAroundTwoSecondAndMark(recordId, studentId, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertBalanceSittingAroundTwoSecondSuccess";
                            logger.LogInformation("InsertBalanceSittingAroundTwoSecondSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            // 更新 體適能-平衡 坐立繞物第二次秒數或備註
                            new PhysicalFitnessModels(conn).UpdatePhysicalFitnessBalanceSittingAroundTwoSecondAndMark(recordId, studentId, today, body);

                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "UpdateBalanceSittingAroundTwoSecondSuccess";
                            logger.LogInformation("UpdateBalanceSittingAroundTwoSecondSuccess");
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "UnderTheAgeOf65";
                        logger.LogError("UnderTheAgeOf65");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-心肺適能 備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessCardiorespiratoryFitness/{studentId:int}/mark/replace")]
        public IActionResult ReplacePhysicalFitnessCardiorespiratoryFitnessMark(int studentId, RequestPhysicalFitnessCardiorespiratoryFitnessMarkModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-心肺適能資料
                    var todayPhysicalFitnessCardiorespiratoryFitness = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessCardiorespiratoryFitnessRecord<InspactTodayPhysicalFitnessCardiorespiratoryFitnessVM>(studentId, today);

                    if (todayPhysicalFitnessCardiorespiratoryFitness == null)
                    {
                        new PhysicalFitnessModels(conn).InsertPhysicalFitnessCardiorespiratoryFitness(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertPhysicalFitnessCardiorespiratoryFitnessMarkSuccess";
                        logger.LogInformation("InsertPhysicalFitnessCardiorespiratoryFitnessMarkSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new PhysicalFitnessModels(conn).UpdatePhysicalFitnessCardiorespiratoryFitness(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdatePhysicalFitnessCardiorespiratoryFitnessMarkSuccess";
                        logger.LogInformation("UpdatePhysicalFitnessCardiorespiratoryFitnessMarkSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-肌力與肌耐力 備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessMuscleStrengthAndMuscleEndurance/{studentId:int}/mark/replace")]
        public IActionResult ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceMark(int studentId, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(setting.GetConnectionString()))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-肌力與肌耐力資料
                    var todayPhysicalFitnessMuscleStrengthAndMuscleEndurance = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEndurance<InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEnduranceVM>(studentId, today);

                    if (todayPhysicalFitnessMuscleStrengthAndMuscleEndurance == null)
                    {
                        new PhysicalFitnessModels(conn).InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceMark(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkSuccess";
                        logger.LogInformation("InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new PhysicalFitnessModels(conn).UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceMark(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkSuccess";
                        logger.LogInformation("UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-柔軟度 備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessSoftness/{studentId:int}/mark/replace")]
        public IActionResult ReplacePhysicalFitnessSoftnessMark(int studentId, RequestPhysicalFitnessSoftnessMarkModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-柔軟度資料
                    var todayPhysicalFitnessSoftness = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessSoftness<InspactTodayPhysicalFitnessSoftnessVM>(studentId, today);

                    if (todayPhysicalFitnessSoftness == null)
                    {
                        new PhysicalFitnessModels(conn).InsertPhysicalFitnessSoftnessMark(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertPhysicalFitnessSoftnessMarkSuccess";
                        logger.LogInformation("InsertPhysicalFitnessSoftnessMarkSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new PhysicalFitnessModels(conn).UpdatePhysicalFitnessSoftnessMark(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdatePhysicalFitnessSoftnessMarkSuccess";
                        logger.LogInformation("UpdatePhysicalFitnessSoftnessMarkSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新學員體適能-平衡 備註
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPut]
        [Route("api/physicalFitnessBalance/{studentId:int}/mark/replace")]
        public IActionResult ReplacePhysicalFitnessBalanceMark(int studentId, RequestPhysicalFitnessBalanceMarkModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    string today = new DayUtils().GetFullDate();

                    // 查詢最新體適能-平衡資料
                    var todayPhysicalFitnessBalance = new PhysicalFitnessModels(conn).InspactTodayPhysicalFitnessBalance<InspactTodayPhysicalFitnessBalanceVM>(studentId, today);

                    if (todayPhysicalFitnessBalance == null)
                    {
                        new PhysicalFitnessModels(conn).InsertPhysicalFitnessBalanceMark(recordId, studentId, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertPhysicalFitnessBalanceMarkSuccess";
                        logger.LogInformation("InsertPhysicalFitnessBalanceMarkSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                    else
                    {
                        new PhysicalFitnessModels(conn).UpdatePhysicalFitnessBalanceMark(recordId, studentId, today, body);

                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdatePhysicalFitnessBalanceMarkSuccess";
                        logger.LogInformation("UpdatePhysicalFitnessBalanceMarkSuccess");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
