using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;

using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.BodyMass;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.VM.BodyMassAndPhysicalFitness;
using TrainingPlatformApi.VM.Student;

/*
 * 身體組成與體適能 api
 */
namespace TrainingPlatformApi.Controllers.BodyMass
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class BodyCompositionAndPhysicalFitnessController : ControllerBase
    {
        private ILogger<BodyCompositionAndPhysicalFitnessController> logger;
        private MySqlConnection? conn;

        public BodyCompositionAndPhysicalFitnessController(ILogger<BodyCompositionAndPhysicalFitnessController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 身體組成與體適能評價清單
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/studentBodyCompositionAndPhysicalFitness/{studentId:int}/fetch")]
        public IActionResult GetStudentBodyCompositionAndPhysicalFitnessHistory(int studentId)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    BodyCompositionAndPhysicalFitnessVM bodyMassAndPhysicalFitness = new BodyCompositionAndPhysicalFitnessVM();

                    // 取得學員身體組成 bmi 歷史資料
                    var studentBmiList = new BodyCompositionAndPhysicalFitnessModels(conn).GetBodyCompositionBmiHistory(studentId, student.gender, student.age);

                    if (studentBmiList.Count() > 0)
                    {
                        bodyMassAndPhysicalFitness.bmiGroup.bmi = studentBmiList.ToList()[0].bmi;
                        bodyMassAndPhysicalFitness.bmiGroup.bmiCommentLevel = studentBmiList.ToList()[0].bmiCommentLevel;
                        bodyMassAndPhysicalFitness.bmiGroup.updateRecordId = studentBmiList.ToList()[0].updateRecordId;
                        bodyMassAndPhysicalFitness.bmiGroup.updateRecordDate = studentBmiList.ToList()[0].updateRecordDate;
                        bodyMassAndPhysicalFitness.bmiGroup.updateRecordTime = studentBmiList.ToList()[0].updateRecordTime;
                        bodyMassAndPhysicalFitness.bmiGroup.mark = studentBmiList.ToList()[0].mark;

                        for (int i = 0; i < studentBmiList.ToList().Count; i++)
                        {
                            if (i != 0)
                            {
                                bodyMassAndPhysicalFitness.bmiGroup.children.Add(new BmiGroupChildrenVM()
                                {
                                    bmi = studentBmiList.ToList()[i].bmi,
                                    bmiCommentLevel = studentBmiList.ToList()[i].bmiCommentLevel,
                                    updateRecordId = studentBmiList.ToList()[i].updateRecordId,
                                    updateRecordDate = studentBmiList.ToList()[i].updateRecordDate,
                                    updateRecordTime = studentBmiList.ToList()[i].updateRecordTime,
                                    mark = studentBmiList.ToList()[i].mark
                                });
                            }
                        }
                    }
                    else
                    {
                        bodyMassAndPhysicalFitness.bmiGroup.bmi = 0;
                        bodyMassAndPhysicalFitness.bmiGroup.bmiCommentLevel = "-";
                        bodyMassAndPhysicalFitness.bmiGroup.updateRecordId = 0;
                        bodyMassAndPhysicalFitness.bmiGroup.updateRecordDate = "";
                        bodyMassAndPhysicalFitness.bmiGroup.updateRecordTime = "";
                        bodyMassAndPhysicalFitness.bmiGroup.mark = "";
                    }


                    // 取得學員身體組成 體脂肪 歷史資料
                    var studentBodyFatList = new BodyCompositionAndPhysicalFitnessModels(conn).GetBodyCompositionBodyFatHistory(studentId, student.gender, student.age);

                    if (studentBodyFatList.Count() > 0)
                    {
                        bodyMassAndPhysicalFitness.bodyFatGroup.bodyFat = studentBodyFatList.ToList()[0].bodyFat;
                        bodyMassAndPhysicalFitness.bodyFatGroup.bodyFatCommentLevel = studentBodyFatList.ToList()[0].bodyFatCommentLevel;
                        bodyMassAndPhysicalFitness.bodyFatGroup.updateRecordId = studentBodyFatList.ToList()[0].updateRecordId;
                        bodyMassAndPhysicalFitness.bodyFatGroup.updateRecordDate = studentBodyFatList.ToList()[0].updateRecordDate;
                        bodyMassAndPhysicalFitness.bodyFatGroup.updateRecordTime = studentBodyFatList.ToList()[0].updateRecordTime;
                        bodyMassAndPhysicalFitness.bodyFatGroup.mark = studentBodyFatList.ToList()[0].mark;
                        
                        for(int i = 0; i < studentBodyFatList.Count(); i++)
                        {
                            if (i != 0)
                            {
                                bodyMassAndPhysicalFitness.bodyFatGroup.children.Add(new BodyFatGroupChildrenVM()
                                {
                                    bodyFat = studentBodyFatList.ToList()[i].bodyFat,
                                    bodyFatCommentLevel = studentBodyFatList.ToList()[i].bodyFatCommentLevel,
                                    updateRecordId = studentBodyFatList.ToList()[i].updateRecordId,
                                    updateRecordDate = studentBodyFatList.ToList()[i].updateRecordDate,
                                    updateRecordTime = studentBodyFatList.ToList()[i].updateRecordTime,
                                    mark = studentBodyFatList.ToList()[i].mark
                                });
                            }
                        }
                    }
                    else
                    {
                        bodyMassAndPhysicalFitness.bodyFatGroup.bodyFat = 0;
                        bodyMassAndPhysicalFitness.bodyFatGroup.bodyFatCommentLevel = "-";
                        bodyMassAndPhysicalFitness.bodyFatGroup.updateRecordId = 0;
                        bodyMassAndPhysicalFitness.bodyFatGroup.updateRecordDate = "";
                        bodyMassAndPhysicalFitness.bodyFatGroup.updateRecordTime = "";
                        bodyMassAndPhysicalFitness.bodyFatGroup.mark = "";
                    }


                    // 取得學員身體組成 內臟脂肪 歷史資料
                    var studentVisceralFatList = new BodyCompositionAndPhysicalFitnessModels(conn).GetBodyCompositionVisceralFatHistory(studentId);

                    if (studentVisceralFatList.Count() > 0)
                    {
                        bodyMassAndPhysicalFitness.visceralFatGroup.visceralFat = studentVisceralFatList.ToList()[0].visceralFat;
                        bodyMassAndPhysicalFitness.visceralFatGroup.visceralFatCommentLevel = studentVisceralFatList.ToList()[0].visceralFatCommentLevel;
                        bodyMassAndPhysicalFitness.visceralFatGroup.updateRecordId = studentVisceralFatList.ToList()[0].updateRecordId;
                        bodyMassAndPhysicalFitness.visceralFatGroup.updateRecordDate = studentVisceralFatList.ToList()[0].updateRecordDate;
                        bodyMassAndPhysicalFitness.visceralFatGroup.updateRecordTime = studentVisceralFatList.ToList()[0].updateRecordTime;
                        bodyMassAndPhysicalFitness.visceralFatGroup.mark = studentVisceralFatList.ToList()[0].mark;

                        for (int i = 0; i < studentVisceralFatList.Count(); i++)
                        {
                            if (i != 0)
                            {
                                bodyMassAndPhysicalFitness.visceralFatGroup.children.Add(new VisceralFatGroupChildrenVM()
                                {
                                    visceralFat = studentVisceralFatList.ToList()[i].visceralFat,
                                    visceralFatCommentLevel = studentVisceralFatList.ToList()[i].visceralFatCommentLevel,
                                    updateRecordId = studentVisceralFatList.ToList()[i].updateRecordId,
                                    updateRecordDate = studentVisceralFatList.ToList()[i].updateRecordDate,
                                    updateRecordTime = studentVisceralFatList.ToList()[i].updateRecordTime,
                                    mark = studentVisceralFatList.ToList()[i].mark
                                });
                            }
                        }
                    }
                    else
                    {
                        bodyMassAndPhysicalFitness.visceralFatGroup.visceralFat = 0;
                        bodyMassAndPhysicalFitness.visceralFatGroup.visceralFatCommentLevel = "-";
                        bodyMassAndPhysicalFitness.visceralFatGroup.updateRecordId = 0;
                        bodyMassAndPhysicalFitness.visceralFatGroup.updateRecordDate = "";
                        bodyMassAndPhysicalFitness.visceralFatGroup.updateRecordTime = "";
                        bodyMassAndPhysicalFitness.visceralFatGroup.mark = "";
                    }


                    // 取得學員身體組成 骨骼機率 歷史資料
                    var studentSkeletalMuscleRateList = new BodyCompositionAndPhysicalFitnessModels(conn).GetBodyCompositionSkeletalMuscleHistory(studentId, student.gender);

                    if (studentSkeletalMuscleRateList.Count() > 0)
                    {
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.skeletalMuscleRate = studentSkeletalMuscleRateList.ToList()[0].skeletalMuscleRate;
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.skeletalMuscleRateCommentLevel = studentSkeletalMuscleRateList.ToList()[0].skeletalMuscleRateCommentLevel;
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.updateRecordId = studentSkeletalMuscleRateList.ToList()[0].updateRecordId;
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.updateRecordDate = studentSkeletalMuscleRateList.ToList()[0].updateRecordDate;
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.updateRecordTime = studentSkeletalMuscleRateList.ToList()[0].updateRecordTime;
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.mark = studentSkeletalMuscleRateList.ToList()[0].mark;

                        for (int i = 0; i < studentSkeletalMuscleRateList.Count(); i++)
                        {
                            if (i != 0)
                            {
                                bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.children.Add(new SkeletalMuscleRateGroupChildrenVM()
                                {
                                    skeletalMuscleRate = studentSkeletalMuscleRateList.ToList()[i].skeletalMuscleRate,
                                    skeletalMuscleRateCommentLevel = studentSkeletalMuscleRateList.ToList()[i].skeletalMuscleRateCommentLevel,
                                    updateRecordId = studentSkeletalMuscleRateList.ToList()[i].updateRecordId,
                                    updateRecordDate = studentSkeletalMuscleRateList.ToList()[i].updateRecordDate,
                                    updateRecordTime = studentSkeletalMuscleRateList.ToList()[i].updateRecordTime,
                                    mark = studentSkeletalMuscleRateList.ToList()[i].mark
                                });
                            }
                        }
                    }
                    else
                    {
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.skeletalMuscleRate = 0;
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.skeletalMuscleRateCommentLevel = "-";
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.updateRecordId = 0;
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.updateRecordDate = "";
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.updateRecordTime = "";
                        bodyMassAndPhysicalFitness.skeletalMuscleRateGroup.mark = "";
                    }


                    // 取得學員體適能心肺適能歷史資料
                    var physicalFitnessCardiorespiratoryFitnessList = new BodyCompositionAndPhysicalFitnessModels(conn).GetPhysicalFitnessCardiorespiratoryFitnessHistory(studentId, student.gender, student.age);

                    if (physicalFitnessCardiorespiratoryFitnessList.Count() > 0)
                    {
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.cardiorespiratoryFitnessFraction = physicalFitnessCardiorespiratoryFitnessList.ToList()[0].cardiorespiratoryFitness;
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.cardiorespiratoryFitnessComment = physicalFitnessCardiorespiratoryFitnessList.ToList()[0].cardiorespiratoryFitnessCommentLevel;
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.updateRecordId = physicalFitnessCardiorespiratoryFitnessList.ToList()[0].updateRecordId;
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.updateRecordDate = physicalFitnessCardiorespiratoryFitnessList.ToList()[0].updateRecordDate;
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.updateRecordTime = physicalFitnessCardiorespiratoryFitnessList.ToList()[0].updateRecordTime;
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.mark = physicalFitnessCardiorespiratoryFitnessList.ToList()[0].mark;

                        for (int i = 0; i < physicalFitnessCardiorespiratoryFitnessList.Count(); i++)
                        {
                            if (i != 0)
                            {
                                bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.children.Add(new PhysicalFitnessCardiorespiratoryFitnessGroupChildrenVM()
                                {
                                    cardiorespiratoryFitnessFraction = physicalFitnessCardiorespiratoryFitnessList.ToList()[i].cardiorespiratoryFitness,
                                    cardiorespiratoryFitnessComment = physicalFitnessCardiorespiratoryFitnessList.ToList()[i].cardiorespiratoryFitnessCommentLevel,
                                    updateRecordId = physicalFitnessCardiorespiratoryFitnessList.ToList()[i].updateRecordId,
                                    updateRecordDate = physicalFitnessCardiorespiratoryFitnessList.ToList()[i].updateRecordDate,
                                    updateRecordTime = physicalFitnessCardiorespiratoryFitnessList.ToList()[i].updateRecordTime,
                                    mark = physicalFitnessCardiorespiratoryFitnessList.ToList()[i].mark
                                });
                            }
                        }
                    }
                    else
                    {
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.cardiorespiratoryFitnessFraction = 0;
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.cardiorespiratoryFitnessComment = "-";
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.updateRecordId = 0;
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.updateRecordDate = "";
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.updateRecordTime = "";
                        bodyMassAndPhysicalFitness.physicalFitnessCardiorespiratoryFitnessGroup.mark = "";
                    }


                    // 取得體適能肌力與肌耐力歷史資料
                    var physicalFitnessMuscleStrengthAndMuscleEnduranceList = new BodyCompositionAndPhysicalFitnessModels(conn).GetPhysicalFitnessMuscleStrengthAndMuscleEnduranceHistory(studentId, student.gender, student.age);

                    if (physicalFitnessMuscleStrengthAndMuscleEnduranceList.Count() > 0)
                    {
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.muscleStrengthAndMuscleEnduranceFitness = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToArray()[0].muscleStrengthAndMuscleEnduranceFitness;
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.muscleStrengthAndMuscleEnduranceComment = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[0].muscleStrengthAndMuscleEnduranceCommentLevel;
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.updateRecordId = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[0].updateRecordId;
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.updateRecordDate = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[0].updateRecordDate;
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.updateRecordTime = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[0].updateRecordTime;
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.mark = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[0].mark;

                        for (int i = 0; i < physicalFitnessMuscleStrengthAndMuscleEnduranceList.Count(); i++)
                        {
                            if (i != 0)
                            {
                                bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.children.Add(new PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupChildrenVM()
                                {
                                    muscleStrengthAndMuscleEnduranceFitness = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[i].muscleStrengthAndMuscleEnduranceFitness,
                                    muscleStrengthAndMuscleEnduranceComment = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[i].muscleStrengthAndMuscleEnduranceCommentLevel,
                                    updateRecordId = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[i].updateRecordId,
                                    updateRecordDate = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[i].updateRecordDate,
                                    updateRecordTime = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[i].updateRecordTime,
                                    mark = physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[i].mark
                                });
                            }
                        }
                    }
                    else
                    {
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.muscleStrengthAndMuscleEnduranceFitness = 0;
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.muscleStrengthAndMuscleEnduranceComment = "-";
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.updateRecordId = 0;
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.updateRecordDate = "";
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.updateRecordTime = "";
                        bodyMassAndPhysicalFitness.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.mark = "";
                    }


                    // 取得體適能柔軟度歷史資料
                    var physicalFitnessSoftness = new BodyCompositionAndPhysicalFitnessModels(conn).GetPhysicalFitnessSoftnessHistory(studentId, student.gender, student.age);

                    if (physicalFitnessSoftness.Count() > 0)
                    {
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.softnessFitness = physicalFitnessSoftness.ToList()[0].softnessFitness;
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.softnessComment = physicalFitnessSoftness.ToList()[0].softnessComment;
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.updateRecordId = physicalFitnessSoftness.ToList()[0].updateRecordId;
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.updateRecordDate = physicalFitnessSoftness.ToList()[0].updateRecordDate;
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.updateRecordTime = physicalFitnessSoftness.ToList()[0].updateRecordTime;
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.mark = physicalFitnessSoftness.ToList()[0].mark;

                        for (int i = 0; i < physicalFitnessSoftness.Count(); i++)
                        {
                            if (i != 0)
                            {
                                bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.children.Add(new PhysicalFitnessSoftnessGroupChildrenVM()
                                {
                                    softnessFitness = physicalFitnessSoftness.ToList()[i].softnessFitness,
                                    softnessComment = physicalFitnessSoftness.ToList()[i].softnessComment,
                                    updateRecordId = physicalFitnessSoftness.ToList()[i].updateRecordId,
                                    updateRecordDate = physicalFitnessSoftness.ToList()[i].updateRecordDate,
                                    updateRecordTime = physicalFitnessSoftness.ToList()[i].updateRecordTime,
                                    mark = physicalFitnessSoftness.ToList()[i].mark
                                });
                            }
                        }
                    }
                    else
                    {
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.softnessFitness = 0;
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.softnessComment = "-";
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.updateRecordId = 0;
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.updateRecordDate = "";
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.updateRecordTime = "";
                        bodyMassAndPhysicalFitness.physicalFitnessSoftnessGroup.mark = "";
                    }


                    // 取得體適能平衡歷史資料
                    var physicalFitnessBalance = new BodyCompositionAndPhysicalFitnessModels(conn).GetPhysicalFitnessBalanceHistory(studentId, student.gender, student.age);

                    if (physicalFitnessBalance.Count() > 0)
                    {
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.balanceFitness = physicalFitnessBalance.ToList()[0].balanceFitness;
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.balanceComment = physicalFitnessBalance.ToList()[0].balanceComment;
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.updateRecordId = physicalFitnessBalance.ToList()[0].updateRecordId;
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.updateRecordDate = physicalFitnessBalance.ToList()[0].updateRecordDate;
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.updateRecordTime = physicalFitnessBalance.ToList()[0].updateRecordTime;
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.mark = physicalFitnessBalance.ToList()[0].mark;

                        for (int i = 0; i < physicalFitnessBalance.Count(); i++)
                        {
                            if (i != 0)
                            {
                                bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.children.Add(new PhysicalFitnessBalanceGroupChildrenVM()
                                {
                                    balanceFitness = physicalFitnessBalance.ToList()[i].balanceFitness,
                                    balanceComment = physicalFitnessBalance.ToList()[i].balanceComment,
                                    updateRecordId = physicalFitnessBalance.ToList()[i].updateRecordId,
                                    updateRecordDate = physicalFitnessBalance.ToList()[i].updateRecordDate,
                                    updateRecordTime = physicalFitnessBalance.ToList()[i].updateRecordTime,
                                    mark = physicalFitnessBalance.ToList()[i].mark
                                });
                            }
                        }
                    }
                    else
                    {
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.balanceFitness = 0;
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.balanceComment = "-";
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.updateRecordId = 0;
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.updateRecordDate = "";
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.updateRecordTime = "";
                        bodyMassAndPhysicalFitness.physicalFitnessBalanceGroup.mark = "";
                    }

                    result.isSuccess = true;
                    result.data = bodyMassAndPhysicalFitness;
                    logger.LogInformation("GetBodyAndPhysicalFitnessSuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
