using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;
using TrainingPlatformApi.Models;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.VM.Student;
using TrainingPlatformApi.Models.Print;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.Models.BodyMass;
using TrainingPlatformApi.Models.ExercisePrescription;
using TrainingPlatformApi.VM.ExercisePrescription;
using TrainingPlatformApi.Models.InstrumentSettings;
using TrainingPlatformApi.VM.InstrumentSettings;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using TrainingPlatformApi.Services;

namespace TrainingPlatformApi.Controllers.Print
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class PrintReportController : ControllerBase
    {
        private MySqlConnection? conn;
        private readonly ILogger<PrintReportController> logger;
        private readonly HostService hostService;

        public PrintReportController(HostService _hostService, ILogger<PrintReportController> _logger)
        {
            hostService = _hostService;
            logger = _logger;
        }

        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/print/training/{studentId:int:required}/pdf/report")]
        public IActionResult PrintTrainingReport(int studentId, RequestPrintTrainingReportModels body)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    LatestStudentInfoVM latestStudentInfo = new LatestStudentInfoVM();

                    var onlyStudnetInfo = new StudentModels(conn).GetOnlyStudentInfo<GetOnlyStudentInfoVM>(studentId, hostService.Domain, hostService.ImagePath);

                    // 組合最新學員資訊
                    latestStudentInfo.userAuthorizedId = onlyStudnetInfo.userAuthorizedId;
                    latestStudentInfo.phone = onlyStudnetInfo.phone;
                    latestStudentInfo.name = onlyStudnetInfo.name;
                    latestStudentInfo.gender = onlyStudnetInfo.gender;
                    latestStudentInfo.age = onlyStudnetInfo.age;
                    latestStudentInfo.email = onlyStudnetInfo.email;
                    latestStudentInfo.address = onlyStudnetInfo.address;
                    latestStudentInfo.imageUrl = onlyStudnetInfo.imageUrl;
                    latestStudentInfo.posture = onlyStudnetInfo.posture;

                    // 撈取最新 aha 和 acsm 問卷
                    var onlyStudentAhaAndAscmQuestionnaireInfo = new StudentModels(conn).GetOnlyAhaAndAcsmQuestionnaire<GetOnlyAhaAndAcsmQuestionnaireVM>(studentId);

                    if (onlyStudentAhaAndAscmQuestionnaireInfo.Count() == 0)
                    {
                        latestStudentInfo.dangerGrading = "低危險群";
                        latestStudentInfo.medicalHistory = "-";
                    }
                    else
                    {
                        var highDangerGrading = onlyStudentAhaAndAscmQuestionnaireInfo.Where(w => (w.part == 1 || w.part == 2 || w.part == 3)).ToList();

                        if (highDangerGrading.Count() > 0)
                        {
                            List<string> medicalHistoryArr = new MedicalHistoryUtils().InspactMedicalHistory(highDangerGrading);

                            latestStudentInfo.dangerGrading = "高危險群";

                            if (medicalHistoryArr.Count() > 0)
                            {
                                medicalHistoryArr.ForEach((item) =>
                                {
                                    latestStudentInfo.medicalHistory += item + "、";
                                });

                                latestStudentInfo.medicalHistory = latestStudentInfo.medicalHistory.Substring(0, latestStudentInfo.medicalHistory.Length - 1);
                            }
                            else
                            {
                                latestStudentInfo.medicalHistory = "-";
                            }
                        }
                        else
                        {
                            // 中低危險群
                            var lowRiskDangerGrading = onlyStudentAhaAndAscmQuestionnaireInfo.Where(w => w.part == 4);

                            if (lowRiskDangerGrading.Count() >= 2)
                            {
                                latestStudentInfo.dangerGrading = "中危險群";
                                latestStudentInfo.medicalHistory = "-";
                            }
                            else
                            {
                                latestStudentInfo.dangerGrading = "低危險群";
                                latestStudentInfo.medicalHistory = "-";
                            }
                        }
                    }

                    // 取得學員 bmi
                    var studentBmiList = new BodyCompositionAndPhysicalFitnessModels(conn).GetBodyCompositionBmiHistory(studentId, student.gender, student.age);

                    // 取得學員 體脂肪
                    var studentBodyFatList = new BodyCompositionAndPhysicalFitnessModels(conn).GetBodyCompositionBodyFatHistory(studentId, student.gender, student.age);
                    
                    // 取得學員 內臟脂肪
                    var studentVisceralFatList = new BodyCompositionAndPhysicalFitnessModels(conn).GetBodyCompositionVisceralFatHistory(studentId);

                    // 取得學員 骨骼心肌率
                    var studentSkeletalMuscleRateList = new BodyCompositionAndPhysicalFitnessModels(conn).GetBodyCompositionSkeletalMuscleHistory(studentId, student.gender);

                    // 取得體適能-心肺適能
                    var physicalFitnessCardiorespiratoryFitnessList = new BodyCompositionAndPhysicalFitnessModels(conn).GetPhysicalFitnessCardiorespiratoryFitnessHistory(studentId, student.gender, student.age);

                    // 取得體適能-肌力與肌耐力
                    var physicalFitnessMuscleStrengthAndMuscleEnduranceList = new BodyCompositionAndPhysicalFitnessModels(conn).GetPhysicalFitnessMuscleStrengthAndMuscleEnduranceHistory(studentId, student.gender, student.age);

                    // 取得體適能-柔軟度
                    var physicalFitnessSoftnessList = new BodyCompositionAndPhysicalFitnessModels(conn).GetPhysicalFitnessSoftnessHistory(studentId, student.gender, student.age);

                    // 取得體適能-平衡
                    var physicalFitnessBalance = new BodyCompositionAndPhysicalFitnessModels(conn).GetPhysicalFitnessBalanceHistory(studentId, student.gender, student.age);


                    // 取得最新運動處方-訓練階段
                    var latestExercisePrescriptionTrainingStage = new ExercisePrescriptionListModels(conn).GetLatestExercisePrescriptionTrainingStage<LatestExercisePrescriptionListVM>(studentId);

                    // 取得運動處方-心肺適能
                    var exercisePrescriptionCardiorespiratoryFitnessList = new ExercisePrescriptionListModels(conn).GetExercisePrescriptionCardiorespiratoryFitnessHistory<ExercisePrescriptionCardiorespiratoryFitnessHistoryVM>(studentId);
                    
                    // 取得運動處方-肌力與肌耐力
                    var exercisePrescriptionMuscleStrengthAndMuscleEnduranceList = new ExercisePrescriptionListModels(conn).GetExercisePrescriptionMuscleStrengthAndMuscleEnduranceHistory<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceHistoryVM>(studentId);

                    // 取得運動處方-柔軟度
                    var exercisePrescriptionSoftnessList = new ExercisePrescriptionListModels(conn).GetExercisePrescriptionSoftnessHistory<ExercisePrescriptionSoftnessHistoryVM>(studentId);

                    // 取得運動處方-平衡
                    var exercisePrescriptionBalanceList = new ExercisePrescriptionListModels(conn).GetExercisePrescriptionBalanceHistory<ExercisePrescriptionBalanceHistoryVM>(studentId);

                    // 取得儀器設定值
                    var instrumentSettingSportList = new InstrumentSettingListModels(conn).GetInstrumentSettingSportsPerformanceList<InstrumentSettingSportsPerformanceListVM>(studentId);

                    var msPdfBytes = new PrintReportModels(body).PrintTrainingReportPdfInfo(latestStudentInfo, studentBmiList, studentBodyFatList, studentVisceralFatList, studentSkeletalMuscleRateList, physicalFitnessCardiorespiratoryFitnessList, physicalFitnessMuscleStrengthAndMuscleEnduranceList, physicalFitnessSoftnessList, physicalFitnessBalance, latestExercisePrescriptionTrainingStage, exercisePrescriptionCardiorespiratoryFitnessList, exercisePrescriptionMuscleStrengthAndMuscleEnduranceList, exercisePrescriptionSoftnessList, exercisePrescriptionBalanceList, instrumentSettingSportList);

                    return File(msPdfBytes, "application/pdf", "等速肌力訓練報表.pdf");
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
