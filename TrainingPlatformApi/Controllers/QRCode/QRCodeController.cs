using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using QRCoder;

using TrainingPlatformApi.Models;
using MySql.Data.MySqlClient;
using TrainingPlatformApi.Models.QRCode;
using TrainingPlatformApi.VM.QRCode;
using TrainingPlatformApi.Settings;
using Microsoft.AspNetCore.Authorization;

/*
 * QR Code api
 */
namespace TrainingPlatformApi.Controllers.QRCode
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class QrcodeController : ControllerBase
    {
        private MySqlConnection? conn;
        private readonly ILogger<QrcodeController> logger;

        public QrcodeController(ILogger<QrcodeController> _logger)
        {
            logger = _logger;
        }

        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/qrcode/{studentId:int}/generate")]
        public IActionResult GetQRCode(int studentId)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    var studentInfo = new QRCodeModels(conn).InspectStudentPhone<InspectStudentPhoneVM>(studentId);
                    
                    if (studentInfo == null)
                    {
                        result.isSuccess = false;
                        result.message = "NoStudentInformationFound";
                        logger.LogError("NoStudentInformationFound");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    if (string.IsNullOrWhiteSpace(studentInfo.phone))
                    {
                        result.isSuccess = false;
                        result.message = "StudentMobilePhoneInformationIsBlank";
                        logger.LogError("StudentMobilePhoneInformationIsBlank");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    QRCodeGenerator generator = new QRCodeGenerator();
                    QRCodeData codeData = generator.CreateQrCode(studentInfo.phone, QRCodeGenerator.ECCLevel.H);
                    QRCoder.QRCode qrcode = new QRCoder.QRCode(codeData);

                    Bitmap icon = new Bitmap($"{AppDomain.CurrentDomain.BaseDirectory}Assets/emg_logo.png");
                    Bitmap qrCodeIconImage = qrcode.GetGraphic(50, Color.Black, Color.White, icon, 15, 30, true);

                    Bitmap backgrond = new Bitmap(qrCodeIconImage.Width, qrCodeIconImage.Height);
                    backgrond.MakeTransparent();

                    Graphics g2 = Graphics.FromImage(backgrond);
                    g2.Clear(Color.Transparent);

                    g2.DrawImage(qrCodeIconImage, 0, 0);

                    FontFamily fontFamily = new FontFamily("微軟正黑體");
                    Font font = new Font(fontFamily, 100f, FontStyle.Bold, GraphicsUnit.Pixel);

                    int strWidth = (int)g2.MeasureString(studentInfo.phone, font).Width;

                    int wordStartX = (qrCodeIconImage.Width - strWidth) / 2;
                    int wordStartY = qrCodeIconImage.Height - 150;

                    g2.DrawString(studentInfo.phone, font, Brushes.Black, wordStartX, wordStartY);

                    //backgrond.Save($"{studentInfo.phone}.png", ImageFormat.Png);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        qrCodeIconImage.Save(ms, ImageFormat.Png);
                        ms.Seek(0, SeekOrigin.Begin);

                        byte[] arr = ms.ToArray();

                        result.isSuccess = true;
                        result.data = "data:image/png;base64," + Convert.ToBase64String(arr);
                    }
                    
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.ToString());
            }
        }
    }
}
