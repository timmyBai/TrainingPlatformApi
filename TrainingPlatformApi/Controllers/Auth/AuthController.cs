using System.Net;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using MySql.Data.MySqlClient;

using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.Auth;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.VM.Auth;
using TrainingPlatformApi.Models.BodyMassAndPhysicalFitness;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;
using TrainingPlatformApi.Services;

/*
 * 會員登入與註冊 api
 */
namespace TrainingPlatformApi.Controllers.Auth
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private MySqlConnection? conn;
        private readonly ILogger<AuthController> logger;
        private readonly HostService hostService;

        public AuthController(HostService _hostService, ILogger<AuthController> _logger)
        {
            hostService = _hostService;
            logger = _logger;
        }

        /// <summary>
        /// 會員登入
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [AllowAnonymous]
        [HttpPost]
        [Route("api/auth/login")]
        public IActionResult AuthLogin(RequestAuthLoginModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                MysqlSetting mysqlSetting = new MysqlSetting();
                string connectionString = mysqlSetting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查帳號密碼
                    var userInfo = new AuthModels(conn).InspactUserAuthSecret<AuthLoginVM>(body);

                    // 如果帳號不存在
                    if (userInfo == null)
                    {
                        result.isSuccess = false;
                        result.message = "NoSuchUser";
                        logger.LogError("NoSuchUser");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 如果帳號停用
                    if (userInfo.status == 0)
                    {
                        result.isSuccess = false;
                        result.message = "AccountDisabled";
                        logger.LogError("AccountDisabled");
                        return StatusCode((int)HttpStatusCode.Gone, result);
                    }

                    // 使用者 id
                    string userId = userInfo.userAuthorizedId.ToString();

                    // 圖片 url
                    string fileImageUrl = string.Empty;

                    // 檢查檔案是否存在
                    var userImageInfo = new ImageUtils().InspactImageFilePathDoesNotExist(userId);

                    if (userImageInfo != null)
                    {
                        for(int i = 0; i < userImageInfo.Count(); i++)
                        {
                            // 圖片名稱
                            string fileName = Path.GetFileNameWithoutExtension(userImageInfo[0].ToString());

                            // 圖片副檔名
                            string fileExtension = Path.GetExtension(userImageInfo[0].ToString());

                            if (fileName == userInfo.name)
                            {
                                fileImageUrl = $@"{hostService.Domain}{hostService.ImagePath}/{userId}/{fileName}{fileExtension}";
                                break;
                            }
                        }
                    }
                    else
                    {
                        fileImageUrl = "";
                    }

                    Claim[] claim = new[]
                    {
                        new Claim("id", userInfo.userAuthorizedId.ToString()),
                        new Claim("account", userInfo.account),
                        new Claim("name", userInfo.name),
                        new Claim("imageUrl", fileImageUrl),
                        new Claim("_gender", userInfo.gender),
                        new Claim("birthday", userInfo.birthday),
                        new Claim("age", userInfo.age.ToString()),
                        new Claim("_role", userInfo.role),
                        new Claim("identity", userInfo.identity),
                        new Claim(JwtRegisteredClaimNames.Nbf, $"{new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()}"),
                        new Claim(JwtRegisteredClaimNames.Exp, $"{new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds()}")
                    };

                    // 建立 token 模型
                    AuthLoginTokenVM tokenAuthVM = new AuthLoginTokenVM();
                    tokenAuthVM.accessToken = new JwtUtils().GenerateJwtToken(claim);
                    tokenAuthVM.expires_in = Convert.ToInt64(new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds().ToString().PadRight(13, '0'));

                    // 統一資料壓縮回傳
                    result.isSuccess = true;
                    result.data = tokenAuthVM;
                    logger.LogInformation("SendTokenSuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.ToString());
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 會員註冊
        /// </summary>
        /// <param name="body">接收會員註冊模型</param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/auth/register")]
        public IActionResult AuthRegister(RequestAuthRegister body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            string pattern = "[A-Za-z0-9]+@[A-Za-z0-9]+.[A-Za-z0-9]+";
            
            if (!string.IsNullOrWhiteSpace(body.email) && !Regex.IsMatch(body.email, pattern))
            {
                result.isSuccess = false;
                result.message = "EmailBadRequest";
                logger.LogError("EmailBadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查年齡是否有低於 21 歲
                    var userAge = Convert.ToInt32(DateTime.Now.ToString("yyyy")) - Convert.ToInt32(body.birthday.ToString("yyyy"));
                    if (userAge <= 20)
                    {
                        result.isSuccess = false;
                        result.message = "AgeNotSuitable";
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 檢查是否有此帳號註冊
                    var isAccountAlreadyRegistered = new AuthModels(conn).InspactUserAuthAccount<InspactAuthAccountVM>(body.account);

                    // 如果此帳號已註冊過
                    if (isAccountAlreadyRegistered != null)
                    {
                        result.isSuccess = false;
                        result.message = "UserAccountAlreadyRegistered";
                        logger.LogError("UserAccountAlreadyRegistered");
                        return StatusCode((int)HttpStatusCode.Conflict, result);
                    }

                    // 檢查是否有此電話註冊
                    var isPhoneAlreadyRegistered = new AuthModels(conn).InspactUserAuthPhone<InspactAuthAccountVM>(body.phone);

                    // 如果此電話已註冊過
                    if (isPhoneAlreadyRegistered != null)
                    {
                        result.isSuccess = false;
                        result.message = "UserPhoneAlreadyRegistered";
                        logger.LogError("UserPhoneAlreadyRegistered");
                        return StatusCode((int)HttpStatusCode.Conflict, result);
                    }

                    // 檢查是否有傳無效副檔名
                    bool isInvalidFileExtension = new ImageUtils().InspactPictureFileExtension(body.photo);
                    if (!string.IsNullOrEmpty(body.photo))
                    {
                        if (!isInvalidFileExtension)
                        {
                            result.isSuccess = false;
                            result.message = "InvalidFileExtension";
                            return StatusCode((int)HttpStatusCode.RequestEntityTooLarge, result);
                        }
                    }

                    // 檢查檔案大小是否超過 2 MB
                    var isFileTooLarge = new ImageUtils().InspactPictureSize(body.photo);
                    if (!string.IsNullOrWhiteSpace(body.photo))
                    {
                        if (isFileTooLarge)
                        {
                            result.isSuccess = false;
                            result.data = isFileTooLarge;
                            result.message = "FileTooLarge";
                            return StatusCode((int)HttpStatusCode.RequestEntityTooLarge, result);
                        }
                    }

                    bool isQuestionnaireFirstPartAnyYes = body.questionnaireFirstPart.Any(a => a.answer == true);

                    // 如果第一部分任一題為 '是'，但是第二部分沒寫一題
                    if (isQuestionnaireFirstPartAnyYes)
                    {
                        if (body.questionnaireTwoPart.Count() == 0)
                        {
                            result.isSuccess = true;
                            result.message = "UnfilledQuestionnaireTwoPart";
                            logger.LogError("UnfilledQuestionnaireTwoPart");
                            return StatusCode((int)HttpStatusCode.RequestEntityTooLarge, result);
                        }
                    }

                    // 新增會員註冊資料
                    var insertUserAuthRow = new AuthModels(conn).InsertUserAuthorizedTable(recordId, body);

                    // 取得最新註冊表 id
                    var newUserId = new AuthModels(conn).GetNewUserAuthorizedId<NewAuthIdVM>(body.account);

                    // 新增會員生體質量紀錄
                    var insertUserBasicRow = new BodyMassModels(conn).InsertBodyMassTable(newUserId.newUserAuthorizedId, body.account, body.height, body.weight);

                    // 新增問卷第一部分
                    var insertQuestionnaireFirstPartRow = new AuthModels(conn).InsertQuestionnaireFirstPart(newUserId.newUserAuthorizedId, recordId, body);

                    // 新增問卷第二部分
                    var insertQuestionnaireTwoPartRow = new AuthModels(conn).InsertQuestionnaireTwoPart(newUserId.newUserAuthorizedId, recordId, body);

                    // 新增 aha 和 acsm 問卷部分
                    var insert_questionnaire_aha_and_acsm_part_row = new AuthModels(conn).Insert_Questionnaire_AHA_AND_ACSM_PART(newUserId.newUserAuthorizedId, recordId, body);

                    // 新增體適能-運動前測
                    var insertPhysicalFitnessRow = new AuthModels(conn).InsertPhysicalFitness(newUserId.newUserAuthorizedId, recordId, userAge, body);

                    // 確認作業系統與目標儲存路徑
                    string userFolder = $@"{AppDomain.CurrentDomain.BaseDirectory}ServerImage/{newUserId.newUserAuthorizedId}/";

                    // 檢查圖片保存是否成功
                    if (!string.IsNullOrEmpty(body.photo))
                    {
                        var isSaveUserImage = new ImageUtils().Base64StringConvertImage(body.photo, userFolder, body.name);
                        if (isSaveUserImage == null)
                        {
                            transaction.Rollback();
                            result.isSuccess = false;
                            result.message = "ImageSaveFail";
                            return StatusCode((int)HttpStatusCode.RequestEntityTooLarge, result);
                        }
                    }

                    // 生成 qrcode
                    QRCodeUtils qrcode = new QRCodeUtils();
                    qrcode.GenerateQRCodePicture(body.phone);

                    var isSaveUserQRCodeImage = qrcode.Save($@"{userFolder}", $"{body.name}-qrcode.png", ImageFormat.Png);

                    if (isSaveUserQRCodeImage == null)
                    {
                        transaction.Rollback();
                        result.isSuccess = false;
                        result.message = "ImageSaveQRCodeFail";
                        return StatusCode((int)HttpStatusCode.RequestEntityTooLarge, result);
                    }

                    if (insertUserAuthRow != 1 &&
                        insertUserBasicRow != 2 &&
                        insertQuestionnaireFirstPartRow <= 0 &&
                        insertQuestionnaireTwoPartRow <= 0 &&
                        insert_questionnaire_aha_and_acsm_part_row <= 0 &&
                        insertPhysicalFitnessRow <= 0)
                    {
                        transaction.Rollback();
                        result.isSuccess = false;
                        result.message = "ApplyRegisterFail";
                        return StatusCode((int)HttpStatusCode.RequestEntityTooLarge, result);
                    }

                    transaction.Commit();
                    result.isSuccess = true;
                    result.message = "RegisterSuccess";
                    logger.LogError("RegisterSuccess");
                    return StatusCode((int)HttpStatusCode.Created, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
