﻿namespace TrainingPlatformApi.Models
{
    public class ResultModels
    {
        public bool isSuccess { get; set; }
        public string message { get; set;  }
        public object data { get; set; }

        public ResultModels()
        {
            isSuccess = false;
            message = "";
            data = new object();
        }
    }
}
