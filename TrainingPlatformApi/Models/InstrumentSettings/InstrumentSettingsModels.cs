using Dapper;
using MySql.Data.MySqlClient;
using System.ComponentModel.DataAnnotations;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.VM.InstrumentSettings;

namespace TrainingPlatformApi.Models.InstrumentSettings
{
    public class InstrumentSettingsModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public InstrumentSettingsModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查是否有此筆運動表現資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studnetId"></param>
        /// <param name="machinaryCode"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public IEnumerable<T> InspactInstrumentSettingsSportsPerformance<T>(int studnetId, int machinaryCode, RequestSportsPerformanceModels obj)
        {
            sqlStr = $@"SELECT instrumentSettingsId,
                               userAuthorizedId,
                               machinaryCode,
                               level,
                               finish,
                               mistake,
                               maxRange,
                               power,
                               speed,
                               trainingDate,
                               trainingTime
                          FROM instrument_settings
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND machinaryCode    = @machinaryCode
                               AND trainingDate     = @trainingDate
                               AND consciousEffort IS NULL
                               AND maxSpeed        IS NULL
            ";

            var param = new
            {
                userAuthorizedId = studnetId,
                machinaryCode = machinaryCode,
                trainingDate = obj.trainingDate
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 檢查儀器設定最大運動表現備註
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studnetId"></param>
        /// <param name="machinaryCode"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public T InspactInstrumentSettingsSportsPerformanceMark<T>(int studnetId, int machinaryCode, RequestSportsPerformanceModels obj)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                               trainingDate,
                               mark,
                               newRecordId,
                               newRecordDate,
                               newRecordTime,
                               updateRecordId,
                               updateRecordDate,
                               updateRecordTime
                          FROM instrument_settings_mark
                         WHERE userAuthorizedId = @userAuthorizedId
                               AND trainingDate = @trainingDate
            ";

            var param = new
            {
                userAuthorizedId = studnetId,
                machinaryCode = machinaryCode,
                trainingDate = obj.trainingDate
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增儀器設定最大運動表現備註
        /// </summary>
        /// <param name="studnetId"></param>
        /// <param name="recordId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int InsertInstrumentSettingsSportsPerformanceMark(int studnetId, int recordId, RequestSportsPerformanceModels obj)
        {
            sqlStr = $@"INSERT INTO instrument_settings_mark
                        ( userAuthorizedId, type,  trainingDate,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                        (@userAuthorizedId,    1, @trainingDate, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studnetId,
                trainingDate = obj.trainingDate.ToString("yyyyMMdd"),
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新儀器設定最大運動表現備註
        /// </summary>
        /// <param name="studnetId"></param>
        /// <param name="recordId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int UpdateInstrumentSettingsSportsPerformanceMark(int studnetId,int recordId, RequestSportsPerformanceModels obj)
        {
            sqlStr = $@"UPDATE instrument_settings_mark
                           SET mark             = @mark,
                               updateRecordId   = @updateRecordId,
                               updateRecordDate = @updateRecordDate,
                               updateRecordTime = @updateRecordTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND trainingDate     = @trainingDate
            ";
            

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studnetId,
                mark = obj.mark,
                trainingDate = obj.trainingDate.ToString("yyyyMMdd"),
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }
    }

    public class RequestSportsPerformanceModels
    {
        /// <summary>
        /// 備註
        /// </summary>
        [MaxLength(150)]
        public string mark { get; set; } = "";
        
        /// <summary>
        /// 訓練日期
        /// </summary>
        [Required]
        public DateTime trainingDate { get; set; }
    }
}
