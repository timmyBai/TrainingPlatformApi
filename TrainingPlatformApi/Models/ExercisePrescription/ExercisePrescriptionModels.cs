using Dapper;
using MySql.Data.MySqlClient;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;

namespace TrainingPlatformApi.Models.ExercisePrescription
{
    public class ExercisePrescriptionModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public ExercisePrescriptionModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查今日是否有運動處方-訓練階段資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <returns></returns>
        public T InspactTodayExercisePrescriptionTrainingStage<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               exercise_prescription_training_stage.firstStage,
                               exercise_prescription_training_stage.middleStage,
                               exercise_prescription_training_stage.startDate,
                               exercise_prescription_training_stage.newRecordId,
                               exercise_prescription_training_stage.newRecordDate,
                               exercise_prescription_training_stage.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (exercise_prescription_training_stage)
                            ON (user_authorized.userAuthorizedId = exercise_prescription_training_stage.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId                      = @userAuthorizedId
                               AND exercise_prescription_training_stage.userAuthorizedId = @userAuthorizedId
                               AND exercise_prescription_training_stage.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增 運動處方-訓練階段資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 新增或更新運動處方-訓練階段 模型</param>
        /// <returns></returns>
        public int InsertExercisePrescriptionTrainingStage(int recordId, int studentId, RequestExercisePrescriptionTrainingStage obj)
        {
            sqlStr = $@"INSERT INTO exercise_prescription_training_stage
                        ( userAuthorizedId,  firstStage,  middleStage,  startDate,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                        (@userAuthorizedId, @firstStage, @middleStage, @startDate, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                firstStage = obj.firstStage,
                middleStage = obj.middleStage,
                startDate = obj.startDate.ToString("yyyyMMdd"),
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 新增 運動處方-訓練階段資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 新增或更新運動處方-訓練階段 模型</param>
        /// <returns></returns>
        public int UpdateExercisePrescriptionTrainingStage(int recordId, int studentId, string today, RequestExercisePrescriptionTrainingStage obj)
        {
            sqlStr = $@"UPDATE exercise_prescription_training_stage
                           SET firstStage       = @firstStage,
                               middleStage      = @middleStage,
                               startDate        = @startDate,
                               updateRecordId   = @updateRecordId,
                               updateRecordTime = @updateRecordTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND updateRecordDate = @updateRecordDate
            ";

            var nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                firstStage = obj.firstStage,
                middleStage = obj.middleStage,
                startDate = obj.startDate.ToString("yyyyMMdd"),
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 取得學員今日運動處方-心肺適能
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <returns></returns>
        public T InspactTodayExercisePrescriptionCardiorespiratoryFitness<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               exercise_prescription_cardiorespiratory_fitness.sports,
                               exercise_prescription_cardiorespiratory_fitness.exerciseIntensity,
                               exercise_prescription_cardiorespiratory_fitness.exerciseFrequency,
                               exercise_prescription_cardiorespiratory_fitness.trainingUnitMinute,
                               exercise_prescription_cardiorespiratory_fitness.mark,
                               exercise_prescription_cardiorespiratory_fitness.newRecordId,
                               exercise_prescription_cardiorespiratory_fitness.newRecordDate,
                               exercise_prescription_cardiorespiratory_fitness.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (exercise_prescription_cardiorespiratory_fitness)
                            ON (user_authorized.userAuthorizedId = exercise_prescription_cardiorespiratory_fitness.userAuthorizedId) 
                         WHERE     user_authorized.userAuthorizedId                                 = @userAuthorizedId
                               AND exercise_prescription_cardiorespiratory_fitness.userAuthorizedId = @userAuthorizedId
                               AND exercise_prescription_cardiorespiratory_fitness.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增運動處方-心肺適能
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 新增或更新運動處方-心肺適能 模型</param>
        /// <returns></returns>
        public int InsertExercisePrescriptionCardiorespiratoryFitness(int recordId, int studentId, RequestExercisePrescriptionCardiorespiratoryFitness obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO exercise_prescription_cardiorespiratory_fitness
                            ( userAuthorizedId,  sports,  exerciseIntensity,  exerciseFrequency,  trainingUnitMinute,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @sports, @exerciseIntensity, @exerciseFrequency, @trainingUnitMinute, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO exercise_prescription_cardiorespiratory_fitness
                            ( userAuthorizedId,  sports,  exerciseIntensity,  exerciseFrequency,  trainingUnitMinute,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @sports, @exerciseIntensity, @exerciseFrequency, @trainingUnitMinute, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string sports = JsonConvert.SerializeObject(obj.sports);
            string exerciseIntensity = obj.exerciseIntensity;
            string exerciseFrequency = obj.exerciseFrequency;
            string trainingUnitMinute = obj.trainingUnitMinute;
            string mark = obj.mark;
            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sports = sports,
                exerciseIntensity = exerciseIntensity,
                exerciseFrequency = exerciseFrequency,
                trainingUnitMinute = trainingUnitMinute,
                mark = mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 新增運動處方-心肺適能
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 新增或更新運動處方-心肺適能 模型</param>
        /// <returns></returns>
        public int UpdateExercisePrescriptionCardiorespiratoryFitness(int recordId, int studentId, string today, RequestExercisePrescriptionCardiorespiratoryFitness obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE exercise_prescription_cardiorespiratory_fitness
                               SET sports             = @sports,
                                   exerciseIntensity  = @exerciseIntensity,
                                   exerciseFrequency  = @exerciseFrequency,
                                   trainingUnitMinute = @trainingUnitMinute,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE exercise_prescription_cardiorespiratory_fitness
                               SET sports             = @sports,
                                   exerciseIntensity  = @exerciseIntensity,
                                   exerciseFrequency  = @exerciseFrequency,
                                   trainingUnitMinute = @trainingUnitMinute,
                                   mark               = @mark,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string sports = JsonConvert.SerializeObject(obj.sports);
            string exerciseIntensity = obj.exerciseIntensity;
            string exerciseFrequency = obj.exerciseFrequency;
            string trainingUnitMinute = obj.trainingUnitMinute;
            string mark = obj.mark;
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sports = sports,
                exerciseIntensity = exerciseIntensity,
                exerciseFrequency = exerciseFrequency,
                trainingUnitMinute = trainingUnitMinute,
                mark = mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 取得學員今日運動處方-肌力與肌耐力
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <returns></returns>
        public T InspactTodayExercisePrescriptionMuscleStrengthAndMuscleEndurance<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               exercise_prescription_muscle_strength_and_muscle_endurance.exerciseIntensity,
                               exercise_prescription_muscle_strength_and_muscle_endurance.exerciseFrequency,
                               exercise_prescription_muscle_strength_and_muscle_endurance.trainingUnitGroup,
                               exercise_prescription_muscle_strength_and_muscle_endurance.trainingUnitNumber,
                               exercise_prescription_muscle_strength_and_muscle_endurance.trainingArea,
                               exercise_prescription_muscle_strength_and_muscle_endurance.mark,
                               exercise_prescription_muscle_strength_and_muscle_endurance.newRecordId,
                               exercise_prescription_muscle_strength_and_muscle_endurance.newRecordDate,
                               exercise_prescription_muscle_strength_and_muscle_endurance.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (exercise_prescription_muscle_strength_and_muscle_endurance)
                            ON (user_authorized.userAuthorizedId = exercise_prescription_muscle_strength_and_muscle_endurance.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId                                            = @userAuthorizedId
                               AND exercise_prescription_muscle_strength_and_muscle_endurance.userAuthorizedId = @userAuthorizedId
                               AND exercise_prescription_muscle_strength_and_muscle_endurance.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增運動處方-肌力與肌耐力
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 新增或更新運動處方-肌力與肌耐力 模型</param>
        /// <returns></returns>
        public int InsertExercisePrescriptionMuscleStrengthAndMuscleEndurance(int recordId, int studentId, RequestExercisePrescriptionMuscleStrengthAndMuscleEndurance obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO exercise_prescription_muscle_strength_and_muscle_endurance
                            ( userAuthorizedId,  exerciseIntensity,  exerciseFrequency,  trainingUnitGroup,  trainingUnitNumber,  trainingArea,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @exerciseIntensity, @exerciseFrequency, @trainingUnitGroup, @trainingUnitNumber, @trainingArea, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO exercise_prescription_muscle_strength_and_muscle_endurance
                            ( userAuthorizedId,  exerciseIntensity,  exerciseFrequency,  trainingUnitGroup,  trainingUnitNumber,  trainingArea,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @exerciseIntensity, @exerciseFrequency, @trainingUnitGroup, @trainingUnitNumber, @trainingArea, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string exerciseIntensity = obj.exerciseIntensity;
            string exerciseFrequency = obj.exerciseFrequency;
            string trainingUnitGroup = obj.trainingUnitGroup;
            string trainingUnitNumber = obj.trainingUnitNumber;
            string trainingArea = JsonConvert.SerializeObject(obj.trainingArea);
            string mark = obj.mark;
            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                exerciseIntensity = exerciseIntensity,
                exerciseFrequency = exerciseFrequency,
                trainingUnitGroup = trainingUnitGroup,
                trainingUnitNumber = trainingUnitNumber,
                trainingArea = trainingArea,
                mark = mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新運動處方-肌力與肌耐力
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <param name="obj">接收 新增或更新運動處方-肌力與肌耐力 模型</param>
        /// <returns></returns>
        public int UpdateExercisePrescriptionMuscleStrengthAndMuscleEndurance(int recordId, int studentId, string today, RequestExercisePrescriptionMuscleStrengthAndMuscleEndurance obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE exercise_prescription_muscle_strength_and_muscle_endurance
                               SET exerciseIntensity  = @exerciseIntensity,
                                   exerciseFrequency  = @exerciseFrequency,
                                   trainingUnitGroup  = @trainingUnitGroup,
                                   trainingUnitNumber = @trainingUnitNumber,
                                   trainingArea       = @trainingArea,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE exercise_prescription_muscle_strength_and_muscle_endurance
                               SET exerciseIntensity  = @exerciseIntensity,
                                   exerciseFrequency  = @exerciseFrequency,
                                   trainingUnitGroup  = @trainingUnitGroup,
                                   trainingUnitNumber = @trainingUnitNumber,
                                   trainingArea       = @trainingArea,
                                   mark               = @mark,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string exerciseIntensity = obj.exerciseIntensity;
            string exerciseFrequency = obj.exerciseFrequency;
            string trainingUnitGroup = obj.trainingUnitGroup;
            string trainingUnitNumber = obj.trainingUnitNumber;
            string trainingArea = JsonConvert.SerializeObject(obj.trainingArea);
            string mark = obj.mark;
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                exerciseIntensity = exerciseIntensity,
                exerciseFrequency = exerciseFrequency,
                trainingUnitGroup = trainingUnitGroup,
                trainingUnitNumber = trainingUnitNumber,
                trainingArea = trainingArea,
                mark = mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 取得學員今日運動處方-柔軟度
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id<</param>
        /// <param name="today">今日日期</param>
        /// <returns></returns>
        public T InspactTodayExercisePrescriptionSoftness<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               exercise_prescription_softness.sports,
                               exercise_prescription_softness.exerciseIntensity,
                               exercise_prescription_softness.exerciseFrequency,
                               exercise_prescription_softness.trainingUnitMinute,
                               exercise_prescription_softness.mark,
                               exercise_prescription_softness.newRecordId,
                               exercise_prescription_softness.newRecordDate,
                               exercise_prescription_softness.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (exercise_prescription_softness)
                            ON (user_authorized.userAuthorizedId = exercise_prescription_softness.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId                = @userAuthorizedId
                               AND exercise_prescription_softness.userAuthorizedId = @userAuthorizedId
                               AND exercise_prescription_softness.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增運動處方-柔軟度
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 新增或更新運動處方-柔軟度 模型</param>
        /// <returns></returns>
        public int InsertExercisePrescriptionSoftness(int recordId, int studentId, RequestExercisePrescriptionSoftness obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO exercise_prescription_softness
                            ( userAuthorizedId,  sports,  exerciseIntensity,  exerciseFrequency,  trainingUnitMinute,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @sports, @exerciseIntensity, @exerciseFrequency, @trainingUnitMinute, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO exercise_prescription_softness
                            ( userAuthorizedId,  sports,  exerciseIntensity,  exerciseFrequency,  trainingUnitMinute,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @sports, @exerciseIntensity, @exerciseFrequency, @trainingUnitMinute, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string sports = JsonConvert.SerializeObject(obj.sports);
            string exerciseIntensity = obj.exerciseIntensity;
            string exerciseFrequency = obj.exerciseFrequency;
            string trainingUnitMinute = obj.trainingUnitMinute;
            string mark = obj.mark;
            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sports = sports,
                exerciseIntensity = exerciseIntensity,
                exerciseFrequency = exerciseFrequency,
                trainingUnitMinute = trainingUnitMinute,
                mark = mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新運動處方-柔軟度
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <param name="obj">接收 新增或更新運動處方-柔軟度 模型</param>
        /// <returns></returns>
        public int UpdateExercisePrescriptionSoftness(int recordId, int studentId, string today, RequestExercisePrescriptionSoftness obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE exercise_prescription_softness
                               SET sports             = @sports,
                                   exerciseIntensity  = @exerciseIntensity,
                                   exerciseFrequency  = @exerciseFrequency,
                                   trainingUnitMinute = @trainingUnitMinute,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE exercise_prescription_softness
                               SET sports             = @sports,
                                   exerciseIntensity  = @exerciseIntensity,
                                   exerciseFrequency  = @exerciseFrequency,
                                   trainingUnitMinute = @trainingUnitMinute,
                                   mark               = @mark,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string sports = JsonConvert.SerializeObject(obj.sports);
            string exerciseIntensity = obj.exerciseIntensity;
            string exerciseFrequency = obj.exerciseFrequency;
            string trainingUnitMinute = obj.trainingUnitMinute;
            string mark = obj.mark;
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sports = sports,
                exerciseIntensity = exerciseIntensity,
                exerciseFrequency = exerciseFrequency,
                trainingUnitMinute = trainingUnitMinute,
                mark = mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 取得學員今日運動處方-柔軟度
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="today"></param>
        /// <returns></returns>
        public T InspactTodayExercisePrescriptioBalance<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               exercise_prescription_balance.sports,
                               exercise_prescription_balance.exerciseIntensity,
                               exercise_prescription_balance.exerciseFrequency,
                               exercise_prescription_balance.trainingUnitMinute,
                               exercise_prescription_balance.mark,
                               exercise_prescription_balance.newRecordId,
                               exercise_prescription_balance.newRecordDate,
                               exercise_prescription_balance.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (exercise_prescription_balance)
                            ON (user_authorized.userAuthorizedId = exercise_prescription_balance.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId               = @userAuthorizedId
                               AND exercise_prescription_balance.userAuthorizedId = @userAuthorizedId
                               AND exercise_prescription_balance.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增運動處方-平衡
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 新增或更新運動處方-平衡 模型</param>
        /// <returns></returns>
        public int InsertExercisePrescriptioBalance(int recordId, int studentId, RequestExercisePrescriptionBalance obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO exercise_prescription_balance
                            ( userAuthorizedId,  sports,  exerciseIntensity,  exerciseFrequency,  trainingUnitMinute,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @sports, @exerciseIntensity, @exerciseFrequency, @trainingUnitMinute, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO exercise_prescription_balance
                            ( userAuthorizedId,  sports,  exerciseIntensity,  exerciseFrequency,  trainingUnitMinute,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @sports, @exerciseIntensity, @exerciseFrequency, @trainingUnitMinute, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string sports = JsonConvert.SerializeObject(obj.sports);
            string exerciseIntensity = obj.exerciseIntensity;
            string exerciseFrequency = obj.exerciseFrequency;
            string trainingUnitMinute = obj.trainingUnitMinute;
            string mark = obj.mark;
            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sports = sports,
                exerciseIntensity = exerciseIntensity,
                exerciseFrequency = exerciseFrequency,
                trainingUnitMinute = trainingUnitMinute,
                mark = mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新運動處方-平衡
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 新增或更新運動處方-平衡 模型</param>
        /// <returns></returns>
        public int UpdateExercisePrescriptioBalance(int recordId, int studentId, string today, RequestExercisePrescriptionBalance obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE exercise_prescription_balance
                               SET sports             = @sports,
                                   exerciseIntensity  = @exerciseIntensity,
                                   exerciseFrequency  = @exerciseFrequency,
                                   trainingUnitMinute = @trainingUnitMinute,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE exercise_prescription_balance
                               SET sports             = @sports,
                                   exerciseIntensity  = @exerciseIntensity,
                                   exerciseFrequency  = @exerciseFrequency,
                                   trainingUnitMinute = @trainingUnitMinute,
                                   mark               = @mark,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string sports = JsonConvert.SerializeObject(obj.sports);
            string exerciseIntensity = obj.exerciseIntensity;
            string exerciseFrequency = obj.exerciseFrequency;
            string trainingUnitMinute = obj.trainingUnitMinute;
            string mark = obj.mark;
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sports = sports,
                exerciseIntensity = exerciseIntensity,
                exerciseFrequency = exerciseFrequency,
                trainingUnitMinute = trainingUnitMinute,
                mark = mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }
    }

    /// <summary>
    /// 接收 新增或更新運動處方-訓練階段 模型
    /// </summary>
    public class RequestExercisePrescriptionTrainingStage
    {
        /// <summary>
        /// 開始階段
        /// </summary>
        [Required]
        [RegularExpression("^([1-9]|[1-9]+[0-9]+)$")]
        public int firstStage { get; set; } = 0;

        /// <summary>
        /// 改善階段
        /// </summary>
        [Required]
        [RegularExpression("^([1-9]|[1-9]+[0-9]+)$")]
        public int middleStage { get; set; } = 0;

        /// <summary>
        /// 設定起始日期
        /// </summary>
        [Required]
        public DateTime startDate { get; set; } = new DateTime();
    }

    /// <summary>
    /// 接收 新增或更新運動處方-心肺適能 模型
    /// </summary>
    public class RequestExercisePrescriptionCardiorespiratoryFitness
    {
        /// <summary>
        /// 訓練強度
        /// </summary>
        [Range(1, 3)]
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 運動頻率(天/周)
        /// </summary>
        [Range(1, 7)]
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        [RegularExpression("^(5|10|15|20|25|30|35|40|45|50)$")]
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        [MaxLengthAttribute(8)]
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 備註
        /// </summary>
        [MaxLength(150)]
        public string mark { get; set; } = "null";
    }

    /// <summary>
    /// 接收 新增或更新運動處方-肌力與肌耐力 模型
    /// </summary>
    public class RequestExercisePrescriptionMuscleStrengthAndMuscleEndurance
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        [Range(1, 3)]
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/周)
        /// </summary>
        [Range(1, 7)]
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(組/台)
        /// </summary>
        [RegularExpression("^(2|4|6|8|10)$")]
        public string trainingUnitGroup { get; set; } = "";

        /// <summary>
        /// 訓練單位(次/組)
        /// </summary>
        [RegularExpression("^(4|6|8|10|12|14|16|18)$")]
        public string trainingUnitNumber { get; set; } = "";

        /// <summary>
        /// 加強訓練部位
        /// </summary>
        [MaxLengthAttribute(4)]
        public List<string> trainingArea { get; set; } = new List<string>();

        /// <summary>
        /// 備註
        /// </summary>
        [MaxLength(150)]
        public string mark { get; set; } = "null";
    }

    /// <summary>
    /// 接收 新增或更新運動處方-柔軟度 模型
    /// </summary>
    public class RequestExercisePrescriptionSoftness
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        [Range(1, 3)]
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 運練頻率(天/周)
        /// </summary>
        [Range(1, 7)]
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        [RegularExpression("^(5|10|15|20|25|30)$")]
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        [MaxLengthAttribute(2)]
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 備註
        /// </summary>
        [MaxLength(150)]
        public string mark { get; set; } = "null";
    }

    /// <summary>
    /// 接收 新增或更新運動處方-平衡 模型
    /// </summary>
    public class RequestExercisePrescriptionBalance
    {
        /// <summary>
        /// 運動強度
        /// </summary>
        [Range(1, 3)]
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 訓練頻率(單位: 天/周)
        /// </summary>
        [Range(1, 7)]
        public string exerciseFrequency { get; set; } = "";

        /// <summary>
        /// 訓練單位(分鐘/次)
        /// </summary>
        [RegularExpression("^(5|10|15|20|25|30)$")]
        public string trainingUnitMinute { get; set; } = "";

        /// <summary>
        /// 運動項目
        /// </summary>
        [MaxLengthAttribute(3)]
        public List<string> sports { get; set; } = new List<string>();

        /// <summary>
        /// 備註
        /// </summary>
        [MaxLength(150)]
        public string mark { get; set; } = "null";
    }
}
