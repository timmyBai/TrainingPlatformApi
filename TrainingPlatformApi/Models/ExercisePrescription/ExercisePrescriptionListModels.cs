using Dapper;
using MySql.Data.MySqlClient;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;

namespace TrainingPlatformApi.Models.ExercisePrescription
{
    public class ExercisePrescriptionListModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public ExercisePrescriptionListModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 取得運動處方-心肺適能歷史紀錄
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public IEnumerable<T> GetExercisePrescriptionCardiorespiratoryFitnessHistory<T>(int studentId)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SELECT user_authorized.userAuthorizedId,
                                CASE exercise_prescription_cardiorespiratory_fitness.exerciseIntensity
                                     WHEN '1' THEN '低強度運動'
	                                 WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exercise_prescription_cardiorespiratory_fitness.exerciseIntensity
                                     WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
	                                 WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
                                 END AS heartRate,
                                exercise_prescription_cardiorespiratory_fitness.sports,
                                CONCAT('每周 ', exercise_prescription_cardiorespiratory_fitness.exerciseFrequency, ' 天，', exercise_prescription_cardiorespiratory_fitness.trainingUnitMinute, ' 分鐘/次') AS trainingFrequency,
                                exercise_prescription_cardiorespiratory_fitness.trainingUnitMinute,
                                exercise_prescription_cardiorespiratory_fitness.mark,
                                exercise_prescription_cardiorespiratory_fitness.updateRecordId,
                                CONCAT(SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordDate, 1, 4), '-', SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordDate, 5, 2), '-', SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordDate, 7, 2)) AS updateRecordDate,
                                CONCAT(SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordTime, 1, 2), ':', SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordTime, 3, 2), ':', SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordTime, 5, 2)) AS updateRecordTime
                           FROM user_authorized
                           LEFT JOIN (exercise_prescription_cardiorespiratory_fitness)
                             ON (user_authorized.userAuthorizedId = exercise_prescription_cardiorespiratory_fitness.userAuthorizedId)
                          WHERE     user_authorized.userAuthorizedId                                 = @userAuthorizedId
                                AND exercise_prescription_cardiorespiratory_fitness.userAuthorizedId = @userAuthorizedId
                          ORDER BY exercise_prescription_cardiorespiratory_fitness.updateRecordDate DESC,
                                   exercise_prescription_cardiorespiratory_fitness.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).ToList();

            return result;
        }

        /// <summary>
        /// 取得運動處方-肌力與肌耐力歷史紀錄
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public IEnumerable<T> GetExercisePrescriptionMuscleStrengthAndMuscleEnduranceHistory<T>(int studentId)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SELECT user_authorized.userAuthorizedId,
                                CASE exercise_prescription_muscle_strength_and_muscle_endurance.exerciseIntensity
                                     WHEN '1' THEN '低強度運動'
	                                 WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exercise_prescription_muscle_strength_and_muscle_endurance.exerciseIntensity
                                     WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
	                                 WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
                                 END AS heartRate,
                                CONCAT('每周 ', exercise_prescription_muscle_strength_and_muscle_endurance.exerciseFrequency, ' 天， ', exercise_prescription_muscle_strength_and_muscle_endurance.trainingUnitGroup, ' 台/組， ', exercise_prescription_muscle_strength_and_muscle_endurance.trainingUnitNumber, ' 次/組') AS trainingFrequency,
                                exercise_prescription_muscle_strength_and_muscle_endurance.trainingArea,
                                exercise_prescription_muscle_strength_and_muscle_endurance.mark,
                                exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordId,
                                CONCAT(SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate, 1, 4), '-', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate, 5, 2), '-', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate, 7, 2)) AS updateRecordDate,
                                CONCAT(SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime, 1, 2), ':', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime, 3, 2), ':', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime, 5, 2)) AS updateRecordTime
                           FROM user_authorized
                           LEFT JOIN (exercise_prescription_muscle_strength_and_muscle_endurance)
                             ON (user_authorized.userAuthorizedId = exercise_prescription_muscle_strength_and_muscle_endurance.userAuthorizedId)
                          WHERE     user_authorized.userAuthorizedId                                            = @userAuthorizedId
                                AND exercise_prescription_muscle_strength_and_muscle_endurance.userAuthorizedId = @userAuthorizedId
                          ORDER BY exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate DESC,
                                   exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).ToList();

            return result;
        }

        /// <summary>
        /// 取得運動處方-柔軟度歷史紀錄
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public IEnumerable<T> GetExercisePrescriptionSoftnessHistory<T>(int studentId)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SELECT user_authorized.userAuthorizedId,
                                CASE exercise_prescription_softness.exerciseIntensity
                                     WHEN '1' THEN '低強度運動'
	                                 WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exercise_prescription_softness.exerciseIntensity
                                     WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
	                                 WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
                                 END AS heartRate,
                                exercise_prescription_softness.sports,
                                CONCAT('每周 ', exercise_prescription_softness.exerciseFrequency, ' 天， ', exercise_prescription_softness.trainingUnitMinute, ' 次/組') AS trainingFrequency,
                                exercise_prescription_softness.trainingUnitMinute,
                                exercise_prescription_softness.mark,
                                exercise_prescription_softness.updateRecordId,
                                CONCAT(SUBSTRING(exercise_prescription_softness.updateRecordDate, 1, 4), '-', SUBSTRING(exercise_prescription_softness.updateRecordDate, 5, 2), '-', SUBSTRING(exercise_prescription_softness.updateRecordDate, 7, 2)) AS updateRecordDate,
                                CONCAT(SUBSTRING(exercise_prescription_softness.updateRecordTime, 1, 2), ':', SUBSTRING(exercise_prescription_softness.updateRecordTime, 3, 2), ':', SUBSTRING(exercise_prescription_softness.updateRecordTime, 5, 2)) AS updateRecordTime
                           FROM user_authorized
                           LEFT JOIN (exercise_prescription_softness)
                             ON (user_authorized.userAuthorizedId = exercise_prescription_softness.userAuthorizedId)
                          WHERE     user_authorized.userAuthorizedId                = @userAuthorizedId
                                AND exercise_prescription_softness.userAuthorizedId = @userAuthorizedId
                          ORDER BY exercise_prescription_softness.updateRecordDate DESC,
                                   exercise_prescription_softness.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).ToList();

            return result;
        }

        /// <summary>
        /// 取得運動處方-平衡歷史紀錄
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public IEnumerable<T> GetExercisePrescriptionBalanceHistory<T>(int studentId)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SELECT user_authorized.userAuthorizedId,
                                CASE exercise_prescription_balance.exerciseIntensity
                                     WHEN '1' THEN '低強度運動'
	                                 WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exercise_prescription_balance.exerciseIntensity
                                     WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
	                                 WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
                                 END AS heartRate,
                                exercise_prescription_balance.sports,
                                CONCAT('每周 ', exercise_prescription_balance.exerciseFrequency, ' 天， ', exercise_prescription_balance.trainingUnitMinute, ' 次/組') AS trainingFrequency,
                                exercise_prescription_balance.trainingUnitMinute,
                                exercise_prescription_balance.mark,
                                exercise_prescription_balance.updateRecordId,
                                CONCAT(SUBSTRING(exercise_prescription_balance.updateRecordDate, 1, 4), '-', SUBSTRING(exercise_prescription_balance.updateRecordDate, 5, 2), '-', SUBSTRING(exercise_prescription_balance.updateRecordDate, 7, 2)) AS updateRecordDate,
                                CONCAT(SUBSTRING(exercise_prescription_balance.updateRecordTime, 1, 2), ':', SUBSTRING(exercise_prescription_balance.updateRecordTime, 3, 2), ':', SUBSTRING(exercise_prescription_balance.updateRecordTime, 5, 2)) AS updateRecordTime
                           FROM user_authorized
                           LEFT JOIN (exercise_prescription_balance)
                             ON (user_authorized.userAuthorizedId = exercise_prescription_balance.userAuthorizedId)
                          WHERE     user_authorized.userAuthorizedId               = @userAuthorizedId
                                AND exercise_prescription_balance.userAuthorizedId = @userAuthorizedId
                          ORDER BY exercise_prescription_balance.updateRecordDate DESC,
                                   exercise_prescription_balance.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).ToList();

            return result;
        }

        /// <summary>
        /// 取得運動處方-訓練階段
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionTrainingStage<T>(int studentId)
        {
            sqlStr = $@"SET @today = REPLACE(CURRENT_DATE(), '-', '');

                        SET @datediff = (SELECT round(datediff(@today, startDate) / 7) FROM exercise_prescription_training_stage WHERE userAuthorizedId = @userAuthorizedId ORDER BY updateRecordDate DESC, updateRecordTime DESC LIMIT 1);

                        SET @result = IF(@datediff = 0, '1', @datediff);
                        
                        SELECT user_authorized.userAuthorizedId,
                               exercise_prescription_training_stage.firstStage,
                               exercise_prescription_training_stage.middleStage,
                               CONCAT('目前階段(第 ', @result, ' 週)') AS weekText,
                               @result AS week,
                               CONCAT(SUBSTRING(exercise_prescription_training_stage.startDate, 1, 4), '-', SUBSTRING(exercise_prescription_training_stage.startDate, 5, 2), '-', SUBSTRING(exercise_prescription_training_stage.startDate, 7, 2)) AS startDate,
                               exercise_prescription_training_stage.updateRecordId,
                               exercise_prescription_training_stage.updateRecordDate,
                               exercise_prescription_training_stage.updateRecordTime
                          FROM user_authorized
                          LEFT JOIN (exercise_prescription_training_stage)
                            ON (user_authorized.userAuthorizedId = exercise_prescription_training_stage.userAuthorizedId)
                          WHERE     user_authorized.userAuthorizedId                      = @userAuthorizedId
                                AND exercise_prescription_training_stage.userAuthorizedId = @userAuthorizedId
                          ORDER BY exercise_prescription_training_stage.updateRecordDate DESC,
                                   exercise_prescription_training_stage.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }
    }
}
