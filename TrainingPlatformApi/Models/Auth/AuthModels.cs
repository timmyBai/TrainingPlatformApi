using MySql.Data.MySqlClient;
using Dapper;
using System.ComponentModel.DataAnnotations;

using TrainingPlatformApi.Utils;
using TrainingPlatformApi.Settings;

namespace TrainingPlatformApi.Models.Auth
{
    public class AuthModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public AuthModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查是否有業主、物理治療師、復健師、體適能指導員帳號
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public T InspactUserAuthSecret<T>(RequestAuthLoginModels obj)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                               account,
                               name,
                               gender,
                               birthday,
                               CASE identity
		                            WHEN 'A' THEN '業主'
                                    WHEN 'B' THEN '物理治療師'
                                    WHEN 'C' THEN '復健師'
                                    WHEN 'D' THEN '體適能指導員'
                                END AS role,
                               identity,
                               YEAR(DATE_SUB(NOW(), INTERVAL 0 YEAR)) - SubString(birthday, 1, 4) AS age,
                               status
                     FROM user_authorized
                    WHERE     account   = @account
                          AND password  = @password
                          AND status   != '0'
                          AND identity IN ('A', 'B', 'C', 'D')
            ";

            string account = obj.account;
            string password = new EncryptionUtils().SHA256Encryption(obj.password);

            var param = new
            {
                account = account,
                password = password
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 檢查 此帳號是否有註冊
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="account">帳號</param>
        /// <returns></returns>
        public T InspactUserAuthAccount<T>(string account)
        {
            sqlStr = @"SELECT account FROM user_authorized WHERE account = @account";

            var param = new
            {
                account = account
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 檢查 是否有電話號碼註冊過
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="phone"></param>
        /// <returns></returns>
        public T InspactUserAuthPhone<T>(string phone)
        {
            sqlStr = @"SELECT phone FROM user_authorized WHERE phone = @phone";

            var param = new
            {
                phone = phone
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得註冊表最新 id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetNewUserAuthorizedId<T>(string account)
        {
            sqlStr = @"SELECT userAuthorizedId AS newUserAuthorizedId
                         FROM user_authorized
                        WHERE account = @account
            ";

            var param = new
            {
                account = account
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增 會員註冊資料
        /// </summary>
        /// <param name="obj">接收 會員註冊模型</param>
        /// <returns></returns>
        public int InsertUserAuthorizedTable(int recordId, RequestAuthRegister obj)
        {
            sqlStr = $@"INSERT INTO user_authorized ( phone,  account,  password,  name,  birthday,   email,  address,  gender, status,  identity,  fileextension,  registerdate,  registertime,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                    (@phone, @account, @password, @name, @birthday,  @email, @address, @gender, '1',     'e',      @fileextension, @registerdate, @registertime, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            int fileTypeIndex = new ImageUtils().GetPictureFileExtensionIndex(obj.photo);
            string fileExtension = "";
            string phone = obj.phone;
            string account = obj.account;
            string password = new EncryptionUtils().SHA256Encryption(obj.password);
            string name = obj.name;
            string gender = obj.gender;
            string birthday = obj.birthday.ToString("yyyyMMdd");
            string email = obj.email;
            string address = obj.address;
            string registerDate = new DayUtils().GetFullDate();
            string registerTime = new DayUtils().GetFullTime();

            switch (fileTypeIndex)
            {
                case 1:
                    fileExtension = "PNG";
                    break;
                case 2:
                    fileExtension = "GIF";
                    break;
                case 3:
                    fileExtension = "jpeg";
                    break;
                case 4:
                    fileExtension = "jpg";
                    break;
                default:
                    fileExtension = "";
                    break;
            }

            var param = new
            {
                phone = phone,
                account = account,
                password = password,
                name = name,
                gender = gender,
                birthday = birthday,
                email = email,
                address = address,
                fileExtension = fileExtension,
                registerDate = registerDate,
                registerTime = registerTime,
                newRecordId = recordId,
                newRecordDate = registerDate,
                newRecordTime = registerTime,
                updateRecordId = recordId,
                updateRecordDate = registerDate,
                updateRecordTime = registerTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 新增問卷第一部分
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="recordId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int InsertQuestionnaireFirstPart(int studentId, int recordId, RequestAuthRegister obj)
        {
            sqlStr = @"INSERT INTO questionnaire_physical_activity ( userAuthorizedId,  part,  questionNumber,  answer,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                   (@userAuthorizedId,  1,    @questionNumber, @answer, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            List<object> param = new List<object>();

            obj.questionnaireFirstPart.ForEach((item) =>
            {
                param.Add(new
                {
                    userAuthorizedId = studentId,
                    questionNumber = item.questionNumber,
                    answer = item.answer ? "Y": "N",
                    newRecordId = recordId,
                    newRecordDate = nowDate,
                    newRecordTime = nowTime,
                    updateRecordId = recordId,
                    updateRecordDate = nowDate,
                    updateRecordTime = nowTime
                });
            });

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }
        
        /// <summary>
        /// 新增第二部分問卷
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="recordId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int InsertQuestionnaireTwoPart(int studentId, int recordId, RequestAuthRegister obj)
        {
            sqlStr = @"INSERT INTO questionnaire_physical_activity ( userAuthorizedId,  part,  questionNumber,  subject,  answer,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                   (@userAuthorizedId,  2,    @questionNumber, @subject, @answer, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            List<object> param = new List<object>();

            obj.questionnaireTwoPart.ForEach((item) =>
            {
                param.Add(new
                {
                    userAuthorizedId = studentId,
                    questionNumber = item.questionNumber,
                    subject = item.subject,
                    answer = item.answer ? "Y": "N",
                    newRecordId = recordId,
                    newRecordDate = nowDate,
                    newRecordTime = nowTime,
                    updateRecordId = recordId,
                    updateRecordDate = nowDate,
                    updateRecordTime = nowTime
                });
            });

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 新增 AHA 和 ACSM 問卷資料
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="recordId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int Insert_Questionnaire_AHA_AND_ACSM_PART(int studentId, int recordId, RequestAuthRegister obj)
        {
            sqlStr = @"INSERT INTO aha_and_acsm_questionnaire ( userAuthorizedId,  part,  subject,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                              (@userAuthorizedId, @part, @subject, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            List<object> param = new List<object>();

            obj.questionnaire_aha_and_acsm_part.ForEach((item) =>
            {
                param.Add(new
                {
                    userAuthorizedId = studentId,
                    part = item.part,
                    subject = item.subject,
                    newRecordId = recordId,
                    newRecordDate = nowDate,
                    newRecordTime = nowTime,
                    updateRecordId = recordId,
                    updateRecordDate = nowDate,
                    updateRecordTime = nowTime
                });
            });

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 新增體適能-運動前測
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="recordId"></param>
        /// <param name="age"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int InsertPhysicalFitness(int studentId, int recordId, int age, RequestAuthRegister obj)
        {
            if (age >= 21 && age <= 65)
            {
                sqlStr = @"INSERT INTO physical_fitness_cardiorespiratory_fitness ( userAuthorizedId,  pulseRateOne,  pulseRateTwo,  pulseRateThree,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                  (@userAuthorizedId, @pulseRateOne, @pulseRateTwo, @pulseRateThree, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime);

                           INSERT INTO physical_fitness_muscle_strength_and_muscle_endurance ( userAuthorizedId,  kneeCrunchesFrequency,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                             (@userAuthorizedId, @kneeCrunchesFrequency, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime);
                           
                           INSERT INTO physical_fitness_softness ( userAuthorizedId,  seatedForwardBendOne,  seatedForwardBendTwo,  seatedForwardBendThree,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                 (@userAuthorizedId, @seatedForwardBendOne, @seatedForwardBendTwo, @seatedForwardBendThree, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime);

                           INSERT INTO physical_fitness_balance ( userAuthorizedId,  adultEyeOpeningMonopodSecond,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                (@userAuthorizedId, @adultEyeOpeningMonopodSecond, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime);
                ";
            }
            else
            {
                sqlStr = @"INSERT INTO physical_fitness_cardiorespiratory_fitness ( userAuthorizedId,  kneeLiftFrequency,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                  (@userAuthorizedId, @kneeLiftFrequency, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime);

                           INSERT INTO physical_fitness_muscle_strength_and_muscle_endurance ( userAuthorizedId,  armCurlUpperBodyFrequency,  armCurlLowerBodyFrequency,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                             (@userAuthorizedId, @armCurlUpperBodyFrequency, @armCurlLowerBodyFrequency, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime);

                           INSERT INTO physical_fitness_softness ( userAuthorizedId,  chairSeatedForwardBendOne,  chairSeatedForwardBendTwo,  chairSeatedForwardBendThree,  pullBackTestOne,  pullBackTestTwo,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                 (@userAuthorizedId, @chairSeatedForwardBendOne, @chairSeatedForwardBendTwo, @chairSeatedForwardBendThree, @pullBackTestOne, @pullBackTestTwo, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime);
                           
                           INSERT INTO physical_fitness_balance ( userAuthorizedId,  elderlyEyeOpeningMonopodSecond,  sittingAroundOneSecond,  sittingAroundTwoSecond,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                (@userAuthorizedId, @elderlyEyeOpeningMonopodSecond, @sittingAroundOneSecond, @sittingAroundTwoSecond, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                pulseRateOne = obj.pulseRateOne,
                pulseRateTwo = obj.pulseRateTwo,
                pulseRateThree = obj.pulseRateThree,
                kneeCrunchesFrequency = obj.kneeCrunchesFrequency,
                seatedForwardBendOne = obj.seatedForwardBendOne,
                seatedForwardBendTwo = obj.seatedForwardBendTwo,
                seatedForwardBendThree = obj.seatedForwardBendThree,
                adultEyeOpeningMonopodSecond = obj.adultEyeOpeningMonopodSecond,

                kneeLiftFrequency = obj.kneeLiftFrequency,
                armCurlUpperBodyFrequency = obj.armCurlUpperBodyFrequency,
                armCurlLowerBodyFrequency = obj.armCurlLowerBodyFrequency,
                chairSeatedForwardBendOne = obj.chairSeatedForwardBendOne,
                chairSeatedForwardBendTwo = obj.chairSeatedForwardBendTwo,
                chairSeatedForwardBendThree = obj.chairSeatedForwardBendThree,
                pullBackTestOne = obj.pullBackTestOne,
                pullBackTestTwo = obj.pullBackTestTwo,
                elderlyEyeOpeningMonopodSecond = obj.elderlyEyeOpeningMonopodSecond,
                sittingAroundOneSecond = obj.sittingAroundOneSecond,
                sittingAroundTwoSecond = obj.sittingAroundTwoSecond,

                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }
    }

    /// <summary>
    /// 接收 帳號密碼登入模型
    /// </summary>
    public class RequestAuthLoginModels
    {
        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        public string account { get; set; } = "";

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        public string password { get; set; } = "";
    }

    /// <summary>
    /// 接收 會員註冊 模型
    /// </summary>
    public class RequestAuthRegister
    {
        /// <summary>
        /// base64 字串圖片
        /// </summary>
        [RegularExpression("^(data:image\\/)(png|jpg|jpeg|gif);(base64),(.*)")]
        public string photo { get; set; } = "";

        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string account { get; set; } = "";

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string password { get; set; } = "";

        /// <summary>
        /// 會員名子
        /// </summary>
        [Required]
        [MaxLength(10)]
        public string name { get; set; } = "";

        /// <summary>
        /// 性別
        /// </summary>
        [Required]
        [RegularExpression("^(男|女){1}")]
        public string gender { get; set; } = "";

        /// <summary>
        /// 生日
        /// </summary>
        [Required]
        public DateTime birthday { get; set; }

        /// <summary>
        /// 身高
        /// </summary>
        [Required]
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double height { get; set; } = 0;

        /// <summary>
        /// 體重
        /// </summary>
        [Required]
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double weight { get; set; } = 0;

        /// <summary>
        /// 電話號碼
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]{8,10}$")]
        public string phone { get; set; } = "";

        /// <summary>
        /// 地址
        /// </summary>
        [MaxLength(50)]
        public string address { get; set; } = "";

        /// <summary>
        /// 信箱
        /// </summary>
        public string email { get; set; } = "";

        /// <summary>
        /// 問卷第一部分
        /// </summary>
        [Required]
        [MinLengthAttribute(7)]
        public List<QuestionnaireFirstPart> questionnaireFirstPart { get; set; } = new List<QuestionnaireFirstPart>();

        /// <summary>
        /// 問卷第二部分
        /// </summary>
        [Required]
        [MaxLengthAttribute(37)]
        public List<QuestionnaireTwoPart> questionnaireTwoPart { get; set; } = new List<QuestionnaireTwoPart>();

        /// <summary>
        /// AHA 及 ACSM 問卷部分
        /// </summary>
        [MaxLengthAttribute(35)]
        public List<Questionnaire_AHA_And_ACSM_Part> questionnaire_aha_and_acsm_part { get; set; } = new List<Questionnaire_AHA_And_ACSM_Part>();

        /// <summary>
        /// 第一次脈搏次數
        /// </summary>
        [RegularExpression("^[0-9]*$")]
        public int pulseRateOne { get; set; } = 0;

        /// <summary>
        /// 第二次脈搏次數
        /// </summary>
        [RegularExpression("^[0-9]*$")]
        public int pulseRateTwo { get; set; } = 0;

        /// <summary>
        /// 第三次脈搏次數
        /// </summary>
        [RegularExpression("^[0-9]*$")]
        public int pulseRateThree { get; set; } = 0;

        /// <summary>
        /// 原地站立抬膝次數
        /// </summary>
        [RegularExpression("^[0-9]*$")]
        public int kneeCrunchesFrequency { get; set; } = 0;

        /// <summary>
        /// 第一次坐姿體前彎
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double seatedForwardBendOne { get; set; } = 0;

        /// <summary>
        /// 第二次坐姿體前彎
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double seatedForwardBendTwo { get; set; } = 0;

        /// <summary>
        /// 第三次坐姿體前彎
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double seatedForwardBendThree { get; set; } = 0;

        /// <summary>
        /// 壯年開眼單足立
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double adultEyeOpeningMonopodSecond { get; set; } = 0;




        /// <summary>
        /// 原地站立抬膝
        /// </summary>
        [RegularExpression("^[0-9]*$")]
        public int kneeLiftFrequency { get; set; } = 0;

        /// <summary>
        /// 椅子坐姿體前彎
        /// </summary>
        [RegularExpression("^[0-9]*$")]
        public int armCurlUpperBodyFrequency { get; set; } = 0;

        /// <summary>
        /// 起立坐下
        /// </summary>
        [RegularExpression("^[0-9]*$")]
        public int armCurlLowerBodyFrequency { get; set; } = 0;

        /// <summary>
        /// 第一次椅子坐姿體前彎
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double chairSeatedForwardBendOne { get; set; } = 0;

        /// <summary>
        /// 第二次椅子坐姿體前彎
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double chairSeatedForwardBendTwo { get; set; } = 0;

        /// <summary>
        /// 第三次椅子坐姿體前彎
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double chairSeatedForwardBendThree { get; set; } = 0;

        /// <summary>
        /// 第一次拉背測驗距離
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double pullBackTestOne { get; set; } = 0;

        /// <summary>
        /// 第二次拉背測驗距離
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double pullBackTestTwo { get; set; } = 0;

        /// <summary>
        /// 老年開眼單足立
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double elderlyEyeOpeningMonopodSecond { get; set; } = 0;

        /// <summary>
        /// 第一次 2.44 公尺椅子坐立繞物
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double sittingAroundOneSecond { get; set; } = 0;

        /// <summary>
        /// 第二次 2.44 公尺椅子坐立繞物
        /// </summary>
        [RegularExpression("^([0-9]+|[0-9]+.[0-9]+)$")]
        public double sittingAroundTwoSecond { get; set; } = 0;
    }

    /// <summary>
    /// 問卷第一部分
    /// </summary>
    public class QuestionnaireFirstPart
    {
        /// <summary>
        /// 題號
        /// </summary>
        public int questionNumber { get; set; } = 0;
        
        /// <summary>
        /// 答案
        /// </summary>
        public bool answer { get; set; } = false;
    }

    /// <summary>
    /// 問卷第二部分
    /// </summary>
    public class QuestionnaireTwoPart
    {
        /// <summary>
        /// 題號
        /// </summary>
        public int questionNumber { get; set; } = 0;
        
        /// <summary>
        /// 子題號
        /// </summary>
        public string subject { get; set; } = "";

        /// <summary>
        /// 答案
        /// </summary>
        public bool answer { get; set; } = false;
    }

    /// <summary>
    /// AHA 及 ACSM 問卷部分
    /// </summary>
    public class Questionnaire_AHA_And_ACSM_Part
    {
        /// <summary>
        /// 問卷部分
        /// </summary>
        public int part { get; set; } = 0;

        /// <summary>
        /// 問卷部分題號
        /// </summary>
        public int subject { get; set; } = 0;
    }
}
