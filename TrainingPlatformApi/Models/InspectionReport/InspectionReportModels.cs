using Dapper;
using MySql.Data.MySqlClient;
using System.ComponentModel.DataAnnotations;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;

namespace TrainingPlatformApi.Models.InspectionReport
{
    public class InspectionReportModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public InspectionReportModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 取得正式訓練學員訓練各台儀器總功率
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public IEnumerable<T> GetStudentEachMachinaryTotalPowerList<T>(int studentId)
        {
            sqlStr = $@"SELECT instrument_settings.userAuthorizedId,
                               instrument_settings.machinaryCode,
	                           CASE instrument_settings.machinaryCode
			                        WHEN '1' THEN '屈腿伸腿訓練機'
			                        WHEN '2' THEN '推胸拉背訓練機'
			                        WHEN '3' THEN '髖關節外展內收訓練機'
			                        WHEN '4' THEN '蹬腿訓練機'
			                        WHEN '5' THEN '上斜推拉訓練機'
			                        WHEN '6' THEN '蝴蝶訓練機'
			                        WHEN '7' THEN '坐式上臂訓練機'
			                        WHEN '8' THEN '坐式轉體訓練機'
			                        WHEN '9' THEN '深蹲訓練機'
			                        WHEN '10' THEN '坐式腹背訓練機'
		                        END AS machinaryName,
	                            SUM(instrument_settings.power) AS power
                           FROM instrument_settings
                          WHERE     instrument_settings.userAuthorizedId = @userAuthorizedId
	                            AND instrument_settings.maxRange IS NULL
	                            AND instrument_settings.consciousEffort IS NOT NULL
                          GROUP BY instrument_settings.userAuthorizedId,
		                           instrument_settings.machinaryCode
                          ORDER BY instrument_settings.machinaryCode ASC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }


        /// <summary>
        /// 取得學員訓練機器總功率
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="machinaryCode">機器 id</param>
        /// <returns></returns>
        public T GetStudentTotalPower<T>(int studentId, int machinaryCode)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                               SUM(power) AS power
                          FROM instrument_settings
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND machinaryCode    = @machinaryCode
                               AND consciousEffort  IS NOT NULL
                         GROUP BY userAuthorizedId
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = machinaryCode
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得過去 20 次訓練狀況報表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="machinaryCode">機器編號</param>
        /// <param name="today">今天日期</param>
        /// <returns></returns>
        public IEnumerable<T> GetFormalTrainingStatusReport<T>(int studentId, int machinaryCode, string today)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                               MAX(level) AS level,
                               SUM(finish) AS finsh,
                               SUM(mistake) AS mistake,
                               CONCAT(SUBSTRING(trainingDate, 1, 4), '-', SUBSTRING(trainingDate, 5, 2), '-', SUBSTRING(trainingDate, 7, 2)) AS trainingDate,
                               CONCAT(FLOOR(SUBSTRING(trainingDate, 5, 2)), '/', FLOOR(SUBSTRING(trainingDate, 7, 2))) AS trainingDateText
                          FROM instrument_settings
                         WHERE     userAuthorizedId =  @userAuthorizedId
                               AND machinaryCode    =  @machinaryCode
                               AND maxRange         IS NULL
                               AND consciousEffort  IS NOT NULL
                               AND trainingDate     <= @today
                         GROUP BY trainingDate
                            ORDER BY trainingDate DESC LIMIT 20
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = machinaryCode,
                today = today
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 取得過去 20 次訓練功率
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="machinaryCode">機器編號</param>
        /// <param name="today">今天日期</param>
        /// <returns></returns>
        public IEnumerable<T> GetFormalTrainingPowerReport<T>(int studentId, int machinaryCode, string today)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                              SUM(power) AS dayTotalPower,
                              CONCAT(SUBSTRING(trainingDate, 1, 4), '-', SUBSTRING(trainingDate, 5, 2), '-', SUBSTRING(trainingDate, 7, 2)) AS trainingDate,
                              CONCAT(FLOOR(SUBSTRING(trainingDate, 5, 2)), '/', FLOOR(SUBSTRING(trainingDate, 7, 2))) AS trainingDateText
                         FROM instrument_settings
                        WHERE     userAuthorizedId =  @userAuthorizedId
                              AND machinaryCode    =  @machinaryCode
                              AND maxRange         IS NULL
                              AND consciousEffort  IS NOT NULL
                              AND trainingDate     <= @today
                        GROUP BY trainingDate
                           ORDER BY trainingDate DESC LIMIT 20
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = machinaryCode,
                today = today
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 取得學員機器訓練天清單
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public IEnumerable<T> GetTrainingAllDay<T>(int studentId)
        {
            sqlStr = $@"SELECT CONCAT(SUBSTRING(trainingDate, 1, 4), '-', SUBSTRING(trainingDate, 5, 2), '-', SUBSTRING(trainingDate, 7, 2)) AS trainingDate
                          FROM instrument_settings
                         WHERE     userAuthorizedId =  @userAuthorizedId
                               AND consciousEffort  IS NOT NULL
                               AND maxRange         IS NULL
	                     GROUP BY trainingDate
                         ORDER BY trainingDate ASC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).ToList();

            return result;
        }

        /// <summary>
        /// 取得單日報表清單
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="trainingDate"></param>
        /// <returns></returns>
        public IEnumerable<T> GetSingleDayReport<T>(int studentId, string trainingDate)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                              machinaryCode,
                              CASE machinaryCode
                                WHEN '1' THEN '屈腿伸腿訓練機'
                                WHEN '2' THEN '推胸拉背訓練機'
                                WHEN '3' THEN '髖關節外展內收訓練機'
                                WHEN '4' THEN '蹬腿訓練機'
                                WHEN '5' THEN '上斜推拉訓練機'
                                WHEN '6' THEN '蝴蝶訓練機'
                                WHEN '7' THEN '坐式上臂訓練機'
                                WHEN '8' THEN '坐式轉體訓練機'
                                WHEN '9' THEN '深蹲訓練機'
                                WHEN '10' THEN '坐式腹背訓練機'
                              END AS machinaryCodeName,
                              level,
                              finish,
                              mistake,
                              power,
                              speed,
                              maxSpeed,
                              consciousEffort,
                              CONCAT(SUBSTRING(trainingDate, 1, 4), '-', SUBSTRING(trainingDate, 5, 2), '-', SUBSTRING(trainingDate, 7, 2)) AS trainingDate
                         FROM instrument_settings
                        WHERE     userAuthorizedId =  @userAuthorizedId
                              AND trainingDate     =  @trainingDate
                              AND consciousEffort  IS NOT NULL
                              AND maxRange         IS NULL
                        ORDER BY trainingDate DESC, trainingTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                trainingDate = trainingDate.Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }
        
        /// <summary>
        /// 取得學員機器訓練總表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <returns></returns>
        public IEnumerable<T> GetStudentTrainingMachinarySummary<T>(int studentId, int machinaryCode)
        {
            sqlStr = $@"SELECT instrument_settings.userAuthorizedId,
                               MAX(instrument_settings.level) AS level,
                               SUM(instrument_settings.finish) AS finsh,
                               SUM(instrument_settings.mistake) AS mistake,
                               SUM(instrument_settings.power) AS power,
                               CONCAT(SUBSTRING(instrument_settings.trainingDate, 1, 4), '-', SUBSTRING(instrument_settings.trainingDate, 5, 2), '-', SUBSTRING(instrument_settings.trainingDate, 7, 2)) AS trainingDate,
	                           ism.mark
                          FROM instrument_settings
                          LEFT JOIN (SELECT userAuthorizedId,
                                            trainingDate,
                                            mark
                                       FROM instrument_settings_mark
                                      WHERE type = 'FT' ORDER BY updateRecordDate DESC, updateRecordTime DESC) AS ism
                            ON (    instrument_settings.userAuthorizedId = ism.userAuthorizedId
		                        AND instrument_settings.trainingDate = ism.trainingDate)
                         WHERE     instrument_settings.userAuthorizedId = @userAuthorizedId
                               AND instrument_settings.machinaryCode = @machinaryCode
                               AND instrument_settings.maxRange IS NULL
                               AND instrument_settings.consciousEffort IS NOT NULL
                         GROUP BY instrument_settings.trainingDate, ism.mark
                         ORDER BY instrument_settings.trainingDate DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = machinaryCode
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 檢查是否有當天機器訓練資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public IEnumerable<T> InspectFormalTrainingSingleDay<T>(int studentId, int machinaryCode, RequestStudentTrainingMachinarySummaryListModels obj)
        {
            sqlStr = @"SELECT userAuthorizedId,
                              machinaryCode,
                              level,
                              finish,
                              mistake,
	                          power,
                              speed,
                              maxSpeed,
                              consciousEffort,
                              trainingDate,
                              trainingTime
                         FROM instrument_settings
                        WHERE     userAuthorizedId = @userAuthorizedId
                              AND machinaryCode    = @machinaryCode
                              AND trainingDate     = @trainingDate
                              AND maxRange         IS NULL
                              AND consciousEffort  IS NOT NULL
                              AND maxSpeed         IS NOT NULL
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = machinaryCode,
                trainingDate = obj.trainingDate.ToString("yyyyMMdd")
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 檢查是否有正式訓練當天備註資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public T InspectFormalTrainingSingleDayMark<T>(int studentId, int machinaryCode, RequestStudentTrainingMachinarySummaryListModels obj)
        {
            sqlStr = @"SELECT userAuthorizedId,
                              trainingDate,
                              type,
                              machinaryCode,
                              mark,
                              newRecordId,
                              newRecordDate,
                              newRecordTime,
                              updateRecordId,
                              updateRecordDate,
                              updateRecordTime
                         FROM instrument_settings_mark
                        WHERE     userAuthorizedId = @userAuthorizedId
                              AND type             = 'FT'
                              AND machinaryCode    = @machinaryCode
                              AND trainingDate     = @trainingDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = machinaryCode,
                trainingDate = obj.trainingDate
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增正式訓練當天備註資料
        /// </summary>
        /// <param name="recordId"></param>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int InsertFormalTrainingSingleDayMark(int recordId, int studentId, int machinaryCode, RequestStudentTrainingMachinarySummaryListModels obj)
        {
            sqlStr = $@"INSERT INTO instrument_settings_mark ( userAuthorizedId,  trainingDate,  type,  machinaryCode,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                             (@userAuthorizedId, @trainingDate,  'FT', @machinaryCode, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                trainingDate = obj.trainingDate.ToString("yyyyMMdd"),
                machinaryCode = machinaryCode,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新正式訓練當天備註資料
        /// </summary>
        /// <param name="recordId"></param>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int UpdateFormalTrainingSingleDayMark(int recordId, int studentId, int machinaryCode, RequestStudentTrainingMachinarySummaryListModels obj)
        {
            sqlStr = $@"UPDATE instrument_settings_mark
                           SET mark             = @mark,
                               updateRecordId   = @updateRecordId,
                               updateRecordDate = @updateRecordDate,
                               updateRecordTime = @updateRecordTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND type             = 'FT'
                               AND machinaryCode    = @machinaryCode
                               AND trainingDate     = @trainingDate
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime,
                userAuthorizedId = studentId,
                machinaryCode = machinaryCode,
                trainingDate = obj.trainingDate.ToString("yyyyMMdd")
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 刪除正式訓練當天備註資料
        /// </summary>
        /// <param name="recordId"></param>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int DeleteFormalTrainingSingleDayMark(int studentId, int machinaryCode, RequestDeleteStudentTrainingMachinarySummaryListModels obj)
        {
            sqlStr = $@"DELETE FROM instrument_settings_mark
                              WHERE     userAuthorizedId = @userAuthorizedId
                                    AND type             = 'FT'
                                    AND machinaryCode    = @machinaryCode
                                    AND trainingDate     = @trainingDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = machinaryCode,
                trainingDate = obj.trainingDate.ToString("yyyyMMdd")
            };

            int deleteRow = conn.Execute(sqlStr, param);

            return deleteRow;
        }
    }

    /// <summary>
    /// 接收 單日報表日期
    /// </summary>
    public class RequestStudentSingleDayReportModels
    {
        /// <summary>
        /// 訓練日期
        /// </summary>
        [Required]
        public DateTime trainingDate { get; set; }
    }

    /// <summary>
    /// 接收 新增或更新當天正式機器訓練備註
    /// </summary>
    public class RequestStudentTrainingMachinarySummaryListModels
    {
        /// <summary>
        /// 訓練日期
        /// </summary>
        [Required]
        public DateTime trainingDate { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        [MaxLength(150)]
        public string mark { get; set; } = "";
    }

    /// <summary>
    /// 接收 刪除當天正式機器訓練備註
    /// </summary>
    public class RequestDeleteStudentTrainingMachinarySummaryListModels
    {
        /// <summary>
        /// 訓練日期
        /// </summary>
        [Required]
        public DateTime trainingDate { get; set; }
    }
}
