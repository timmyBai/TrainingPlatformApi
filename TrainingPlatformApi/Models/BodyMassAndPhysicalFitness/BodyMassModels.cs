using MySql.Data.MySqlClient;
using Dapper;

using TrainingPlatformApi.Utils;

namespace TrainingPlatformApi.Models.BodyMassAndPhysicalFitness
{
    public class BodyMassModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public BodyMassModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 新增 使用者生體質量紀錄表(註冊時先後先新增身高、體重、bmi 紀錄)
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="height">身高</param>
        /// <param name="weight">體重</param>
        /// <returns></returns>
        public int InsertBodyMassTable(int userId, string account, double height, double weight)
        {
            sqlStr = @"INSERT INTO body_mass_record ( userAuthorizedId,  height,  weight,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                    (@userAuthorizedId, @height, @weight, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime);
	                                    
                      INSERT INTO body_composition_bmi ( userAuthorizedId,  bmi,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate, updateRecordTime) VALUES
                                                       (@userAuthorizedId, @bmi, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime);
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = userId,
                account = account,
                height = height,
                weight = weight,
                bmi = Math.Round(weight / Math.Pow(height / 100.0, 2), 1),
                updateDate = nowDate,
                updateTime = nowTime,
                newRecordId = userId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = userId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }
    }
}
