using Dapper;
using MySql.Data.MySqlClient;

using TrainingPlatformApi.VM.BodyMassAndPhysicalFitness;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.Settings;

namespace TrainingPlatformApi.Models.BodyMass
{
    public class BodyCompositionAndPhysicalFitnessModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";
        
        public BodyCompositionAndPhysicalFitnessModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 取得學員身體組成 bmi 歷史資料
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <returns></returns>
        public IEnumerable<BodyMassBmiVM> GetBodyCompositionBmiHistory(int studentId, string gender, int age)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               body_composition_bmi.bmi,
                               body_composition_bmi.mark,
                               body_composition_bmi.updateRecordId,
                               CONCAT(SUBSTRING(body_composition_bmi.updateRecordDate, 1, 4), '-', SUBSTRING(body_composition_bmi.updateRecordDate, 5, 2), '-', SUBSTRING(body_composition_bmi.updateRecordDate, 7, 2)) AS updateRecordDate,
                               CONCAT(SUBSTRING(body_composition_bmi.updateRecordTime, 1, 2), ':', SUBSTRING(body_composition_bmi.updateRecordTime, 3, 2), ':', SUBSTRING(body_composition_bmi.updateRecordTime, 5, 2)) AS updateRecordTime
                          FROM user_authorized
                          LEFT JOIN (body_composition_bmi)
                            ON (user_authorized.userAuthorizedId = body_composition_bmi.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId      = @userAuthorizedId
                               AND body_composition_bmi.userAuthorizedId = @userAuthorizedId
                         ORDER BY body_composition_bmi.updateRecordDate DESC,
                                  body_composition_bmi.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };
            
            var result = conn.Query<BodyMassBmiVM>(sqlStr, param).ToList();
            
            result.ForEach(item =>
            {
                item.bmiCommentLevel = new NormUtils().BmiPercentageLevel(gender, age, item.bmi);
            });

            return result;
        }

        /// <summary>
        /// 取得學員身體組成 體脂肪 歷史資料
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <returns></returns>
        public IEnumerable<BodyMassBodyFatVM> GetBodyCompositionBodyFatHistory(int studentId, string gender, int age)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               body_composition_bodyFat.bodyFat,
                               body_composition_bodyFat.mark,
                               body_composition_bodyFat.updateRecordId,
                               CONCAT(SUBSTRING(body_composition_bodyFat.updateRecordDate, 1, 4), '-', SUBSTRING(body_composition_bodyFat.updateRecordDate, 5, 2), '-', SUBSTRING(body_composition_bodyFat.updateRecordDate, 7, 2)) AS updateRecordDate,
                               CONCAT(SUBSTRING(body_composition_bodyFat.updateRecordTime, 1, 2), ':', SUBSTRING(body_composition_bodyFat.updateRecordTime, 3, 2), ':', SUBSTRING(body_composition_bodyFat.updateRecordTime, 5, 2)) AS updateRecordTime
                          FROM user_authorized
                          LEFT JOIN body_composition_bodyFat
                            ON (user_authorized.userAuthorizedId = body_composition_bodyFat.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId          = @userAuthorizedId
                               AND body_composition_bodyFat.userAuthorizedId = @userAuthorizedId
                         ORDER BY body_composition_bodyFat.updateRecordDate DESC,
                                  body_composition_bodyFat.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<BodyMassBodyFatVM>(sqlStr, param).ToList();

            result.ForEach(item =>
            {
                item.bodyFatCommentLevel = new NormUtils().BodyFatPercentageLevel(gender, age, item.bodyFat);
            });

            return result;
        }

        /// <summary>
        /// 取得學員身體組成 內臟脂肪 歷史資料
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public IEnumerable<BodyMassVisceralFatVM> GetBodyCompositionVisceralFatHistory(int studentId)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               body_composition_visceralFat.visceralFat,
                               body_composition_visceralFat.mark,
                               body_composition_visceralFat.updateRecordId,
                               CONCAT(SUBSTRING(body_composition_visceralFat.updateRecordDate, 1, 4), '-', SUBSTRING(body_composition_visceralFat.updateRecordDate, 5, 2), '-', SUBSTRING(body_composition_visceralFat.updateRecordDate, 7, 2)) AS updateRecordDate,
                               CONCAT(SUBSTRING(body_composition_visceralFat.updateRecordTime, 1, 2), ':', SUBSTRING(body_composition_visceralFat.updateRecordTime, 3, 2), ':', SUBSTRING(body_composition_visceralFat.updateRecordTime, 5, 2)) AS updateRecordTime
                          FROM user_authorized
                          LEFT JOIN body_composition_visceralFat
                            ON (user_authorized.userAuthorizedId = body_composition_visceralFat.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId              = @userAuthorizedId
                               AND body_composition_visceralFat.userAuthorizedId = @userAuthorizedId
                         ORDER BY body_composition_visceralFat.updateRecordDate DESC,
                                  body_composition_visceralFat.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<BodyMassVisceralFatVM>(sqlStr, param).ToList();

            result.ForEach(item =>
            {
                item.visceralFatCommentLevel = new NormUtils().VisceralFatPercentageLevel(item.visceralFat);
            });

            return result;
        }

        /// <summary>
        /// 取得學員身體組成 骨骼肌率 歷史資料
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="gender">性別</param>
        /// <returns></returns>
        public IEnumerable<BodyMassSkeletalMuscleRateVM> GetBodyCompositionSkeletalMuscleHistory(int studentId, string gender)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               body_composition_skeletalMuscleRate.skeletalMuscleRate,
                               body_composition_skeletalMuscleRate.mark,
                               body_composition_skeletalMuscleRate.updateRecordId,
                               CONCAT(SUBSTRING(body_composition_skeletalMuscleRate.updateRecordDate, 1, 4), '-', SUBSTRING(body_composition_skeletalMuscleRate.updateRecordDate, 5, 2), '-', SUBSTRING(body_composition_skeletalMuscleRate.updateRecordDate, 7, 2)) AS updateRecordDate,
                               CONCAT(SUBSTRING(body_composition_skeletalMuscleRate.updateRecordTime, 1, 2), ':', SUBSTRING(body_composition_skeletalMuscleRate.updateRecordTime, 3, 2), ':', SUBSTRING(body_composition_skeletalMuscleRate.updateRecordTime, 5, 2)) AS updateRecordTime
                          FROM user_authorized
                          LEFT JOIN body_composition_skeletalMuscleRate
                            ON (user_authorized.userAuthorizedId = body_composition_skeletalMuscleRate.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId                     = @userAuthorizedId
                               AND body_composition_skeletalMuscleRate.userAuthorizedId = @userAuthorizedId
                         ORDER BY body_composition_skeletalMuscleRate.updateRecordDate DESC,
                                  body_composition_skeletalMuscleRate.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<BodyMassSkeletalMuscleRateVM>(sqlStr, param).ToList();

            result.ForEach(item =>
            {
                item.skeletalMuscleRateCommentLevel = new NormUtils().SkeletalMuscleRatePercentageLevel(gender, item.skeletalMuscleRate);
            });

            return result;
        }

        /// <summary>
        /// 取得學員心肺適能歷史資料
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <returns></returns>
        public IEnumerable<PhysicalFitnessCardiorespiratoryFitnessHistoryVM> GetPhysicalFitnessCardiorespiratoryFitnessHistory(int studentId, string gender, int age)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               (180 * 100) / ((physical_fitness_cardiorespiratory_fitness.pulseRateOne + physical_fitness_cardiorespiratory_fitness.pulseRateTwo + physical_fitness_cardiorespiratory_fitness.pulseRateThree) * 2) AS setUpTest,
                               physical_fitness_cardiorespiratory_fitness.kneeLiftFrequency,
                               physical_fitness_cardiorespiratory_fitness.mark,
                               physical_fitness_cardiorespiratory_fitness.updateRecordId,
                               CONCAT(SUBSTRING(physical_fitness_cardiorespiratory_fitness.updateRecordDate, 1, 4), '-', SUBSTRING(physical_fitness_cardiorespiratory_fitness.updateRecordDate, 5, 2), '-', SUBSTRING(physical_fitness_cardiorespiratory_fitness.updateRecordDate, 7, 2)) AS updateRecordDate,
                               CONCAT(SUBSTRING(physical_fitness_cardiorespiratory_fitness.updateRecordTime, 1, 2), ':', SUBSTRING(physical_fitness_cardiorespiratory_fitness.updateRecordTime, 3, 2), ':', SUBSTRING(physical_fitness_cardiorespiratory_fitness.updateRecordTime, 5, 2)) AS updateRecordTime
                          FROM user_authorized
                          LEFT JOIN (physical_fitness_cardiorespiratory_fitness)
                            ON (user_authorized.userAuthorizedId = physical_fitness_cardiorespiratory_fitness.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId                            = @userAuthorizedId
                               AND physical_fitness_cardiorespiratory_fitness.userAuthorizedId = @userAuthorizedId
                         ORDER BY physical_fitness_cardiorespiratory_fitness.updateRecordDate DESC,
                                  physical_fitness_cardiorespiratory_fitness.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId,
            };

            var result = conn.Query<PhysicalFitnessCardiorespiratoryFitnessHistoryVM>(sqlStr, param).ToList();

            if (age >= 21 && age <= 65)
            {
                result.ForEach((item) =>
                {
                    // 心肺適能
                    item.cardiorespiratoryFitness = new NormUtils().PhysicalFitnessSetUpTestLevel(gender, age, item.setUpTest);
                    item.cardiorespiratoryFitnessCommentLevel = new NormUtils().FractionConvertComments(item.cardiorespiratoryFitness, Enum.GetName(NormEnum.YoungManCardiorespiratoryFitness));
                });
            }
            else
            {
                result.ForEach((item) =>
                {
                    // 心肺適能
                    item.cardiorespiratoryFitness = new NormUtils().PhysicalFitnessStandingKneeRaiseLevel(gender, age, item.kneeLiftFrequency);
                    item.cardiorespiratoryFitnessCommentLevel = new NormUtils().FractionConvertComments(item.cardiorespiratoryFitness, Enum.GetName(NormEnum.ElderlyCardiorespiratoryFitness));
                });
            }

            return result;
        }

        /// <summary>
        /// 取得學員肌力與肌耐力歷史資料
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <returns></returns>
        public IEnumerable<PhysicalFitnessMuscleStrengthAndMuscleEnduranceHistoryVM> GetPhysicalFitnessMuscleStrengthAndMuscleEnduranceHistory(int studentId, string gender, int age)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               physical_fitness_muscle_strength_and_muscle_endurance.kneeCrunchesFrequency,
                               physical_fitness_muscle_strength_and_muscle_endurance.armCurlUpperBodyFrequency,
                               physical_fitness_muscle_strength_and_muscle_endurance.armCurlLowerBodyFrequency,
                               physical_fitness_muscle_strength_and_muscle_endurance.mark,
                               physical_fitness_muscle_strength_and_muscle_endurance.updateRecordId,
                               CONCAT(SUBSTRING(physical_fitness_muscle_strength_and_muscle_endurance.updateRecordDate, 1, 4), '-', SUBSTRING(physical_fitness_muscle_strength_and_muscle_endurance.updateRecordDate, 5, 2), '-', SUBSTRING(physical_fitness_muscle_strength_and_muscle_endurance.updateRecordDate, 7, 2)) AS updateRecordDate,
                               CONCAT(SUBSTRING(physical_fitness_muscle_strength_and_muscle_endurance.updateRecordTime, 1, 2), ':', SUBSTRING(physical_fitness_muscle_strength_and_muscle_endurance.updateRecordTime, 3, 2), ':', SUBSTRING(physical_fitness_muscle_strength_and_muscle_endurance.updateRecordTime, 5, 2)) AS updateRecordTime
                          FROM user_authorized
                          LEFT JOIN (physical_fitness_muscle_strength_and_muscle_endurance)
                            ON (user_authorized.userAuthorizedId = physical_fitness_muscle_strength_and_muscle_endurance.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId                                       = @userAuthorizedId
                               AND physical_fitness_muscle_strength_and_muscle_endurance.userAuthorizedId = @userAuthorizedId
                         ORDER BY physical_fitness_muscle_strength_and_muscle_endurance.updateRecordDate DESC,
                                  physical_fitness_muscle_strength_and_muscle_endurance.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<PhysicalFitnessMuscleStrengthAndMuscleEnduranceHistoryVM>(sqlStr, param).ToList();

            // 肌力與肌耐力
            if (age >= 21 && age <= 65)
            {
                result.ToList().ForEach((item) =>
                {
                    // 肌力與肌耐力
                    item.muscleStrengthAndMuscleEnduranceFitness = new NormUtils().PhysicalFitnessKneeCrunchesFrequencyLevel(gender, age, item.kneeCrunchesFrequency);
                    item.muscleStrengthAndMuscleEnduranceCommentLevel = new NormUtils().FractionConvertComments(item.muscleStrengthAndMuscleEnduranceFitness, Enum.GetName(NormEnum.YoungManMuscleStrengthAndMuscleEndurance));
                });
            }
            else
            {
                result.ToList().ForEach((item) =>
                {
                    var armCurlUpperBodyFrequency = new NormUtils().PhysicalFitnessArmCurlUpperBodyLevel(gender, age, item.armCurlUpperBodyFrequency);
                    var armCurlLowerBodyFrequency = new NormUtils().PhysicalFitnessArmCurlLowerBodyFrequencyLevel(gender, age, item.armCurlLowerBodyFrequency);
                    item.muscleStrengthAndMuscleEnduranceFitness = (armCurlUpperBodyFrequency + armCurlLowerBodyFrequency) / 2;
                    item.muscleStrengthAndMuscleEnduranceCommentLevel = new NormUtils().FractionConvertComments(item.muscleStrengthAndMuscleEnduranceFitness, Enum.GetName(NormEnum.ElderlyMuscleStrengthAndMuscleEndurance));
                });
            }

            return result;
        }

        /// <summary>
        /// 取得學員柔軟度歷史資料
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <returns></returns>
        public IEnumerable<PhysicalFitnessSoftnessHistoryVM> GetPhysicalFitnessSoftnessHistory(int studentId, string gender, int age)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               physical_fitness_softness.seatedForwardBendOne,
                               physical_fitness_softness.seatedForwardBendTwo,
                               physical_fitness_softness.seatedForwardBendThree,
                               physical_fitness_softness.chairSeatedForwardBendOne,
                               physical_fitness_softness.chairSeatedForwardBendTwo,
                               physical_fitness_softness.chairSeatedForwardBendThree,
                               physical_fitness_softness.pullBackTestOne,
                               physical_fitness_softness.pullBackTestTwo,
                               physical_fitness_softness.mark,
                               physical_fitness_softness.updateRecordId,
                               CONCAT(SUBSTRING(physical_fitness_softness.updateRecordDate, 1, 4), '-', SUBSTRING(physical_fitness_softness.updateRecordDate, 5, 2), '-', SUBSTRING(physical_fitness_softness.updateRecordDate, 7, 2)) AS updateRecordDate,
                               CONCAT(SUBSTRING(physical_fitness_softness.updateRecordTime, 1, 2), ':', SUBSTRING(physical_fitness_softness.updateRecordTime, 3, 2), ':', SUBSTRING(physical_fitness_softness.updateRecordTime, 5, 2)) AS updateRecordTime
                          FROM user_authorized
                          LEFT JOIN (physical_fitness_softness)
                            ON (user_authorized.userAuthorizedId = physical_fitness_softness.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId           = @userAuthorizedId
                               AND physical_fitness_softness.userAuthorizedId = @userAuthorizedId
                         ORDER BY physical_fitness_softness.updateRecordDate DESC,
                                  physical_fitness_softness.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<PhysicalFitnessSoftnessHistoryVM>(sqlStr, param);

            if (age > 21 && age <= 65)
            {
                result.ToList().ForEach((item) =>
                {
                    item.softnessFitness = new NormUtils().PhysicalFitnessSeatedForwardBend(gender, age, item.seatedForwardBendOne, item.chairSeatedForwardBendTwo, item.chairSeatedForwardBendThree);
                    item.softnessComment = new NormUtils().FractionConvertComments(item.softnessFitness, Enum.GetName(NormEnum.YoungManSoftness));
                });
            }
            else
            {
                result.ToList().ForEach((item) =>
                {
                    var chairSeatedForwardBend = new NormUtils().PhysicalFitnessChairSeatedForwardBend(gender, age, item.chairSeatedForwardBendOne, item.chairSeatedForwardBendTwo, item.chairSeatedForwardBendThree);
                    var pullBackTest = new NormUtils().PhysicalFitnessPullBackTest(gender, age, item.pullBackTestOne, item.pullBackTestTwo);

                    item.softnessFitness = (chairSeatedForwardBend + pullBackTest) / 2;
                    item.softnessComment = new NormUtils().FractionConvertComments(item.softnessFitness, Enum.GetName(NormEnum.ElderlySoftness));
                });
            }

            return result;
        }

        /// <summary>
        /// 取得學員平衡歷史資料
        /// </summary>
        /// <param name="studentId">學員 id</param>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <returns></returns>
        public IEnumerable<PhysicalFitnessBalanceHistoryVM> GetPhysicalFitnessBalanceHistory(int studentId, string gender, int age)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               physical_fitness_balance.adultEyeOpeningMonopodSecond,
                               physical_fitness_balance.elderlyEyeOpeningMonopodSecond,
                               physical_fitness_balance.sittingAroundOneSecond,
                               physical_fitness_balance.sittingAroundTwoSecond,
                               physical_fitness_balance.mark,
                               physical_fitness_balance.updateRecordId,
                               CONCAT(SUBSTRING(physical_fitness_balance.updateRecordDate, 1, 4), '-', SUBSTRING(physical_fitness_balance.updateRecordDate, 5, 2), '-', SUBSTRING(physical_fitness_balance.updateRecordDate, 7, 2)) AS updateRecordDate,
                               CONCAT(SUBSTRING(physical_fitness_balance.updateRecordTime, 1, 2), ':', SUBSTRING(physical_fitness_balance.updateRecordTime, 3, 2), ':', SUBSTRING(physical_fitness_balance.updateRecordTime, 5, 2)) AS updateRecordTime
                          FROM user_authorized
                          LEFT JOIN (physical_fitness_balance)
                            ON (user_authorized.userAuthorizedId = physical_fitness_balance.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId          = @userAuthorizedId
                               AND physical_fitness_balance.userAuthorizedId = @userAuthorizedId
                         ORDER BY physical_fitness_balance.updateRecordDate DESC,
                                  physical_fitness_balance.updateRecordTime DESC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<PhysicalFitnessBalanceHistoryVM>(sqlStr, param);

            if (age > 21 && age <= 65)
            {
                result.ToList().ForEach((item) =>
                {
                    item.balanceFitness = new NormUtils().PhysicalFitnessAdultEyeOpeningMonopodSecond(age, item.adultEyeOpeningMonopodSecond);
                    item.balanceComment = new NormUtils().FractionConvertComments(item.balanceFitness, Enum.GetName(NormEnum.YoungManBalance));
                });
            }
            else
            {
                result.ToList().ForEach((item) =>
                {
                    item.balanceFitness = new NormUtils().PhysicalFitnessElderlyEyeOpeningMonopodSecond(gender, age, item.elderlyEyeOpeningMonopodSecond);
                    item.balanceComment = new NormUtils().FractionConvertComments(item.balanceFitness, Enum.GetName(NormEnum.ElderlyBalance));
                });
            }

            return result;
        }
    }
}
