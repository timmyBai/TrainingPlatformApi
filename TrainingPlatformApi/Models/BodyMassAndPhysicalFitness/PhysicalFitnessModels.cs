using Dapper;
using MySql.Data.MySqlClient;
using System.ComponentModel.DataAnnotations;

using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;

namespace TrainingPlatformApi.Models.BodyMassAndPhysicalFitness
{
    public class PhysicalFitnessModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public PhysicalFitnessModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查最新體適能-心肺適能資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public T InspactTodayPhysicalFitnessCardiorespiratoryFitnessRecord<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               physical_fitness_cardiorespiratory_fitness.pulseRateOne,
                               physical_fitness_cardiorespiratory_fitness.pulseRateTwo,
                               physical_fitness_cardiorespiratory_fitness.pulseRateThree,
                               physical_fitness_cardiorespiratory_fitness.kneeLiftFrequency,
                               physical_fitness_cardiorespiratory_fitness.mark,
                               physical_fitness_cardiorespiratory_fitness.newRecordId,
                               physical_fitness_cardiorespiratory_fitness.newRecordDate,
                               physical_fitness_cardiorespiratory_fitness.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (physical_fitness_cardiorespiratory_fitness)
                            ON (user_authorized.userAuthorizedId = physical_fitness_cardiorespiratory_fitness.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId                            = @userAuthorizedId
                               AND physical_fitness_cardiorespiratory_fitness.userAuthorizedId = @userAuthorizedId
                               AND physical_fitness_cardiorespiratory_fitness.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增 體適能-心肺適能 登階測驗與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-心肺適能 登階測驗與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessCardiorespiratoryStepTestAndMark(int recordId, int studentId, RequestPhysicalFitnessCardiorespiratoryFitnessStepTestModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_cardiorespiratory_fitness
                            ( userAuthorizedId,  pulseRateOne,  pulseRateTwo,  pulseRateThree,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @pulseRateOne, @pulseRateTwo, @pulseRateThree, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_cardiorespiratory_fitness
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            var nowDate = new DayUtils().GetFullDate();
            var nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                pulseRateOne = obj.pulseRateOne,
                pulseRateTwo = obj.pulseRateTwo,
                pulseRateThree = obj.pulseRateThree,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 心肺適能 登階測驗與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <param name="obj">接收 體適能-心肺適能 登階測驗與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessCardiorespiratoryStepTestAndMark(int recordId, int studentId, string today, RequestPhysicalFitnessCardiorespiratoryFitnessStepTestModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = @$"UPDATE physical_fitness_cardiorespiratory_fitness
                               SET pulseRateOne     = @pulseRateOne,
                                   pulseRateTwo     = @pulseRateTwo,
                                   pulseRateThree   = @pulseRateThree,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = @$"UPDATE physical_fitness_cardiorespiratory_fitness
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                mark = obj.mark,
                pulseRateOne = obj.pulseRateOne,
                pulseRateTwo = obj.pulseRateTwo,
                pulseRateThree = obj.pulseRateThree,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增 體適能-心肺適能 原地站立抬膝與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-心肺適能 原地站立抬膝與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessCardiorespiratoryKneeLiftFrequencyAndMark(int recordId, int studentId, RequestPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_cardiorespiratory_fitness
                            ( userAuthorizedId,  kneeLiftFrequency,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @kneeLiftFrequency, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_cardiorespiratory_fitness
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            var nowDate = new DayUtils().GetFullDate();
            var nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                kneeLiftFrequency = obj.kneeLiftFrequency,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新 體適能-心肺適能 原地站立抬膝與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <param name="obj">接收 體適能-心肺適能 原地站立抬膝與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessCardiorespiratoryKneeLiftFrequencyAndMark(int recordId, int studentId, string today, RequestPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_cardiorespiratory_fitness
                               SET kneeLiftFrequency = @kneeLiftFrequency,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_cardiorespiratory_fitness
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            var nowDate = new DayUtils().GetFullDate();
            var nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                kneeLiftFrequency = obj.kneeLiftFrequency,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 檢查最新 體適能-肌力與肌耐力資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public T InspactTodayPhysicalFitnessMuscleStrengthAndMuscleEndurance<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               physical_fitness_muscle_strength_and_muscle_endurance.kneeCrunchesFrequency,
                               physical_fitness_muscle_strength_and_muscle_endurance.armCurlUpperBodyFrequency,
                               physical_fitness_muscle_strength_and_muscle_endurance.armCurlLowerBodyFrequency,
                               physical_fitness_muscle_strength_and_muscle_endurance.newRecordId,
                               physical_fitness_muscle_strength_and_muscle_endurance.newRecordDate,
                               physical_fitness_muscle_strength_and_muscle_endurance.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (physical_fitness_muscle_strength_and_muscle_endurance)
                            ON (user_authorized.userAuthorizedId = physical_fitness_muscle_strength_and_muscle_endurance.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId                                       = @userAuthorizedId
                               AND physical_fitness_muscle_strength_and_muscle_endurance.userAuthorizedId = @userAuthorizedId
                               AND physical_fitness_muscle_strength_and_muscle_endurance.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增 體適能-肌力與肌耐力 屈膝仰做起坐與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-肌力與肌耐力 屈膝仰做起坐與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyAndMark(int recordId, int studentId, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_muscle_strength_and_muscle_endurance
                            ( userAuthorizedId,  kneeCrunchesFrequency,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @kneeCrunchesFrequency, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_muscle_strength_and_muscle_endurance
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                kneeCrunchesFrequency = obj.kneeCrunchesFrequency,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 新增 體適能-肌力與肌耐力 屈膝仰做起坐與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <param name="obj">接收 體適能-肌力與肌耐力 屈膝仰做起坐與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyAndMark(int recordId, int studentId, string today, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_muscle_strength_and_muscle_endurance
                               SET kneeCrunchesFrequency = @kneeCrunchesFrequency,
                                   updateRecordId        = @updateRecordId,
                                   updateRecordTime      = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_muscle_strength_and_muscle_endurance
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            var nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                kneeCrunchesFrequency = obj.kneeCrunchesFrequency,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增 體適能-肌力與肌耐力 手臂彎舉(上肢)與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-肌力與肌耐力 手臂彎舉(上肢)與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyAndMark(int recordId, int studentId, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_muscle_strength_and_muscle_endurance
                            ( userAuthorizedId,  armCurlUpperBodyFrequency,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @armCurlUpperBodyFrequency, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_muscle_strength_and_muscle_endurance
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                armCurlUpperBodyFrequency = obj.armCurlUpperBodyFrequency,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新 體適能-肌力與肌耐力 手臂彎舉(上肢)與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <param name="obj">接收 體適能-肌力與肌耐力 手臂彎舉(上肢)與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyAndMark(int recordId, int studentId, string today, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_muscle_strength_and_muscle_endurance
                               SET armCurlUpperBodyFrequency = @armCurlUpperBodyFrequency,
                                   updateRecordId            = @updateRecordId,
                                   updateRecordTime          = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_muscle_strength_and_muscle_endurance
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            var nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                armCurlUpperBodyFrequency = obj.armCurlUpperBodyFrequency,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增 體適能-肌力與肌耐力 手臂彎舉(下肢)與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-肌力與肌耐力 手臂彎舉(下肢)與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyAndMark(int recordId, int studentId, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_muscle_strength_and_muscle_endurance
                            ( userAuthorizedId,  armCurlLowerBodyFrequency,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @armCurlLowerBodyFrequency, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_muscle_strength_and_muscle_endurance
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                armCurlLowerBodyFrequency = obj.armCurlLowerBodyFrequency,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 新增 體適能-肌力與肌耐力 手臂彎舉(下肢)與備註
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-肌力與肌耐力 手臂彎舉(下肢)與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyAndMark(int recordId, int studentId, string today, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_muscle_strength_and_muscle_endurance
                               SET armCurlLowerBodyFrequency = @armCurlLowerBodyFrequency,
                                   updateRecordId            = @updateRecordId,
                                   updateRecordTime          = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_muscle_strength_and_muscle_endurance
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                armCurlLowerBodyFrequency = obj.armCurlLowerBodyFrequency,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 檢查最新 體適能-柔軟度資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public T InspactTodayPhysicalFitnessSoftness<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               physical_fitness_softness.seatedForwardBendOne,
                               physical_fitness_softness.seatedForwardBendTwo,
                               physical_fitness_softness.seatedForwardBendThree,
                               physical_fitness_softness.chairSeatedForwardBendOne,
                               physical_fitness_softness.chairSeatedForwardBendTwo,
                               physical_fitness_softness.chairSeatedForwardBendThree,
                               physical_fitness_softness.pullBackTestOne,
                               physical_fitness_softness.pullBackTestTwo,
                               physical_fitness_softness.newRecordId,
                               physical_fitness_softness.newRecordDate,
                               physical_fitness_softness.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (physical_fitness_softness)
                            ON (user_authorized.userAuthorizedId = physical_fitness_softness.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId           = @userAuthorizedId
                               AND physical_fitness_softness.userAuthorizedId = @userAuthorizedId
                               AND physical_fitness_softness.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 更新學員 體適能-柔軟度 坐姿體前彎與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-柔軟度 坐姿體前彎與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessSoftnessSeatedForwardBendAndMark(int recordId, int studentId, RequestPysicalFitnessSoftnessSeatedForwardBendModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_softness
                            ( userAuthorizedId,  seatedForwardBendOne,  seatedForwardBendTwo,  seatedForwardBendThree,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @seatedForwardBendOne, @seatedForwardBendTwo, @seatedForwardBendThree, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_softness
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                seatedForwardBendOne = obj.seatedForwardBendOne,
                seatedForwardBendTwo = obj.seatedForwardBendTwo,
                seatedForwardBendThree = obj.seatedForwardBendThree,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-柔軟度 坐姿體前彎與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today"></param>
        /// <param name="obj">接收 體適能-柔軟度 坐姿體前彎與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessSoftnessSeatedForwardBendAndMark(int recordId, int studentId, string today, RequestPysicalFitnessSoftnessSeatedForwardBendModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_softness
                               SET seatedForwardBendOne   = @seatedForwardBendOne,
                                   seatedForwardBendTwo   = @seatedForwardBendTwo,
                                   seatedForwardBendThree = @seatedForwardBendThree,
                                   updateRecordId         = @updateRecordId,
                                   updateRecordTime       = @updateRecordTime
                             WHERE     userAuthorizedId       = @userAuthorizedId
                                   AND updateRecordDate       = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_softness
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                seatedForwardBendOne = obj.seatedForwardBendOne,
                seatedForwardBendTwo = obj.seatedForwardBendTwo,
                seatedForwardBendThree = obj.seatedForwardBendThree,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增學員 體適能-柔軟度 椅子坐姿體前彎與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-柔軟度 椅子坐姿體前彎與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessSoftnessChairSeatedForwardBendAndMark(int recordId, int studentId, RequestPysicalFitnessSoftnessChairSeatedForwardBendModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_softness
                            ( userAuthorizedId,  chairSeatedForwardBendOne,  chairSeatedForwardBendTwo,  chairSeatedForwardBendThree,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @chairSeatedForwardBendOne, @chairSeatedForwardBendTwo, @chairSeatedForwardBendThree, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_softness
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                chairSeatedForwardBendOne = obj.chairSeatedForwardBendOne,
                chairSeatedForwardBendTwo = obj.chairSeatedForwardBendTwo,
                chairSeatedForwardBendThree = obj.chairSeatedForwardBendThree,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-柔軟度 椅子坐姿體前彎與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-柔軟度 椅子坐姿體前彎與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessSoftnessChairSeatedForwardBendAndMark(int recordId, int studentId, string today, RequestPysicalFitnessSoftnessChairSeatedForwardBendModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_softness
                               SET chairSeatedForwardBendOne   = @chairSeatedForwardBendOne,
                                   chairSeatedForwardBendTwo   = @chairSeatedForwardBendTwo,
                                   chairSeatedForwardBendThree = @chairSeatedForwardBendThree,
                                   updateRecordId              = @updateRecordId,
                                   updateRecordTime            = @updateRecordTime
                             WHERE     userAuthorizedId        = @userAuthorizedId
                                   AND updateRecordDate        = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_softness
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                chairSeatedForwardBendOne = obj.chairSeatedForwardBendOne,
                chairSeatedForwardBendTwo = obj.chairSeatedForwardBendTwo,
                chairSeatedForwardBendThree = obj.chairSeatedForwardBendThree,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增學員 體適能-柔軟度 拉貝測驗與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-柔軟度 拉貝測驗與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessSoftnessPullBackTestAndMark(int recordId, int studentId, RequestPysicalFitnessSoftnessPullBackModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_softness
                            ( userAuthorizedId,  pullBackTestOne,  pullBackTestTwo,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @pullBackTestOne, @pullBackTestTwo, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_softness
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                pullBackTestOne = obj.pullBackTestOne,
                pullBackTestTwo = obj.pullBackTestTwo,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-柔軟度 拉貝測驗與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-柔軟度 拉背測驗與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessSoftnessPullBackTestAndMark(int recordId, int studentId, string today, RequestPysicalFitnessSoftnessPullBackModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_softness
                               SET pullBackTestOne      = @pullBackTestOne,
                                   pullBackTestTwo      = @pullBackTestTwo,
                                   updateRecordId       = @updateRecordId,
                                   updateRecordTime     = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_softness
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                pullBackTestOne = obj.pullBackTestOne,
                pullBackTestTwo = obj.pullBackTestTwo,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 檢查最新 體適能-平衡資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public T InspactTodayPhysicalFitnessBalance<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               physical_fitness_balance.adultEyeOpeningMonopodSecond,
                               physical_fitness_balance.elderlyEyeOpeningMonopodSecond,
                               physical_fitness_balance.sittingAroundOneSecond,
                               physical_fitness_balance.sittingAroundTwoSecond,
                               physical_fitness_balance.newRecordId,
                               physical_fitness_balance.newRecordDate,
                               physical_fitness_balance.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (physical_fitness_balance)
                            ON (user_authorized.userAuthorizedId = physical_fitness_balance.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId          = @userAuthorizedId
                               AND physical_fitness_balance.userAuthorizedId = @userAuthorizedId
                               AND physical_fitness_balance.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增學員 體適能-平衡 壯年單眼開足立與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-平衡 壯年單眼開足立與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondAndMark(int recordId, int studentId, RequestPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_balance
                            ( userAuthorizedId,  adultEyeOpeningMonopodSecond,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @adultEyeOpeningMonopodSecond, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_balance
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                adultEyeOpeningMonopodSecond = obj.adultEyeOpeningMonopodSecond,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-平衡 壯年單眼開足立與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-平衡 壯年單眼開足立與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondAndMark(int recordId, int studentId, string today, RequestPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_balance
                               SET adultEyeOpeningMonopodSecond = @adultEyeOpeningMonopodSecond,
                                   updateRecordId               = @updateRecordId,
                                   updateRecordTime             = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_balance
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                adultEyeOpeningMonopodSecond = obj.adultEyeOpeningMonopodSecond,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增學員 體適能-平衡 老年單眼開足立與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-平衡 老年單眼開足立與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondAndMark(int recordId, int studentId, RequestPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_balance
                            ( userAuthorizedId,  elderlyEyeOpeningMonopodSecond,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @elderlyEyeOpeningMonopodSecond, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_balance
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                elderlyEyeOpeningMonopodSecond = obj.elderlyEyeOpeningMonopodSecond,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 新增學員 體適能-平衡 老年單眼開足立與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-平衡 老年單眼開足立與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondAndMark(int recordId, int studentId, string today, RequestPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_balance
                               SET elderlyEyeOpeningMonopodSecond = @elderlyEyeOpeningMonopodSecond,
                                   updateRecordId                 = @updateRecordId,
                                   updateRecordTime               = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_balance
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                elderlyEyeOpeningMonopodSecond = obj.elderlyEyeOpeningMonopodSecond,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);
            
            return updateRow;
        }

        /// <summary>
        /// 新增學員 體適能-平衡 坐立繞物第一次秒數與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-平衡 坐立繞物第一次秒數與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessBalanceSittingAroundOneSecondAndMark(int recordId, int studentId, RequestPhysicalFitnessBalanceSittingAroundOneSecondModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_balance
                            ( userAuthorizedId,  sittingAroundOneSecond,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @sittingAroundOneSecond, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_balance
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sittingAroundOneSecond = obj.sittingAroundOneSecond,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-平衡 坐立繞物第一次秒數與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-平衡 坐立繞物第一次秒數與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessBalanceSittingAroundOneSecondAndMark(int recordId, int studentId, string today, RequestPhysicalFitnessBalanceSittingAroundOneSecondModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_balance
                               SET sittingAroundOneSecond = @sittingAroundOneSecond,
                                   updateRecordId         = @updateRecordId,
                                   updateRecordTime       = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_balance
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sittingAroundOneSecond = obj.sittingAroundOneSecond,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增學員 體適能-平衡 坐立繞物第二次秒數與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-平衡 坐立繞物第二次秒數與備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessBalanceSittingAroundTwoSecondAndMark(int recordId, int studentId, RequestPhysicalFitnessBalanceSittingAroundTwoSecondModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO physical_fitness_balance
                            ( userAuthorizedId,  sittingAroundTwoSecond,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @sittingAroundTwoSecond, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO physical_fitness_balance
                            ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                            (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sittingAroundTwoSecond = obj.sittingAroundTwoSecond,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-平衡 坐立繞物第二次秒數與備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-平衡 坐立繞物第二次秒數與備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessBalanceSittingAroundTwoSecondAndMark(int recordId, int studentId, string today, RequestPhysicalFitnessBalanceSittingAroundTwoSecondModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE physical_fitness_balance
                               SET sittingAroundTwoSecond = @sittingAroundTwoSecond,
                                   updateRecordId         = @updateRecordId,
                                   updateRecordTime       = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE physical_fitness_balance
                               SET mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                sittingAroundTwoSecond = obj.sittingAroundTwoSecond,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增學員 體適能-心肺適能 備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-心肺適能 備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessCardiorespiratoryFitness(int recordId, int studentId, RequestPhysicalFitnessCardiorespiratoryFitnessMarkModels obj)
        {
            sqlStr = $@"INSERT INTO physical_fitness_cardiorespiratory_fitness
                        ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                        (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-心肺適能 備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-心肺適能 備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessCardiorespiratoryFitness(int recordId, int studentId, string today, RequestPhysicalFitnessCardiorespiratoryFitnessMarkModels obj)
        {
            sqlStr = $@"UPDATE physical_fitness_cardiorespiratory_fitness
                           SET mark             = @mark,
                               updateRecordId   = @updateRecordId,
                               updateRecordTime = @updateRecordTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND updateRecordDate = @updateRecordDate
            ";

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                mark = obj.mark,
                updateRecordId = recordId,
                userAuthorizedId = studentId,
                updateRecordDate = today,
                updateRecordTime = nowTime,
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增學員 體適能-肌力與肌耐力 備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-肌力與肌耐力 備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessMuscleStrengthAndMuscleEnduranceMark(int recordId, int studentId, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkModels obj)
        {
            sqlStr = $@"INSERT INTO physical_fitness_muscle_strength_and_muscle_endurance
                        ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                        (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-肌力與肌耐力 備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-肌力與肌耐力 備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessMuscleStrengthAndMuscleEnduranceMark(int recordId, int studentId, string today, RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkModels obj)
        {
            sqlStr = $@"UPDATE physical_fitness_muscle_strength_and_muscle_endurance
                           SET mark             = @mark,
                               updateRecordId   = @updateRecordId,
                               updateRecordTime = @updateRecordTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND updateRecordDate = @updateRecordDate
            ";

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                mark = obj.mark,
                updateRecordId = recordId,
                userAuthorizedId = studentId,
                updateRecordDate = today,
                updateRecordTime = nowTime,
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增學員 體適能-柔軟度 備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-柔軟度 備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessSoftnessMark(int recordId, int studentId, RequestPhysicalFitnessSoftnessMarkModels obj)
        {
            sqlStr = $@"INSERT INTO physical_fitness_softness
                        ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                        (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-柔軟度 備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-柔軟度 備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessSoftnessMark(int recordId, int studentId, string today, RequestPhysicalFitnessSoftnessMarkModels obj)
        {
            sqlStr = $@"UPDATE physical_fitness_softness
                           SET mark             = @mark,
                               updateRecordId   = @updateRecordId,
                               updateRecordTime = @updateRecordTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND updateRecordDate = @updateRecordDate
            ";

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                mark = obj.mark,
                updateRecordId = recordId,
                userAuthorizedId = studentId,
                updateRecordDate = today,
                updateRecordTime = nowTime,
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增學員 體適能-平衡 備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 體適能-平衡 備註模型</param>
        /// <returns></returns>
        public int InsertPhysicalFitnessBalanceMark(int recordId, int studentId, RequestPhysicalFitnessBalanceMarkModels obj)
        {
            sqlStr = $@"INSERT INTO physical_fitness_balance
                        ( userAuthorizedId,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                        (@userAuthorizedId, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體適能-平衡 備註資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 體適能-平衡 備註模型</param>
        /// <returns></returns>
        public int UpdatePhysicalFitnessBalanceMark(int recordId, int studentId, string today, RequestPhysicalFitnessBalanceMarkModels obj)
        {
            sqlStr = $@"UPDATE physical_fitness_balance
                           SET mark             = @mark,
                               updateRecordId   = @updateRecordId,
                               updateRecordTime = @updateRecordTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND updateRecordDate = @updateRecordDate
            ";

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                mark = obj.mark,
                updateRecordId = recordId,
                userAuthorizedId = studentId,
                updateRecordDate = today,
                updateRecordTime = nowTime,
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }
    }
}

/// <summary>
/// 接收 新增/更新 體適能-心肺適能(登階測驗) 模型
/// </summary>
public class RequestPhysicalFitnessCardiorespiratoryFitnessStepTestModels
{
    /// <summary>
    /// 第一次脈搏次數
    /// </summary>
    [Required]
    [RegularExpression("^[0-9]+$")]
    public int pulseRateOne { get; set; } = 0;

    /// <summary>
    /// 第二次脈搏次數
    /// </summary>
    [Required]
    [RegularExpression("^[0-9]+$")]
    public int pulseRateTwo { get; set; } = 0;

    /// <summary>
    /// 第三次脈搏次數
    /// </summary>
    [Required]
    [RegularExpression("^[0-9]+$")]
    public int pulseRateThree { get; set; } = 0;

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-心肺適能(原地站立抬膝) 模型
/// </summary>
public class RequestPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyModels
{
    /// <summary>
    /// 原地站立抬膝
    /// </summary>
    [Required]
    [RegularExpression("^[0-9]+$")]
    public int kneeLiftFrequency { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-心肺適能(屈膝仰臥起坐) 模型
/// </summary>
public class RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyModels
{
    /// <summary>
    /// 屈膝仰臥起坐
    /// </summary>
    [Required]
    [RegularExpression("^[0-9]+$")]
    public int kneeCrunchesFrequency { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-肌力與肌耐力 手臂彎舉(上肢) 模型
/// </summary>
public class RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyModels
{
    /// <summary>
    /// 手臂彎舉(上肢)
    /// </summary>
    [Required]
    [RegularExpression("^[0-9]+$")]
    public int armCurlUpperBodyFrequency { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-肌力與肌耐力 手臂彎舉(下肢) 模型
/// </summary>
public class RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyModels
{
    /// <summary>
    /// 手臂彎舉(下肢)
    /// </summary>
    [Required]
    [RegularExpression("^[0-9]+$")]
    public int armCurlLowerBodyFrequency { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-柔軟度 坐姿體前彎模型
/// </summary>
public class RequestPysicalFitnessSoftnessSeatedForwardBendModels
{
    /// <summary>
    /// 坐姿體前彎第一次距離
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double seatedForwardBendOne { get; set; }

    /// <summary>
    /// 坐姿體前彎第二次距離
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double seatedForwardBendTwo { get; set; }

    /// <summary>
    /// 坐姿體前彎第三次距離
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double seatedForwardBendThree { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-柔軟度 椅子坐姿體前彎模型
/// </summary>
public class RequestPysicalFitnessSoftnessChairSeatedForwardBendModels
{
    /// <summary>
    /// 椅子坐姿體前彎第一次量距離
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double chairSeatedForwardBendOne { get; set; }

    /// <summary>
    /// 椅子坐姿體前彎第二次量距離
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double chairSeatedForwardBendTwo { get; set; }

    /// <summary>
    /// 椅子坐姿體前彎第三次量距離
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double chairSeatedForwardBendThree { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-柔軟度 拉背測驗模型
/// </summary>
public class RequestPysicalFitnessSoftnessPullBackModels
{
    /// <summary>
    /// 拉背測驗第一次距離
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double pullBackTestOne { get; set; }

    /// <summary>
    /// 拉背測驗第二次距離
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double pullBackTestTwo { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-平衡 壯年開眼單足立模型
/// </summary>
public class RequestPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondModels
{
    /// <summary>
    /// 壯年開眼單足立(秒數)
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double adultEyeOpeningMonopodSecond { get; set; } = 0.0;

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-平衡 老年開眼單足立模型
/// </summary>
public class RequestPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondModels
{
    /// <summary>
    /// 老年開眼單足立
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double elderlyEyeOpeningMonopodSecond { get; set; } = 0.0;

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-平衡 坐立繞物，第一次測試秒數模型
/// </summary>
public class RequestPhysicalFitnessBalanceSittingAroundOneSecondModels
{
    /// <summary>
    /// 坐立繞物，第一次秒數
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double sittingAroundOneSecond { get; set; }
    
    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-平衡 坐立繞物，第二次測試秒數模型
/// </summary>
public class RequestPhysicalFitnessBalanceSittingAroundTwoSecondModels
{
    /// <summary>
    /// 坐立繞物，第二次秒數
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double sittingAroundTwoSecond { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 新增/更新 體適能-心肺適能 備註
/// </summary>
public class RequestPhysicalFitnessCardiorespiratoryFitnessMarkModels
{
    [MaxLength(150)]
    public string mark { get; set; } = "";
}

/// <summary>
/// 接收 新增/更新 體適能-肌力與肌耐力 備註
/// </summary>
public class RequestPhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkModels
{
    [MaxLength(150)]
    public string mark { get; set; } = "";
}

/// <summary>
/// 接收 新增/更新 體適能-柔軟度 備註
/// </summary>
public class RequestPhysicalFitnessSoftnessMarkModels
{
    [MaxLength(150)]
    public string mark { get; set; } = "";
}

/// <summary>
/// 接收 新增/更新 體適能-平衡 備註
/// </summary>
public class RequestPhysicalFitnessBalanceMarkModels
{
    [MaxLength(150)]
    public string mark { get; set; } = "";
}