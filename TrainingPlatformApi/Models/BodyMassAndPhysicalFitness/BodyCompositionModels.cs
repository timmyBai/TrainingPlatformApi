using MySql.Data.MySqlClient;
using Dapper;
using System.ComponentModel.DataAnnotations;

using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;

namespace TrainingPlatformApi.Models.BodyMassAndPhysicalFitness
{
    public class BodyCompositionModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public BodyCompositionModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查今日 bmi 是否存在
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <returns></returns>
        public T InspactTodayBodyCompositionBmiRecord<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               body_composition_bmi.bmi,
                               body_composition_bmi.mark,
                               body_composition_bmi.newRecordId,
                               body_composition_bmi.newRecordDate,
                               body_composition_bmi.newRecordTime
                       FROM user_authorized
                      LEFT JOIN (body_composition_bmi)
                        ON (user_authorized.userAuthorizedId = body_composition_bmi.userAuthorizedId)
                     WHERE     user_authorized.userAuthorizedId  = @userAuthorizedId
                           AND body_composition_bmi.userAuthorizedId = @userAuthorizedId
                           AND body_composition_bmi.newRecordDate    = @newRecordDate
                     ORDER BY body_composition_bmi.newRecordDate DESC,
                              body_composition_bmi.newRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增 bmi 及 備註 資料
        /// </summary>
        /// <param name="recordId">紀錄人員</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">bmi 與 備註模型</param>
        /// <returns></returns>
        public int InsertBodyCompositionBmiRecord(int recordId, int studentId, RequestBodyCompositionBmiModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO body_composition_bmi ( userAuthorizedId,  bmi,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                     (@userAuthorizedId, @bmi, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO body_composition_bmi ( userAuthorizedId,  bmi,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                 (@userAuthorizedId, @bmi, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                                                 
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                bmi = obj.bmi,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 bmi 及 備註 資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <param name="obj">bmi 與 備註模型</param>
        /// <returns></returns>
        public int UpdateBodyCompositionBmiRecord(int recordId, int studentId, string today, RequestBodyCompositionBmiModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE body_composition_bmi
                               SET bmi              = @bmi,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE body_composition_bmi
                               SET bmi              = @bmi,
                                   mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                bmi = obj.bmi,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 檢查今日 體脂肪 是否存在
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <returns></returns>
        public T InspactTodayBodyCompositionBodyFatRecord<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               body_composition_bodyFat.bodyFat,
                               body_composition_bodyFat.mark,
                               body_composition_bodyFat.newRecordId,
                               body_composition_bodyFat.newRecordDate,
                               body_composition_bodyFat.newRecordTime
                       FROM user_authorized
                      LEFT JOIN (body_composition_bodyFat)
                        ON (user_authorized.userAuthorizedId = body_composition_bodyFat.userAuthorizedId)
                     WHERE     user_authorized.userAuthorizedId  = @userAuthorizedId
                           AND body_composition_bodyFat.userAuthorizedId = @userAuthorizedId
                           AND body_composition_bodyFat.newRecordDate    = @newRecordDate
                     ORDER BY body_composition_bodyFat.newRecordDate DESC,
                              body_composition_bodyFat.newRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增 體脂肪 及 備註 資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">體脂肪 與 備註模型</param>
        /// <returns></returns>
        public int InsertBodyCompositionBodyFatRecord(int recordId, int studentId, RequestBodyCompositionBodyFatModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO body_composition_bodyFat ( userAuthorizedId,  bodyFat,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                         (@userAuthorizedId, @bodyFat, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO body_composition_bodyFat ( userAuthorizedId,  bodyFat,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                         (@userAuthorizedId, @bodyFat, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                bodyFat = obj.bodyFat,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 體脂肪 及 備註 資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <param name="obj">體脂肪 與 備註模型</param>
        /// <returns></returns>
        public int UpdateBodyCompositionBodyFatRecord(int recordId, int studentId, string today, RequestBodyCompositionBodyFatModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE body_composition_bodyFat
                               SET bodyFat          = @bodyFat,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE body_composition_bodyFat
                               SET bodyFat          = @bodyFat,
                                   mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                bodyFat = obj.bodyFat,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 檢查今日 內臟脂肪 是否存在
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <returns></returns>
        public T InspactTodayBodyCompositionVisceralFatRecord<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               body_composition_visceralFat.visceralFat,
                               body_composition_visceralFat.mark,
                               body_composition_visceralFat.newRecordId,
                               body_composition_visceralFat.newRecordDate,
                               body_composition_visceralFat.newRecordTime
                       FROM user_authorized
                      LEFT JOIN (body_composition_visceralFat)
                        ON (user_authorized.userAuthorizedId = body_composition_visceralFat.userAuthorizedId)
                     WHERE     user_authorized.userAuthorizedId  = @userAuthorizedId
                           AND body_composition_visceralFat.userAuthorizedId = @userAuthorizedId
                           AND body_composition_visceralFat.newRecordDate    = @newRecordDate
                     ORDER BY body_composition_visceralFat.newRecordDate DESC,
                              body_composition_visceralFat.newRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增 內臟脂肪 及 備註 資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">內臟脂肪 與 備註模型</param>
        /// <returns></returns>
        public int InsertBodyCompositionVisceralFatRecord(int recordId, int studentId, RequestBodyCompositionVisceralFatModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO body_composition_visceralFat ( userAuthorizedId,  visceralFat,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                             (@userAuthorizedId, @visceralFat, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO body_composition_visceralFat ( userAuthorizedId,  visceralFat,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                             (@userAuthorizedId, @visceralFat, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                visceralFat = obj.visceralFat,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 內臟脂肪 及 備註 資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <param name="obj">內臟脂肪 與 備註模型</param>
        /// <returns></returns>
        public int UpdateBodyCompositionVisceralFatRecord(int recordId, int studentId, string today, RequestBodyCompositionVisceralFatModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE body_composition_visceralFat
                               SET visceralFat      = @visceralFat,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE body_composition_visceralFat
                               SET visceralFat      = @visceralFat,
                                   mark             = @mark,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordTime = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                visceralFat = obj.visceralFat,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }
        
        /// <summary>
        /// 檢查今日 骨骼心肌率 是否存在
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <returns></returns>
        public T InspactTodayBodyCompositionSkeletalMuscleRateRecord<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               body_composition_skeletalMuscleRate.skeletalMuscleRate,
                               body_composition_skeletalMuscleRate.mark,
                               body_composition_skeletalMuscleRate.newRecordId,
                               body_composition_skeletalMuscleRate.newRecordDate,
                               body_composition_skeletalMuscleRate.newRecordTime
                       FROM user_authorized
                      LEFT JOIN (body_composition_skeletalMuscleRate)
                        ON (user_authorized.userAuthorizedId = body_composition_skeletalMuscleRate.userAuthorizedId)
                     WHERE     user_authorized.userAuthorizedId  = @userAuthorizedId
                           AND body_composition_skeletalMuscleRate.userAuthorizedId = @userAuthorizedId
                           AND body_composition_skeletalMuscleRate.newRecordDate    = @newRecordDate
                     ORDER BY body_composition_skeletalMuscleRate.newRecordDate DESC,
                              body_composition_skeletalMuscleRate.newRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增學員 骨骼心肌率 及 備註 資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 更新 骨骼心肌率 與 備註模型</param>
        /// <returns></returns>
        public int InsertBodyCompositionSkeletalMuscleRateRecord(int recordId, int studentId, RequestBodyCompositionSkeletalMuscleRateModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"INSERT INTO body_composition_skeletalMuscleRate ( userAuthorizedId,  skeletalMuscleRate,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                                    (@userAuthorizedId, @skeletalMuscleRate, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }
            else
            {
                sqlStr = $@"INSERT INTO body_composition_skeletalMuscleRate ( userAuthorizedId,  skeletalMuscleRate,  mark,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                                                                                                    (@userAuthorizedId, @skeletalMuscleRate, @mark, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
                ";
            }

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                skeletalMuscleRate = obj.skeletalMuscleRate,
                mark = obj.mark,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 骨骼心肌率 及 備註 資料
        /// </summary>
        /// <param name="recordId">紀錄人員 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="firstObj">身體組成最新一筆資料</param>
        /// <param name="obj">接收 更新 骨骼心肌率 與 備註模型</param>
        /// <returns></returns>
        public int UpdateBodyCompositionSkeletalMuscleRateRecord(int recordId, int studentId, string today, RequestBodyCompositionSkeletalMuscleRateModels obj)
        {
            if (obj.mark == "null")
            {
                sqlStr = $@"UPDATE body_composition_skeletalMuscleRate
                               SET skeletalMuscleRate = @skeletalMuscleRate,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            else
            {
                sqlStr = $@"UPDATE body_composition_skeletalMuscleRate
                               SET skeletalMuscleRate = @skeletalMuscleRate,
                                   mark               = @mark,
                                   updateRecordId     = @updateRecordId,
                                   updateRecordTime   = @updateRecordTime
                             WHERE     userAuthorizedId = @userAuthorizedId
                                   AND updateRecordDate = @updateRecordDate
                ";
            }
            
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                skeletalMuscleRate = obj.skeletalMuscleRate,
                mark = obj.mark,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }
    }
}

/// <summary>
/// 接收 bmi 與 備註模型
/// </summary>
public class RequestBodyCompositionBmiModels
{
    /// <summary>
    /// bmi 指數
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double bmi { get; set; } = 0.0;

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 體脂肪 與 備註模型
/// </summary>
public class RequestBodyCompositionBodyFatModels
{
    /// <summary>
    /// 體脂肪 指數
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double bodyFat { get; set; } = 0.0;

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 更新 內臟脂肪 與 備註模型
/// </summary>
public class RequestBodyCompositionVisceralFatModels
{
    /// <summary>
    /// 內臟脂肪 指數
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double visceralFat { get; set; } = 0.0;

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}

/// <summary>
/// 接收 更新 骨骼心肌率 與 備註模型
/// </summary>
public class RequestBodyCompositionSkeletalMuscleRateModels
{
    /// <summary>
    /// 骨骼心肌率 指數
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double skeletalMuscleRate { get; set; } = 0.0;

    /// <summary>
    /// 備註
    /// </summary>
    [MaxLength(150)]
    public string mark { get; set; } = "null";
}