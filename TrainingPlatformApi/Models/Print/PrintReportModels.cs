using iTextSharp.text;
using iTextSharp.text.pdf;
using TrainingPlatformApi.VM.Student;
using TrainingPlatformApi.VM.BodyMassAndPhysicalFitness;
using TrainingPlatformApi.VM.ExercisePrescription;
using Newtonsoft.Json;
using TrainingPlatformApi.VM.InstrumentSettings;

namespace TrainingPlatformApi.Models.Print
{
    public class PrintReportModels
    {
        private Document? document;
        private Font? font;
        private BaseFont? baseFont;
        private RequestPrintTrainingReportModels? requestPrintTrainingReportModels;

        public PrintReportModels(RequestPrintTrainingReportModels _requestPrintTrainingReportModels)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            string dir = Path.Combine(Path.GetDirectoryName(Environment.CurrentDirectory), "Font");

            // 註冊字型
            FontFactory.RegisterDirectory(dir, true);

            var builder = WebApplication.CreateBuilder();

            var app = builder.Build();

            if (app.Environment.IsDevelopment())
            {
                baseFont = BaseFont.CreateFont($"{AppDomain.CurrentDomain.BaseDirectory}Fonts/新細明體.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            }
            else if (app.Environment.IsStaging())
            {
                baseFont = BaseFont.CreateFont($"{AppDomain.CurrentDomain.BaseDirectory}Fonts/新細明體.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            }
            else
            {
                baseFont = BaseFont.CreateFont($"{AppDomain.CurrentDomain.BaseDirectory}Fonts/新細明體.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            }

            requestPrintTrainingReportModels = _requestPrintTrainingReportModels;
        }

        public byte[] PrintTrainingReportPdfInfo(LatestStudentInfoVM studentInfo, IEnumerable<BodyMassBmiVM> bodyMassBmiList, IEnumerable<BodyMassBodyFatVM> bodMassBodyFatList, IEnumerable<BodyMassVisceralFatVM> bodyMassVisceralFat, IEnumerable<BodyMassSkeletalMuscleRateVM> bodyMassSkeletalMuscleRateList, IEnumerable<PhysicalFitnessCardiorespiratoryFitnessHistoryVM> physicalFitnessCardiorespiratoryFitnessList, IEnumerable<PhysicalFitnessMuscleStrengthAndMuscleEnduranceHistoryVM> physicalFitnessMuscleStrengthAndMuscleEnduranceList, IEnumerable<PhysicalFitnessSoftnessHistoryVM> physicalFitnessSoftnessList, IEnumerable<PhysicalFitnessBalanceHistoryVM> physicalFitnessBalance, LatestExercisePrescriptionListVM latestExercisePrescriptionTrainingStage, IEnumerable<ExercisePrescriptionCardiorespiratoryFitnessHistoryVM> exercisePrescriptionCardiorespiratoryFitnessList, IEnumerable<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceHistoryVM> exercisePrescriptionMuscleStrengthAndMuscleEnduranceList, IEnumerable<ExercisePrescriptionSoftnessHistoryVM> exercisePrescriptionSoftnessList, IEnumerable<ExercisePrescriptionBalanceHistoryVM> exercisePrescriptionBalanceList, IEnumerable<InstrumentSettingSportsPerformanceListVM> instrumentSettingSportList)
        {
            MemoryStream ms = new MemoryStream();

            document = new Document(PageSize.A4, 0f, 0f, 0f, 0f);
            document.SetPageSize(PageSize.A4);
            document.SetMargins(20f, 20f, 20f, 20f);

            PdfWriter.GetInstance(document, ms);

            document.Open();


            // 報表標題
            this.PrintReportTitle("等速肌力訓練報表");
            this.NewLine();

            // 學員資訊
            this.PrintStudentBaseInfo(studentInfo);
            this.NewLine();

            // 身體組成與體適能評價
            if (requestPrintTrainingReportModels.isPrintBodyCompositionAndPhysicalFitness)
            {
                this.PrintBodyCompositionAndPhysicalFitness(bodyMassBmiList, bodMassBodyFatList, bodyMassVisceralFat, bodyMassSkeletalMuscleRateList, physicalFitnessCardiorespiratoryFitnessList, physicalFitnessMuscleStrengthAndMuscleEnduranceList, physicalFitnessSoftnessList, physicalFitnessBalance);
                this.NewLine();
            }

            // 運動處方
            if (requestPrintTrainingReportModels.isPrintExercisePrescription)
            {
                this.PrintExercisePrescription(latestExercisePrescriptionTrainingStage, exercisePrescriptionCardiorespiratoryFitnessList, exercisePrescriptionMuscleStrengthAndMuscleEnduranceList, exercisePrescriptionSoftnessList, exercisePrescriptionBalanceList);
                this.NewLine();
            }

            // 儀器設定
            if (requestPrintTrainingReportModels.isPrintInstrumentSetting)
            {
                this.PrintInstrumentSettings(instrumentSettingSportList);
                this.NewLine();
            }

            // 檢視報表-標題
            if (requestPrintTrainingReportModels.printInspectionReportCardiorespiratoryFitness.Length > 0 || requestPrintTrainingReportModels.printInspectionReportMachinaryOne.Length > 0 || requestPrintTrainingReportModels.printInspectionReportMachinaryTwo.Length > 0
                || requestPrintTrainingReportModels.printInspectionReportMachinaryThree.Length > 0 || requestPrintTrainingReportModels.printInspectionReportMachinaryFour.Length > 0 || requestPrintTrainingReportModels.printInspectionReportMachinaryFive.Length > 0
                || requestPrintTrainingReportModels.printInspectionReportMachinarySix.Length > 0 || requestPrintTrainingReportModels.printInspectionReportMachinarySeven.Length > 0 || requestPrintTrainingReportModels.printInspectionReportMachinaryEight.Length > 0
                || requestPrintTrainingReportModels.printInspectionReportMachinaryNine.Length > 0 || requestPrintTrainingReportModels.printInspectionReportMachinaryTen.Length > 0 || requestPrintTrainingReportModels.printInspectionReportSoftness.Length > 0
                || requestPrintTrainingReportModels.printInspectionReportBalance.Length > 0)
            {
                this.PrintInspectionReportTitle();
            }

            // 檢視報表-心肺適能
            if (requestPrintTrainingReportModels.printInspectionReportCardiorespiratoryFitness.Length > 0)
            {
                this.PrintInspectionReportCardiorespiratoryFitness(requestPrintTrainingReportModels.printInspectionReportCardiorespiratoryFitness);
                this.NewLine();
            }

            // 機器訓練一
            if (requestPrintTrainingReportModels.printInspectionReportMachinaryOne.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeOneTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinaryOne);
                this.NewLine();
            }

            // 機器訓練二
            if (requestPrintTrainingReportModels.printInspectionReportMachinaryTwo.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeTwoTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinaryTwo);
                this.NewLine();
            }

            // 機器訓練三
            if (requestPrintTrainingReportModels.printInspectionReportMachinaryThree.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeThreeTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinaryThree);
                this.NewLine();
            }

            // 機器訓練四
            if (requestPrintTrainingReportModels.printInspectionReportMachinaryFour.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeFourTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinaryFour);
                this.NewLine();
            }

            // 機器訓練五
            if (requestPrintTrainingReportModels.printInspectionReportMachinaryFive.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeFiveTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinaryFive);
                this.NewLine();
            }

            // 機器訓練六
            if (requestPrintTrainingReportModels.printInspectionReportMachinarySix.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeSixTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinarySix);
                this.NewLine();
            }

            // 機器訓練七
            if (requestPrintTrainingReportModels.printInspectionReportMachinarySeven.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeSevenTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinarySeven);
                this.NewLine();
            }

            // 機器訓練八
            if (requestPrintTrainingReportModels.printInspectionReportMachinaryEight.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeEightTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinaryEight);
                this.NewLine();
            }

            // 機器訓練九
            if (requestPrintTrainingReportModels.printInspectionReportMachinaryNine.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeNineTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinaryNine);
                this.NewLine();
            }

            // 機器訓練十
            if (requestPrintTrainingReportModels.printInspectionReportMachinaryTen.Length > 0)
            {
                this.PrintInspectionReportMachinaryCodeTenTrainingStatus(requestPrintTrainingReportModels.printInspectionReportMachinaryTen);
                this.NewLine();
            }

            // 檢視報表-柔軟度
            if (requestPrintTrainingReportModels.printInspectionReportSoftness.Length > 0)
            {
                this.PrintInspectionReportSoftness(requestPrintTrainingReportModels.printInspectionReportSoftness);
                this.NewLine();
            }

            // 檢視報表-平衡
            if (requestPrintTrainingReportModels.printInspectionReportBalance.Length > 0)
            {
                this.PrintInspectionReportBalance(requestPrintTrainingReportModels.printInspectionReportBalance);
                this.NewLine();
            }

            document.Close();

            return ms.ToArray();
        }
        
        /// <summary>
        /// 列印報表標題
        /// </summary>
        /// <param name="title"></param>
        private void PrintReportTitle(string title)
        {
            iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 24f, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            Paragraph paragraphReportTitle = new Paragraph(new Phrase(title, font));
            paragraphReportTitle.Alignment = Element.ALIGN_CENTER;
            document.Add(paragraphReportTitle);
        }

        /// <summary>
        /// 學員資本資料
        /// </summary>
        /// <param name="studentInfo"></param>
        private void PrintStudentBaseInfo(LatestStudentInfoVM studentInfo)
        {
            #region 使用者基本資料標題
            font = new iTextSharp.text.Font(baseFont, 12f, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            Paragraph userBaseInfoTitle = new Paragraph(new Phrase("使用者基本資料", font));
            userBaseInfoTitle.SpacingAfter = 10;
            userBaseInfoTitle.Alignment = Element.ALIGN_CENTER;
            document.Add(userBaseInfoTitle);
            #endregion

            #region 使用者基本資料表格
            PdfPTable userBaseInfoTable = new PdfPTable(6);
            PdfPCell userBaseInfoCell = new PdfPCell(new Phrase(studentInfo.name, font));
            userBaseInfoCell.HorizontalAlignment = Element.ALIGN_LEFT;
            userBaseInfoCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            userBaseInfoCell.BackgroundColor = BaseColor.WHITE;
            userBaseInfoCell.Padding = 5;
            userBaseInfoCell.ExtraParagraphSpace = 5;
            userBaseInfoTable.AddCell(userBaseInfoCell);

            userBaseInfoCell = new PdfPCell(new Phrase(studentInfo.gender, font));
            userBaseInfoCell.HorizontalAlignment = Element.ALIGN_LEFT;
            userBaseInfoCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            userBaseInfoCell.BackgroundColor = BaseColor.WHITE;
            userBaseInfoCell.Padding = 5;
            userBaseInfoCell.ExtraParagraphSpace = 5;
            userBaseInfoTable.AddCell(userBaseInfoCell);

            userBaseInfoCell = new PdfPCell(new Phrase($"{studentInfo.age} 歲", font));
            userBaseInfoCell.HorizontalAlignment = Element.ALIGN_LEFT;
            userBaseInfoCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            userBaseInfoCell.BackgroundColor = BaseColor.WHITE;
            userBaseInfoCell.Padding = 5;
            userBaseInfoCell.ExtraParagraphSpace = 5;
            userBaseInfoTable.AddCell(userBaseInfoCell);

            userBaseInfoCell = new PdfPCell(new Phrase(studentInfo.posture, font));
            userBaseInfoCell.HorizontalAlignment = Element.ALIGN_LEFT;
            userBaseInfoCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            userBaseInfoCell.BackgroundColor = BaseColor.WHITE;
            userBaseInfoCell.Padding = 5;
            userBaseInfoCell.ExtraParagraphSpace = 5;
            userBaseInfoTable.AddCell(userBaseInfoCell);

            userBaseInfoCell = new PdfPCell(new Phrase(studentInfo.dangerGrading, font));
            userBaseInfoCell.HorizontalAlignment = Element.ALIGN_LEFT;
            userBaseInfoCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            userBaseInfoCell.BackgroundColor = BaseColor.WHITE;
            userBaseInfoCell.Padding = 5;
            userBaseInfoCell.ExtraParagraphSpace = 5;
            userBaseInfoTable.AddCell(userBaseInfoCell);

            userBaseInfoCell = new PdfPCell(new Phrase(studentInfo.medicalHistory, font));
            userBaseInfoCell.HorizontalAlignment = Element.ALIGN_LEFT;
            userBaseInfoCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            userBaseInfoCell.BackgroundColor = BaseColor.WHITE;
            userBaseInfoCell.Padding = 5;
            userBaseInfoCell.ExtraParagraphSpace = 5;
            userBaseInfoTable.AddCell(userBaseInfoCell);

            userBaseInfoCell = new PdfPCell(new Phrase($"{studentInfo.phone.Substring(0, 4)} {studentInfo.phone.Substring(4, 3)} {studentInfo.phone.Substring(7, 3)}", font));
            userBaseInfoCell.HorizontalAlignment = Element.ALIGN_LEFT;
            userBaseInfoCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            userBaseInfoCell.BackgroundColor = BaseColor.WHITE;
            userBaseInfoCell.Padding = 5;
            userBaseInfoCell.ExtraParagraphSpace = 5;
            userBaseInfoCell.Colspan = 2;
            userBaseInfoTable.AddCell(userBaseInfoCell);

            userBaseInfoCell = new PdfPCell(new Phrase(studentInfo.address, font));
            userBaseInfoCell.HorizontalAlignment = Element.ALIGN_LEFT;
            userBaseInfoCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            userBaseInfoCell.BackgroundColor = BaseColor.WHITE;
            userBaseInfoCell.Padding = 5;
            userBaseInfoCell.ExtraParagraphSpace = 5;
            userBaseInfoCell.Colspan = 4;
            userBaseInfoTable.AddCell(userBaseInfoCell);

            userBaseInfoTable.CompleteRow();

            document.Add(userBaseInfoTable);
            #endregion
        }

        /// <summary>
        /// 身體組成與體適能評價
        /// </summary>
        /// <param name="bodyMassBmiList"></param>
        /// <param name="bodMassBodyFatList"></param>
        /// <param name="bodyMassVisceralFat"></param>
        /// <param name="bodyMassSkeletalMuscleRateList"></param>
        /// <param name="physicalFitnessCardiorespiratoryFitnessList"></param>
        /// <param name="physicalFitnessMuscleStrengthAndMuscleEnduranceList"></param>
        /// <param name="physicalFitnessSoftnessList"></param>
        /// <param name="physicalFitnessBalance"></param>
        private void PrintBodyCompositionAndPhysicalFitness(IEnumerable<BodyMassBmiVM> bodyMassBmiList, IEnumerable<BodyMassBodyFatVM> bodMassBodyFatList, IEnumerable<BodyMassVisceralFatVM> bodyMassVisceralFat, IEnumerable<BodyMassSkeletalMuscleRateVM> bodyMassSkeletalMuscleRateList, IEnumerable<PhysicalFitnessCardiorespiratoryFitnessHistoryVM> physicalFitnessCardiorespiratoryFitnessList, IEnumerable<PhysicalFitnessMuscleStrengthAndMuscleEnduranceHistoryVM> physicalFitnessMuscleStrengthAndMuscleEnduranceList, IEnumerable<PhysicalFitnessSoftnessHistoryVM> physicalFitnessSoftnessList, IEnumerable<PhysicalFitnessBalanceHistoryVM> physicalFitnessBalance)
        {
            #region 身體組成與體適能評價標題
            font = new iTextSharp.text.Font(baseFont, 12f, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            Paragraph bodyCompositionAndPhysicalFitnessTitle = new Paragraph(new Phrase("身體組成與體適能評價", font));
            bodyCompositionAndPhysicalFitnessTitle.SpacingAfter = 10;
            bodyCompositionAndPhysicalFitnessTitle.Alignment = Element.ALIGN_CENTER;
            document.Add(bodyCompositionAndPhysicalFitnessTitle);
            #endregion

            #region 身體組成與體適能評價表格
            PdfPTable bodyCompositionAndPhysicalFitnessTable = new PdfPTable(4);
            PdfPCell bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("項目", font));
            bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
            bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
            bodyCompositionAndPhysicalFitnessCell.Padding = 5;
            bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
            bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

            bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("目前數值", font));
            bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
            bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
            bodyCompositionAndPhysicalFitnessCell.Padding = 5;
            bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
            bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

            bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("評價等級", font));
            bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
            bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
            bodyCompositionAndPhysicalFitnessCell.Padding = 5;
            bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
            bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

            bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("備註", font));
            bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
            bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
            bodyCompositionAndPhysicalFitnessCell.Padding = 5;
            bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
            bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

            if (bodyMassBmiList.Count() == 0 && bodMassBodyFatList.Count() == 0 && bodyMassVisceralFat.Count() == 0 && bodyMassSkeletalMuscleRateList.Count() == 0 && physicalFitnessCardiorespiratoryFitnessList.Count() == 0 && physicalFitnessMuscleStrengthAndMuscleEnduranceList.Count() == 0 && physicalFitnessSoftnessList.Count() == 0 && physicalFitnessBalance.Count() == 0)
            {
                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("查無任何資料", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_CENTER;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessCell.Colspan = 4;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);
            }   

            // BMI
            if (bodyMassBmiList.Count() > 0)
            {
                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("BMI", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodyMassBmiList.ToList()[0].bmi.ToString(), font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodyMassBmiList.ToList()[0].bmiCommentLevel, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodyMassBmiList.ToList()[0].mark, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);
            }

            // 體脂肪
            if (bodMassBodyFatList.Count() > 0)
            {
                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("體脂肪", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodMassBodyFatList.ToList()[0].bodyFat.ToString(), font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodMassBodyFatList.ToList()[0].bodyFatCommentLevel, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodMassBodyFatList.ToList()[0].mark, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);
            }

            // 內臟脂肪
            if (bodyMassVisceralFat.Count() > 0)
            {
                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("內臟脂肪", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodyMassVisceralFat.ToList()[0].visceralFat.ToString(), font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodyMassVisceralFat.ToList()[0].visceralFatCommentLevel, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodyMassVisceralFat.ToList()[0].mark, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);
            }

            // 骨骼心肌率
            if (bodyMassSkeletalMuscleRateList.Count() > 0)
            {
                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("骨骼心肌率", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodyMassSkeletalMuscleRateList.ToList()[0].skeletalMuscleRate.ToString(), font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodyMassSkeletalMuscleRateList.ToList()[0].skeletalMuscleRateCommentLevel, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(bodyMassSkeletalMuscleRateList.ToList()[0].mark, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);
            }

            // 心肺適能
            if (physicalFitnessCardiorespiratoryFitnessList.Count() > 0)
            {
                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("心肺適能", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("--", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(physicalFitnessCardiorespiratoryFitnessList.ToList()[0].cardiorespiratoryFitnessCommentLevel, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(physicalFitnessCardiorespiratoryFitnessList.ToList()[0].mark, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);
            }

            // 肌力與肌耐力
            if (physicalFitnessMuscleStrengthAndMuscleEnduranceList.Count() > 0)
            {
                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("肌力與肌耐力", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("--", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[0].muscleStrengthAndMuscleEnduranceCommentLevel, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(physicalFitnessMuscleStrengthAndMuscleEnduranceList.ToList()[0].mark, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);
            }

            // 柔軟度
            if (physicalFitnessSoftnessList.Count() > 0)
            {
                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("柔軟度", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("--", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(physicalFitnessSoftnessList.ToList()[0].softnessComment, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(physicalFitnessSoftnessList.ToList()[0].mark, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);
            }

            // 平衡
            if (physicalFitnessBalance.Count() > 0)
            {
                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("平衡", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase("--", font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(physicalFitnessBalance.ToList()[0].balanceComment, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);

                bodyCompositionAndPhysicalFitnessCell = new PdfPCell(new Phrase(physicalFitnessBalance.ToList()[0].mark, font));
                bodyCompositionAndPhysicalFitnessCell.HorizontalAlignment = Element.ALIGN_LEFT;
                bodyCompositionAndPhysicalFitnessCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                bodyCompositionAndPhysicalFitnessCell.BackgroundColor = BaseColor.WHITE;
                bodyCompositionAndPhysicalFitnessCell.Padding = 5;
                bodyCompositionAndPhysicalFitnessCell.ExtraParagraphSpace = 5;
                bodyCompositionAndPhysicalFitnessTable.AddCell(bodyCompositionAndPhysicalFitnessCell);
            }

            bodyCompositionAndPhysicalFitnessTable.CompleteRow();
            document.Add(bodyCompositionAndPhysicalFitnessTable);
            #endregion
        }

        /// <summary>
        /// 運動處方
        /// </summary>
        /// <param name="latestExercisePrescriptionTrainingStage"></param>
        /// <param name="exercisePrescriptionCardiorespiratoryFitnessList"></param>
        /// <param name="exercisePrescriptionMuscleStrengthAndMuscleEnduranceList"></param>
        /// <param name="exercisePrescriptionSoftnessList"></param>
        /// <param name="exercisePrescriptionBalanceList"></param>
        private void PrintExercisePrescription(LatestExercisePrescriptionListVM latestExercisePrescriptionTrainingStage, IEnumerable<ExercisePrescriptionCardiorespiratoryFitnessHistoryVM> exercisePrescriptionCardiorespiratoryFitnessList, IEnumerable<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceHistoryVM> exercisePrescriptionMuscleStrengthAndMuscleEnduranceList, IEnumerable<ExercisePrescriptionSoftnessHistoryVM> exercisePrescriptionSoftnessList, IEnumerable<ExercisePrescriptionBalanceHistoryVM> exercisePrescriptionBalanceList)
        {
            #region 運動處方標題
            font = new Font(baseFont, 12f, Font.NORMAL, BaseColor.BLACK);
            Paragraph exercisePrescriptionTitle = new Paragraph(new Phrase("運動處方", font));
            exercisePrescriptionTitle.SpacingAfter = 10;
            exercisePrescriptionTitle.ExtraParagraphSpace = 0;
            exercisePrescriptionTitle.Alignment = Element.ALIGN_CENTER;
            document.Add(exercisePrescriptionTitle);
            #endregion

            #region 運動處方表格
            PdfPTable exercisePrescriptionTable = new PdfPTable(3);
            PdfPCell exercisePrescriptionCell = new PdfPCell(new Phrase("訓練階段", font));
            exercisePrescriptionCell.Colspan = 3;
            exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
            exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            exercisePrescriptionCell.Padding = 5;
            exercisePrescriptionCell.ExtraParagraphSpace = 5;
            exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

            exercisePrescriptionCell = new PdfPCell(new Phrase("開始階段", font));
            exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
            exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            exercisePrescriptionCell.Padding = 5;
            exercisePrescriptionCell.ExtraParagraphSpace = 5;
            exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

            exercisePrescriptionCell = new PdfPCell(new Phrase("改善階段", font));
            exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
            exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            exercisePrescriptionCell.Padding = 5;
            exercisePrescriptionCell.ExtraParagraphSpace = 5;
            exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

            exercisePrescriptionCell = new PdfPCell(new Phrase("維持階段", font));
            exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
            exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            exercisePrescriptionCell.Padding = 5;
            exercisePrescriptionCell.ExtraParagraphSpace = 5;
            exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

            if (latestExercisePrescriptionTrainingStage != null)
            {
                exercisePrescriptionCell = new PdfPCell(new Phrase($"{latestExercisePrescriptionTrainingStage.firstStage} 週", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"{latestExercisePrescriptionTrainingStage.middleStage} 週", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase("--", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"目前階段: 第 {latestExercisePrescriptionTrainingStage.week} 週", font));
                exercisePrescriptionCell.Colspan = 3;
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);
            }
            else
            {
                exercisePrescriptionCell = new PdfPCell(new Phrase($"0 週", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"0 週", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase("--", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"目前階段: 第 1 週", font));
                exercisePrescriptionCell.Colspan = 3;
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);
            }

            document.Add(exercisePrescriptionTable);
            #endregion

            this.NewLine();

            #region 運動處方項目表格
            exercisePrescriptionTable = new PdfPTable(5);
            exercisePrescriptionCell = new PdfPCell(new Phrase("項目", font));
            exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
            exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
            exercisePrescriptionCell.Padding = 5;
            exercisePrescriptionCell.ExtraParagraphSpace = 5;
            exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

            exercisePrescriptionCell = new PdfPCell(new Phrase("訓練建議", font));
            exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
            exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
            exercisePrescriptionCell.Padding = 5;
            exercisePrescriptionCell.ExtraParagraphSpace = 5;
            exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

            exercisePrescriptionCell = new PdfPCell(new Phrase("訓練強度", font));
            exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
            exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
            exercisePrescriptionCell.Padding = 5;
            exercisePrescriptionCell.ExtraParagraphSpace = 5;
            exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

            exercisePrescriptionCell = new PdfPCell(new Phrase("訓練頻率", font));
            exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
            exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
            exercisePrescriptionCell.Padding = 5;
            exercisePrescriptionCell.ExtraParagraphSpace = 5;
            exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

            exercisePrescriptionCell = new PdfPCell(new Phrase("備註", font));
            exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
            exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
            exercisePrescriptionCell.Padding = 5;
            exercisePrescriptionCell.ExtraParagraphSpace = 5;
            exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

            if (exercisePrescriptionCardiorespiratoryFitnessList.Count() == 0 && exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.Count() == 0 && exercisePrescriptionBalanceList.Count() == 0) {
                exercisePrescriptionCell = new PdfPCell(new Phrase("查無任何資料", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.Colspan = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);
            }

            // 心肺適能
            if (exercisePrescriptionCardiorespiratoryFitnessList.Count() > 0)
            {
                exercisePrescriptionCell = new PdfPCell(new Phrase("心肺適能", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                List<string> sportsJson = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].sports);
                string sports = "";
                sportsJson.ForEach((item) =>
                {
                    sports += item.ToString() + "\n";
                });
                sports = sports.Substring(0, sports.Length - 1);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"{sports}", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"{exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].exerciseIntensity}\n{exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].heartRate}", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"{exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].trainingFrequency}", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase(exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].mark, font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);
            }

            // 肌力與肌耐力
            if (exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.Count() > 0)
            {
                exercisePrescriptionCell = new PdfPCell(new Phrase("肌力與肌耐力", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase("肌力訓練", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"{exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].exerciseIntensity}\n{exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].heartRate}", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"{exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].trainingFrequency}", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase(exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].mark, font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);
            }

            // 柔軟度
            if (exercisePrescriptionSoftnessList.Count() > 0)
            {
                exercisePrescriptionCell = new PdfPCell(new Phrase("柔軟度", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                List<string> sportsJson = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionSoftnessList.ToList()[0].sports);
                string sports = "";
                sportsJson.ForEach((item) =>
                {
                    sports += item + "\n";
                });
                sports = sports.Substring(0, sports.Length - 1);

                exercisePrescriptionCell = new PdfPCell(new Phrase("伸展訓練", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"{exercisePrescriptionSoftnessList.ToList()[0].exerciseIntensity}\n{exercisePrescriptionSoftnessList.ToList()[0].heartRate}", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase(exercisePrescriptionSoftnessList.ToList()[0].trainingFrequency, font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase(exercisePrescriptionSoftnessList.ToList()[0].mark, font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);
            }

            // 平衡
            if (exercisePrescriptionBalanceList.Count() > 0)
            {
                exercisePrescriptionCell = new PdfPCell(new Phrase("平衡", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                List<string> sportsJson = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionBalanceList.ToList()[0].sports);
                string sports = "";
                sportsJson.ForEach((item) =>
                {
                    sports += item + "\n";
                });
                sports = sports.Substring(0, sports.Length - 1);

                exercisePrescriptionCell = new PdfPCell(new Phrase(sports, font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase($"{exercisePrescriptionBalanceList.ToList()[0].exerciseIntensity}\n{exercisePrescriptionBalanceList.ToList()[0].heartRate}", font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase(exercisePrescriptionBalanceList.ToList()[0].trainingFrequency, font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);

                exercisePrescriptionCell = new PdfPCell(new Phrase(exercisePrescriptionBalanceList.ToList()[0].mark, font));
                exercisePrescriptionCell.HorizontalAlignment = Element.ALIGN_CENTER;
                exercisePrescriptionCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                exercisePrescriptionCell.BackgroundColor = BaseColor.WHITE;
                exercisePrescriptionCell.Padding = 5;
                exercisePrescriptionCell.ExtraParagraphSpace = 5;
                exercisePrescriptionTable.AddCell(exercisePrescriptionCell);
            }

            exercisePrescriptionTable.CompleteRow();
            document.Add(exercisePrescriptionTable);
            #endregion
        }

        /// <summary>
        /// 儀器設定值
        /// </summary>
        /// <param name="instrumentSettingSportList"></param>
        private void PrintInstrumentSettings(IEnumerable<InstrumentSettingSportsPerformanceListVM> instrumentSettingSportList)
        {
            #region 儀器設定值標題
            font = new Font(baseFont, 12f, Font.NORMAL, BaseColor.BLACK);
            Paragraph instrumentSettingsTitle = new Paragraph(new Phrase("儀器設定值", font));
            instrumentSettingsTitle.SpacingAfter = 10;
            instrumentSettingsTitle.Alignment = Element.ALIGN_CENTER;
            document.Add(instrumentSettingsTitle);
            #endregion

            #region 儀器設定表格
            PdfPTable instrumentSettingsTable = new PdfPTable(5);
            PdfPCell instrumentSettingsCell = new PdfPCell(new Phrase("項目", font));
            instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
            instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
            instrumentSettingsCell.Padding = 5;
            instrumentSettingsCell.ExtraParagraphSpace = 5;
            instrumentSettingsTable.AddCell(instrumentSettingsCell);

            instrumentSettingsCell = new PdfPCell(new Phrase("最適阻力等級", font));
            instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
            instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
            instrumentSettingsCell.Padding = 5;
            instrumentSettingsCell.ExtraParagraphSpace = 5;
            instrumentSettingsTable.AddCell(instrumentSettingsCell);

            instrumentSettingsCell = new PdfPCell(new Phrase("活動範圍", font));
            instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
            instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
            instrumentSettingsCell.Padding = 5;
            instrumentSettingsCell.ExtraParagraphSpace = 5;
            instrumentSettingsTable.AddCell(instrumentSettingsCell);

            instrumentSettingsCell = new PdfPCell(new Phrase("最大運動表現", font));
            instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
            instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
            instrumentSettingsCell.Padding = 5;
            instrumentSettingsCell.ExtraParagraphSpace = 5;
            instrumentSettingsTable.AddCell(instrumentSettingsCell);

            instrumentSettingsCell = new PdfPCell(new Phrase("備註", font));
            instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
            instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
            instrumentSettingsCell.Padding = 5;
            instrumentSettingsCell.ExtraParagraphSpace = 5;
            instrumentSettingsTable.AddCell(instrumentSettingsCell);

            if (instrumentSettingSportList.Count() == 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase("查無任何資料", font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.Colspan = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號一
            var machinaryCodeOne = instrumentSettingSportList.Where(w => w.machinaryCode == 1);

            if (machinaryCodeOne.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeOne.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeOne.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeOne.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeOne.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeOne.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號二
            var machinaryCodeTwo = instrumentSettingSportList.Where(w => w.machinaryCode == 2);

            if (machinaryCodeTwo.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTwo.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTwo.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTwo.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTwo.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTwo.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號三
            var machinaryCodeThree = instrumentSettingSportList.Where(w => w.machinaryCode == 3);

            if (machinaryCodeThree.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeThree.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeThree.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeThree.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeThree.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeThree.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號四
            var machinaryCodeFour = instrumentSettingSportList.Where(w => w.machinaryCode == 4);

            if (machinaryCodeFour.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFour.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFour.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFour.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFour.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFour.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號五
            var machinaryCodeFive = instrumentSettingSportList.Where(w => w.machinaryCode == 5);

            if (machinaryCodeFive.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFive.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFive.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFive.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFive.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeFive.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號六
            var machinaryCodeSix = instrumentSettingSportList.Where(w => w.machinaryCode == 6);

            if (machinaryCodeSix.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSix.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSix.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSix.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSix.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSix.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號七
            var machinaryCodeSeven = instrumentSettingSportList.Where(w => w.machinaryCode == 7);

            if (machinaryCodeSeven.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSeven.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSeven.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSeven.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSeven.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeSeven.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號八
            var machinaryCodeEight = instrumentSettingSportList.Where(w => w.machinaryCode == 8);

            if (machinaryCodeEight.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeEight.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeEight.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeEight.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeEight.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeEight.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號九
            var machinaryCodeNine = instrumentSettingSportList.Where(w => w.machinaryCode == 9);

            if (machinaryCodeNine.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeNine.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeNine.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeNine.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeNine.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeNine.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }


            // 抓取機器編號十
            var machinaryCodeTen = instrumentSettingSportList.Where(w => w.machinaryCode == 10);

            if (machinaryCodeTen.Count() > 0)
            {
                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTen.ToList()[0].machinaryCodeName, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTen.ToList()[0].level.ToString(), font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTen.ToList()[0].maxRange, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTen.ToList()[0].power, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);

                instrumentSettingsCell = new PdfPCell(new Phrase(machinaryCodeTen.ToList()[0].mark, font));
                instrumentSettingsCell.HorizontalAlignment = Element.ALIGN_CENTER;
                instrumentSettingsCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                instrumentSettingsCell.BackgroundColor = BaseColor.WHITE;
                instrumentSettingsCell.Padding = 5;
                instrumentSettingsCell.ExtraParagraphSpace = 5;
                instrumentSettingsTable.AddCell(instrumentSettingsCell);
            }
            #endregion

            instrumentSettingsTable.CompleteRow();
            document.Add(instrumentSettingsTable);
        }

        /// <summary>
        /// 檢視報表標題
        /// </summary>
        private void PrintInspectionReportTitle()
        {
            #region 檢視報表標題
            font = new iTextSharp.text.Font(baseFont, 12f, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            Paragraph inspectionReportTitle = new Paragraph(new Phrase("檢視報表", font));
            inspectionReportTitle.SpacingAfter = 10;
            inspectionReportTitle.Alignment = Element.ALIGN_CENTER;
            document.Add(inspectionReportTitle);
            #endregion
        }

        /// <summary>
        /// 檢視報表-心肺適能
        /// </summary>
        private void PrintInspectionReportCardiorespiratoryFitness(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image = Image.GetInstance(imageBytes);
            image.ScalePercent(31f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document?.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練狀況一
        /// </summary>
        private void PrintInspectionReportMachinaryCodeOneTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image2 = Image.GetInstance(imageBytes);
            image2.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image2);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練狀況二
        /// </summary>
        private void PrintInspectionReportMachinaryCodeTwoTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image = Image.GetInstance(imageBytes);
            image.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練狀況三
        /// </summary>
        private void PrintInspectionReportMachinaryCodeThreeTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image = Image.GetInstance(imageBytes);
            image.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練狀況四
        /// </summary>
        private void PrintInspectionReportMachinaryCodeFourTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image = Image.GetInstance(imageBytes);
            image.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練狀況五
        /// </summary>
        private void PrintInspectionReportMachinaryCodeFiveTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image = Image.GetInstance(imageBytes);
            image.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練狀況六
        /// </summary>
        private void PrintInspectionReportMachinaryCodeSixTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageBytes);
            image.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練七
        /// </summary>
        private void PrintInspectionReportMachinaryCodeSevenTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image = Image.GetInstance(imageBytes);
            image.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練八
        /// </summary>
        private void PrintInspectionReportMachinaryCodeEightTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image = Image.GetInstance(imageBytes);
            image.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練九
        /// </summary>
        private void PrintInspectionReportMachinaryCodeNineTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageBytes);
            image.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 機器訓練十
        /// </summary>
        private void PrintInspectionReportMachinaryCodeTenTrainingStatus(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageBytes);
            image.ScalePercent(26f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 檢視報表-柔軟度
        /// </summary>
        private void PrintInspectionReportSoftness(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image = Image.GetInstance(imageBytes);
            image.ScalePercent(31f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 檢視報表-平衡
        /// </summary>
        private void PrintInspectionReportBalance(string base64Image)
        {
            byte[] imageBytes = Convert.FromBase64String(base64Image);
            Image image = Image.GetInstance(imageBytes);
            image.ScalePercent(31f);

            PdfPTable inspectionReportTable = new PdfPTable(1);
            PdfPCell inspectionReportCell = new PdfPCell(image);
            inspectionReportCell.Padding = 5;
            inspectionReportCell.HorizontalAlignment = Element.ALIGN_CENTER;
            inspectionReportCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            inspectionReportTable.AddCell(inspectionReportCell);
            document.Add(inspectionReportTable);
        }

        /// <summary>
        /// 換行
        /// </summary>
        private void NewLine()
        {
            Paragraph paragraph = new Paragraph(Chunk.NEWLINE);
            document.Add(paragraph);
        }
    }
}

public class RequestPrintTrainingReportModels
{
    /// <summary>
    /// 列印身體組成與體適能評價
    /// </summary>
    public bool isPrintBodyCompositionAndPhysicalFitness { get; set; } = false;

    /// <summary>
    /// 列印運動處方
    /// </summary>
    public bool isPrintExercisePrescription { get; set; } = false;

    /// <summary>
    /// 列印儀器設定
    /// </summary>
    public bool isPrintInstrumentSetting { get; set; } = false;

    /// <summary>
    /// 列印檢視報表心肺適能
    /// </summary>
    public string printInspectionReportCardiorespiratoryFitness { get; set; } = "";

    /// <summary>
    /// 列印機器訓練一
    /// </summary>
    public string printInspectionReportMachinaryOne { get; set; } = "";

    /// <summary>
    /// 列印機器訓練二
    /// </summary>
    public string printInspectionReportMachinaryTwo { get; set; } = "";

    /// <summary>
    /// 列印機器訓練三
    /// </summary>
    public string printInspectionReportMachinaryThree { get; set; } = "";

    /// <summary>
    /// 列印機器訓練四
    /// </summary>
    public string printInspectionReportMachinaryFour { get; set; } = "";

    /// <summary>
    /// 列印機器訓練五
    /// </summary>
    public string printInspectionReportMachinaryFive { get; set; } = "";

    /// <summary>
    /// 列印機器訓練六
    /// </summary>
    public string printInspectionReportMachinarySix { get; set; } = "";

    /// <summary>
    /// 列印機器訓練七
    /// </summary>
    public string printInspectionReportMachinarySeven { get; set; } = "";

    /// <summary>
    /// 列印機器訓練八
    /// </summary>
    public string printInspectionReportMachinaryEight { get; set; } = "";

    /// <summary>
    /// 列印機器訓練九
    /// </summary>
    public string printInspectionReportMachinaryNine { get; set; } = "";

    /// <summary>
    /// 列印機器訓練十
    /// </summary>
    public string printInspectionReportMachinaryTen { get; set; } = "";

    /// <summary>
    /// 列印檢視報表柔軟度
    /// </summary>
    public string printInspectionReportSoftness { get; set; } = "";

    /// <summary>
    /// 列印檢視報表平衡
    /// </summary>
    public string printInspectionReportBalance { get; set; } = "";
}