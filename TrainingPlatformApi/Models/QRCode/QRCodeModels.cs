using Dapper;
using MySql.Data.MySqlClient;

namespace TrainingPlatformApi.Models.QRCode
{
    public class QRCodeModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public QRCodeModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查學員電話
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public T InspectStudentPhone<T>(int studentId)
        {
            sqlStr = @"SELECT userAuthorizedId,
                              phone
                         FROM user_authorized
                        WHERE userAuthorizedId = @userAuthorizedId
            ";

            object param = new
            {
                userAuthorizedId = studentId
            };

            return conn.Query<T>(sqlStr, param).SingleOrDefault();
        }
    }
}
