namespace TrainingPlatformApi.Models
{
    public class PackageJsonModels
    {
        public string name { get; set; } = "";
        public string version { get; set; } = "";
        public string description { get; set; } = "";
        public string author { get; set; } = "";
        public string license { get; set; } = "";
    }
}
