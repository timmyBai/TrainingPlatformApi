namespace TrainingPlatformApi.Tests.VM.Auth
{
    public class AuthLoginTokenVM
    {
        /// <summary>
        /// 訪問令牌
        /// </summary>
        public string accessToken { get; set; } = "";

        /// <summary>
        /// 令牌過期時間
        /// </summary>
        public long expires_in { get; set; } = 0;
    }
}
