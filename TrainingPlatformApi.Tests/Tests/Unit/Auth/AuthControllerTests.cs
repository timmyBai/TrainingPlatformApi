using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrainingPlatformApi.Tests.Controllers.Auth;
using TrainingPlatformApi.Tests.Models.Auth;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace TrainingPlatformApi.Tests.Tests.Unit.Auth
{
    public class AuthControllerTests
    {
        [Test]
        [TestMethod]
        public void TestAuthLoginBadRequest()
        {
            RequestAuthLoginModels requestAuthLoginModels = new RequestAuthLoginModels();

            var controller = new AuthController().AuthLogin(requestAuthLoginModels);

            Assert.IsTrue(!controller.isSuccess);
            Assert.AreNotEqual(controller.message, "BadRequest");
        }

        [Test]
        [TestMethod]
        public void TestAuthLoginNoSuchUser()
        {
            RequestAuthLoginModels requestAuthLoginModels = new RequestAuthLoginModels();
            requestAuthLoginModels.account = "admin";
            requestAuthLoginModels.password = "admin";

            var controller = new AuthController().AuthLogin(requestAuthLoginModels);
            Assert.IsTrue(!controller.isSuccess);
            Assert.AreEqual(controller.message, "NoSuchUser");
        }

        [Test]
        [TestMethod]
        public void TestAuthLoginAccountDisabled()
        {
            RequestAuthLoginModels requestAuthLoginModels = new RequestAuthLoginModels();
            requestAuthLoginModels.account = "test3123456";
            requestAuthLoginModels.password = "test3123456";

            var controller = new AuthController().AuthLogin(requestAuthLoginModels);

            Assert.IsTrue(!controller.isSuccess);
            Assert.AreEqual(controller.message, "AccountDisabled");
        }

        [Test]
        [TestMethod]
        public void TestAuthLoginSendTokenSuccess()
        {
            RequestAuthLoginModels requestAuthLoginModels = new RequestAuthLoginModels();
            requestAuthLoginModels.account = "test2123456";
            requestAuthLoginModels.password = "test2123456";

            var controller = new AuthController().AuthLogin(requestAuthLoginModels);

            Assert.IsTrue(controller.isSuccess);
            Assert.AreEqual(controller.message, "");
            Assert.IsTrue(controller.data != null);
            Assert.IsTrue(controller.data.GetType().GetProperty("accessToken").GetValue(controller.data, null).ToString() != "");
            Assert.IsTrue(controller.data.GetType().GetProperty("expires_in").GetValue(controller.data, null).ToString() != "");
        }

        [Test]
        [TestMethod]
        public void TestAuthLoginInternalServerError()
        {
            RequestAuthLoginModels requestAuthLoginModels = new RequestAuthLoginModels();
            requestAuthLoginModels.account = "test2123456";
            requestAuthLoginModels.password = "test2123456";

            var controller = new AuthController().AuthLogin(requestAuthLoginModels);
            Assert.IsTrue(controller.isSuccess != false);
            Assert.IsTrue(controller.message != "InternalServerError");
        }
    }
}
