using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Web.Http;
using TrainingPlatformApi.Tests.Models;
using TrainingPlatformApi.Tests.Models.Auth;
using TrainingPlatformApi.Tests.VM.Auth;
using TrainingPlatformApi.Tests.Utils;

namespace TrainingPlatformApi.Tests.Controllers.Auth
{
    public class AuthController : ApiController
    {
        public ResultModels AuthLogin(RequestAuthLoginModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";

                return result;
            }

            try
            {
                // 檢查帳號密碼
                var userInfo = new AuthModels().InspactUserAuthSecret(body);

                // 如果帳號不存在
                if (userInfo == false)
                {
                    result.isSuccess = false;
                    result.message = "NoSuchUser";
                    return result;
                }

                // 如果帳號停用

                if (AuthModels.studentModels.ToList()[AuthModels.studentIndex].status == 0)
                {
                    result.isSuccess = false;
                    result.message = "AccountDisabled";
                    return result;
                }

                Claim[] claim = new[]
                {
                    new Claim("id", AuthModels.studentModels.ToList()[AuthModels.studentIndex].userAuthorizedId.ToString()),
                    new Claim("account", AuthModels.studentModels.ToList()[AuthModels.studentIndex].account.ToString()),
                    new Claim("name", AuthModels.studentModels.ToList()[AuthModels.studentIndex].name.ToString()),
                    new Claim("imageUrl", AuthModels.studentModels.ToList()[AuthModels.studentIndex].fileImageUrl.ToString()),
                    new Claim("_gender", AuthModels.studentModels.ToList()[AuthModels.studentIndex].gender.ToString()),
                    new Claim("birthday", AuthModels.studentModels.ToList()[AuthModels.studentIndex].birthday.ToString()),
                    new Claim("age", AuthModels.studentModels.ToList()[AuthModels.studentIndex].age.ToString()),
                    new Claim("_role", AuthModels.studentModels.ToList()[AuthModels.studentIndex].role.ToString()),
                    new Claim("identity", AuthModels.studentModels.ToList()[AuthModels.studentIndex].identity.ToString()),
                    new Claim(JwtRegisteredClaimNames.Nbf, $"{new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()}"),
                    new Claim(JwtRegisteredClaimNames.Exp, $"{new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds()}")
                };

                // 建立 token 模型
                AuthLoginTokenVM tokenAuthVM = new AuthLoginTokenVM();
                tokenAuthVM.accessToken = new JwtUtils().GenerateJwtToken(claim);
                tokenAuthVM.expires_in = Convert.ToInt64(new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds().ToString().PadRight(13, '0'));

                // 統一資料壓縮回傳
                result.isSuccess = true;
                result.data = tokenAuthVM;
                return result;
            }
            catch
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                return result;
            }
        }
    }
}
