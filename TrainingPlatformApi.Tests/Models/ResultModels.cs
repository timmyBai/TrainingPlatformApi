namespace TrainingPlatformApi.Tests.Models
{
    public class ResultModels
    {
        /// <summary>
        /// 使否成功
        /// </summary>
        public bool isSuccess { get; set; }

        /// <summary>
        /// 訊息
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 資料
        /// </summary>
        public object data { get; set; }

        public ResultModels()
        {
            isSuccess = false;
            message = string.Empty;
            data = new object();
        }
    }
}
