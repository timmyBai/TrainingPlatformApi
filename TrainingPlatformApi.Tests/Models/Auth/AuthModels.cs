using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingPlatformApi.Tests.Models.Auth
{
    public class StudentModels
    {
        public int userAuthorizedId { get; set; } = 0;
        public string name { get; set; } = "";
        public string account { get; set; } = "";
        public string password { get; set; } = "";
        public string fileImageUrl { get; set; } = "";
        public char gender { get; set; } = '男';
        public string birthday { get; set; } = "";
        public int age { get; set; } = 0;
        public string role { get; set; } = "";
        public string identity { get; set; } = "";
        public int status { get; set; } = 0;
    }

    public class AuthModels
    {
        public static List<StudentModels>? studentModels;
        public static int studentIndex = 0;

        public AuthModels()
        {
            studentModels = new List<StudentModels>()
            {
                new StudentModels()
                {
                    userAuthorizedId = 1,
                    name = "Timmy",
                    account = "test1123456",
                    password = "test1123456",
                    fileImageUrl = "",
                    gender = '男',
                    birthday = "19960520",
                    age = 27,
                    role = "業主",
                    status = 1
                },
                new StudentModels()
                {
                    userAuthorizedId = 2,
                    name = "Tim",
                    account = "test2123456",
                    password = "test2123456",
                    fileImageUrl = "",
                    gender = '男',
                    birthday = "19561231",
                    age = 67,
                    role = "物理治療師",
                    status = 1
                },
                new StudentModels()
                {
                    userAuthorizedId = 3,
                    name = "Jason",
                    account = "test3123456",
                    password = "test3123456",
                    fileImageUrl = "",
                    gender = '女',
                    birthday = "19661005",
                    age = 57,
                    role = "復健師",
                    status = 0
                }
            };
        }

        public bool InspactUserAuthSecret(RequestAuthLoginModels obj)
        {
            var result = false;

            for (int i = 0; i < studentModels.Count(); i++)
            {
                var temp = studentModels.ToList()[i];

                if (temp.account == obj.account && temp.password == obj.password)
                {
                    result = true;
                    studentIndex = i;
                    break;
                }
            }

            return result;
        }
    }

    /// <summary>
    /// 接收 會員登驗證模型
    /// </summary>
    public class RequestAuthLoginModels
    {
        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        public string account { get; set; } = "";

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        public string password { get; set; } = "";
    }
}
