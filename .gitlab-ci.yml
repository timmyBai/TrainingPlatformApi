stages:
    - testing
    - build_development_image
    - deploy_development
    - build_staging_image
    - deploy_staging
    - build_production_image
    - deploy_production
    - build_ship_image
    - deploy_ship

variables:
    DOTNET_CORE_IMAGE: mcr.microsoft.com/dotnet/sdk:6.0

.deploy:
    variables:
        SERVER_USER_NAME: ""
        SERVER_URL: ""
        SERVER_PRIVATE_KEY: ""
        SERVER_PORT: ""
        BUILD_ENV: ""
        SERVICE_NAME: ""
    tags:
        - shell
    retry:
        max: 2
        when: always
    before_script:
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval $(ssh-agent -s)
        - mkdir -p ~/.ssh
        - echo -e "$SERVER_PRIVATE_KEY" > ~/.ssh/id_rsa
        - chmod 600 ~/.ssh/id_rsa
        - ssh-add ~/.ssh/id_rsa
    script:
        - ssh -o StrictHostKeyChecking=no $SERVER_USER_NAME@$SERVER_URL "
            mkdir -p TrainingPlatformApi &&
            cd TrainingPlatformApi &&
            rm -rf *"
        - scp -o StrictHostKeyChecking=no -r ./docker-compose.$BUILD_ENV.yml $SERVER_USER_NAME@$SERVER_URL:~/TrainingPlatformApi
        - scp -o StrictHostKeyChecking=no -r ./inspect_docker_network.sh $SERVER_USER_NAME@$SERVER_URL:~/TrainingPlatformApi
        - ssh -o StrictHostKeyChecking=no $SERVER_USER_NAME@$SERVER_URL "
            echo $DOCKER_REGISTRY_PASSWORD | docker login -u $DOCKER_REGISTRY_USER_NAME --password-stdin $DOCKER_REGISTRY_URL:$DOCKER_REGISTRY_PORT &&
            cd TrainingPlatformApi &&
            docker compose -f docker-compose.$BUILD_ENV.yml down &&
            docker system prune -af &&
            export DOCKER_NETWORK_NAME=backend_$BUILD_ENV &&
            chmod 700 inspect_docker_network.sh &&
            ./inspect_docker_network.sh &&
            docker compose -f docker-compose.$BUILD_ENV.yml up --build -d"
    environment:
        name: $SERVICE_NAME
        url: http://$SERVER_URL:$SERVER_PORT

.build-to-docker-image:
    variables:
        REGISTRY_USER_NAME: ""
        REGISTRY_URL: ""
        REGISTRY_PORT: ""
        REGISTRY_PASSWORD: ""
        BUILD_ENV: ""
    before_script:
        - echo $REGISTRY_PASSWORD | docker login -u $REGISTRY_USER_NAME --password-stdin $REGISTRY_URL:$REGISTRY_PORT
        # - export IMAGE_VERSION=$(cat package.json | jq -r .version)
    script:
        - docker build -f dockerfile.$BUILD_ENV -t $REGISTRY_URL:$REGISTRY_PORT/emgtech/training_platform_api_$BUILD_ENV:latest .
        - docker push $REGISTRY_URL:$REGISTRY_PORT/emgtech/training_platform_api_$BUILD_ENV:latest
        # - docker build -f dockerfile.development -t $REGISTRY_URL:$REGISTRY_PORT/emgtech/training_platform_api_$BUILD_ENV:$IMAGE_VERSION .
        # - docker push $REGISTRY_URL:$REGISTRY_PORT/emgtech/training_platform_api_$BUILD_ENV:$IMAGE_VERSION
    after_script:
        - docker system prune -af

unit-tests:
    stage: testing
    image: $DOTNET_CORE_IMAGE
    tags:
        - docker
    retry:
        max: 2
        when: always
    script:
        - dotnet test TrainingPlatformApi.Tests > TrainingPlatformApi.Tests.txt
    artifacts:
        paths:
            - TrainingPlatformApi.Tests.txt
        expire_in: "30 days"
    only:
        - main
        - develop
    
test-build:
    stage: testing
    image: $DOTNET_CORE_IMAGE
    tags:
        - docker
    retry:
        max: 2
        when: always
    needs:
        - unit-tests
    script:
        - dotnet restore ./TrainingPlatformApi/*.csproj
        - dotnet publish -c Release -o out
    only:
        - main
        - develop

build-to-docker-development-image:
    stage: build_development_image
    extends: .build-to-docker-image
    tags:
        - shell
    needs:
        - test-build
    variables:
        REGISTRY_USER_NAME: $DOCKER_REGISTRY_USER_NAME
        REGISTRY_URL: $DOCKER_REGISTRY_URL
        REGISTRY_PORT: $DOCKER_REGISTRY_PORT
        REGISTRY_PASSWORD: $DOCKER_REGISTRY_PASSWORD
        BUILD_ENV: development
    only:
        - develop

deploy-to-development:
    stage: deploy_development
    extends: .deploy
    needs:
        - build-to-docker-development-image
    variables:
        SERVER_USER_NAME: $DEV_SERVER_USER_NAME
        SERVER_URL: $DEV_SERVER_URL
        SERVER_PRIVATE_KEY: $DEV_SERVER_PRIVATE_KEY
        SERVER_PORT: $DEV_SERVER_PORT
        BUILD_ENV: development
        SERVICE_NAME: training platform api development
    only:
        - develop

build-to-docker-staging-image:
    stage: build_staging_image
    extends: .build-to-docker-image
    tags:
        - shell
    needs:
        - deploy-to-development
    variables:
        REGISTRY_USER_NAME: $DOCKER_REGISTRY_USER_NAME
        REGISTRY_URL: $DOCKER_REGISTRY_URL
        REGISTRY_PORT: $DOCKER_REGISTRY_PORT
        REGISTRY_PASSWORD: $DOCKER_REGISTRY_PASSWORD
        BUILD_ENV: staging
    only:
        - develop

deploy-to-staging:
    stage: deploy_staging
    extends: .deploy
    needs:
        - build-to-docker-staging-image
    variables:
        SERVER_USER_NAME: $STAGING_SERVER_USER_NAME
        SERVER_URL: $STAGING_SERVER_URL
        SERVER_PRIVATE_KEY: $STAGING_SERVER_PRIVATE_KEY
        SERVER_PORT: $STAGING_SERVER_PORT
        BUILD_ENV: staging
        SERVICE_NAME: training platform api staging
    only:
        - develop

build-to-docker-production-image:
    stage: build_production_image
    extends: .build-to-docker-image
    tags:
        - shell
    needs:
        - test-build
    variables:
        REGISTRY_USER_NAME: $DOCKER_REGISTRY_USER_NAME
        REGISTRY_URL: $DOCKER_REGISTRY_URL
        REGISTRY_PORT: $DOCKER_REGISTRY_PORT
        REGISTRY_PASSWORD: $DOCKER_REGISTRY_PASSWORD
        BUILD_ENV: production
    only:
        - main

deploy-to-production:
    stage: deploy_production
    extends: .deploy
    needs:
        - build-to-docker-production-image
    when: manual
    variables:
        SERVER_USER_NAME: $PROD_SERVER_USER_NAME
        SERVER_URL: $PROD_SERVER_URL
        SERVER_PRIVATE_KEY: $PROD_SERVER_PRIVATE_KEY
        SERVER_PORT: $PROD_SERVER_PORT
        BUILD_ENV: production
        SERVICE_NAME: training platform api production
    only:
        - main

build-to-docker-ship-image:
    stage: build_ship_image
    extends: .build-to-docker-image
    tags:
        - shell
    needs:
        - deploy-to-production
    variables:
        REGISTRY_USER_NAME: $DOCKER_REGISTRY_USER_NAME
        REGISTRY_URL: $DOCKER_REGISTRY_URL
        REGISTRY_PORT: $DOCKER_REGISTRY_PORT
        REGISTRY_PASSWORD: $DOCKER_REGISTRY_PASSWORD
        BUILD_ENV: ship
    only:
        - main

deploy-to-ship:
    stage: deploy_ship
    extends: .deploy
    needs:
        - build-to-docker-ship-image
    when: manual
    variables:
        SERVER_USER_NAME: $SHIP_SERVER_USER_NAME
        SERVER_URL: $SHIP_SERVER_URL
        SERVER_PRIVATE_KEY: $SHIP_SERVER_PRIVATE_KEY
        SERVER_PORT: $SHIP_SERVER_PORT
        BUILD_ENV: ship
        SERVICE_NAME: training platform api ship
    only:
        - main
        