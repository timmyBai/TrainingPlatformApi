# Training Platform Api

<div>
    <img src="https://img.shields.io/badge/C%23-6.0.0-blue">
    <img src="https://img.shields.io/badge/.NetCore-6.0.0-blue">
    <img src="https://img.shields.io/badge/WebApi-6.0.0-blue">
</div>

## 簡介

Training Platform Api 是一個等速肌力訓練後端解決方案，他基於 C#、.Net Core 6、Web Api 實現，他使用最新後端技術，內置了 token 登入權限解決方案，資料庫連線，跨域連線，Swagger，輕量級、高效能且模組化的 HTTP 要求管線，跨平台相容性，提供了許多後端處理方法，他可以幫助你快速搭建後端原型，可以幫助你解決許多需求。本專案針對等速肌力專案釋出相對應 api，提供前台與後台需求，操作相關資料

## 功能

```
- 會員登入與註冊
    - 會員登入
    - 會員註冊
- 會員資訊
    取得會員資訊
- 學員
    - 取得學員清單
    - 停用學員
    - 更新學員基本資訊
    - 取得某學員最新基本資料
- 身體組成與體適能評價
    - 身體組成與體適能評價清單
    - 新增或更新身體組成 bmi與備註
    - 新增或更新身體組成 體脂肪與備註
    - 新增或更新學員 內臟脂肪與備註
    - 新增或更新學員 骨骼心肌率與備註
    - 新增或更新學員體適能-心肺適能 登階測驗與備註
    - 新增或更新學員體適能-心肺適能 原地站立抬膝與備註
    - 新增或更新學員體適能-肌力與肌耐力 屈膝仰臥起坐與備註
    - 新增或更新學員體適能-肌力與肌耐力 手臂彎舉(上肢)與備註
    - 新增或更新學員體適能-肌力與肌耐力 手臂彎舉(下肢)與備註
    - 新增或更新學員體適能-柔軟度 坐姿體前彎與備註
    - 新增或更新學員體適能-柔軟度 椅子坐姿體前彎與備註
    - 新增或更新學員體適能-柔軟度 拉背側驗與備註
    - 新增或更新學員體適能-平衡 壯年開眼單足立與備註
    - 新增或更新學員體適能-平衡 老年開眼單足立與備註
    - 新增或更新學員體適能-平衡 坐立繞物第一次秒數與備註
    - 新增或更新學員體適能-平衡 坐立繞物第二次秒數與備註
    - 新增或更新學員體適能-心肺適能 備註
    - 新增或更新學員體適能-肌力與肌耐力 備註
    - 新增或更新學員體適能-柔軟度 備註
    - 新增或更新學員體適能-平衡 備註
- 運動處方
    - 取得學員運動處方清單
    - 取得運動處方-訓練階段
    - 新增或更新 訓練階段
    - 新增或更新運動處方 心肺適能
    - 新增或更新運動處方 肌力與肌耐力
    - 新增或更新運動處方 柔軟度
    - 新增或更新運動處方 平衡
- QRCode 生成
    - qrcode 生成
- 儀器設定
    - 取得儀器設定運動表現清單
    - 新增或更新儀器設定運動表現備註
- 訓練排程
    - 訓練排程通知
    - 取得訓練排程-心肺適能這週剩餘排程天數
    - 取得訓練排程-肌力與肌耐力這週剩餘排程天數
    - 取得訓練排程-柔軟度這週剩餘排程天數
    - 取得訓練排程-平衡這週剩餘排程天數
    - 取得訓練排程-心肺適能實際完成次數
    - 取得訓練排程-肌力與肌耐力實際完成次數
    - 取得訓練排程-柔軟度實際完成次數
    - 取得訓練排程-平衡實際完成次數
    - 更新訓練排程實際完成次數
    - 新增訓練排程
- 檢視報表
    - 取得報表體適能
    - 取得學員使用機器總功率
    - 取得過去 20 次訓練狀況報表
    - 取得過去 20 次訓練狀況-功率
    - 取得單日報表
    - 取得學員正式訓練訓練各台儀器總功率
    - 取得學員機器訓練總表
    - 新增或更新學員正式機器訓練當天備註
    - 刪除學員正式機器訓練當天備註
- 列印
    - 等速肌力訓練報表
```

## 開發

```
# 克隆項目
git clone http://192.168.1.31/gitlab-instance-f45c9bb0/training_platform_api.git
```

## 本地發佈

```docker
# development(開發機)
docker-compose -f docker-compose.development.yml build && docker-compose docker-compose.development.yml up -d

# staging(demo 機)
docker-compose -f docker-compose.staging.yml build && docker-compose -f docker-compose.staging.yml up -d

# production(正式機)
docker-compose -f docker-compose.production.yml build && docker-compose -f docker-compose.production.yml up -d

# ship(出貨機)
docker-compose -f docker-compose.ship.yml build && docker-compose -f docker-compose.ship.yml up -d
```

## 佈署伺服器

| 環境          | 自動化佈署 | 必須設定 SSH | 必須安裝 docker | 必須安裝 docker-compose |
| ----------- | ----- | -------- | ----------- | ------------------- |
| development | √     | √        | √           | √                   |
| staging     | √     | √        | √           | √                   |
| production  | √     | √        | √           | √                   |
| ship        | √     | √        | √           | √                   |

## 自動化佈署流程

| 階段名稱                    | 工作名稱                              | 作用                          |
| ----------------------- | --------------------------------- | --------------------------- |
| testing                 | unit-tests                        | 單元測試方法                      |
| testing                 | test-build                        | 測試打包                        |
| build_development_image | build-to-docker-development-image | 打包 development docker image |
| deploy_development      | deploy-to-development             | 佈署開發機                       |
| build_staging_image     | build-to-docker-staging-image     | 打包 staging docker image     |
| deploy_staging          | deploy-to-staging                 | 佈署 demo 機                   |
| build_production_image  | build-to-docker-production-image  | 打包 production docker image  |
| deploy_production       | deploy-to-production              | 佈署正式機                       |
| build_ship_image        | build-to-docker-ship-image        | 打包 ship docker image        |
| deploy_ship             | deploy-to-ship                    | 佈署出貨機                       |

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |
